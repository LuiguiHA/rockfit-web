#!/bin/bash
python manage.py migrate
python manage.py collectstatic --noinput
gunicorn \
  --env DJANGO_SETTINGS_MODULE=project.settings \
  --timeout 18000 \
  --log-level DEBUG \
  --capture-output \
  --access-logfile /usr/src/app/logs/gunicorn-access.log \
  --error-logfile /usr/src/app/logs/gunicorn-error.log \
  project.wsgi
nginx -g 'daemon off;'