from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.template.loader import render_to_string
import json


@csrf_exempt
@staff_member_required
def contact_rockfit(request):
    data = json.loads(request.body)
    contact_info = data.get("contact_info")

    if hasattr(request.user, 'admin_sport_center'):
      admin_sport_center = request.user.admin_sport_center
        
      email_template = render_to_string(
         "admin/mails/contact.html",
         {
             'message': contact_info,
             'sport_center': admin_sport_center.sport_center,
         }
      )

      print('email_template', email_template)
    
    return HttpResponse('OK')


@csrf_exempt
@staff_member_required
def cancel_request_rockfit_admin(request):
    data = json.loads(request.body)
    user_name = data.get("user_name")
    user_code = data.get("user_code")
    user_document = data.get("user_document")
    registration_date = data.get("registration_date")
    registration_hour = data.get("registration_hour")
    registration_class = data.get("registration_class")
    request_reason = data.get("request_reason")

    if hasattr(request.user, 'admin_sport_center'):
      admin_sport_center = request.user.admin_sport_center
      email_template = render_to_string(
        "admin/mails/cancellation.html",
        {
          'user_name': user_name,
          'user_code': user_code,
          'user_document': user_document,
          'registration_date': registration_date,
          'registration_hour': registration_hour,
          'registration_class': registration_class,
          'request_reason': request_reason,
          'sport_center': admin_sport_center.sport_center,
        }
      )

    return HttpResponse('OK')