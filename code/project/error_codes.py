CODES = {
    "COUPON": {
        "WRONG_COUPON": {
            "message": "Wrong coupon",
            "code": 100,
        },
        "COUPON_USED": {
            "message": "Coupon used",
            "code": 101,
        }
    },
    "LOGIN": {
        "BAD_CREDENTIALS": {
            "message": "Bad credentials",
            "code": 201,
        }
    },
    "RESERVATIONS": {
        "RESERVATION_DO_NOT_EXIST": {
            "message": "Reservation do not exist",
            "code": 301,
        },
        "RESEVATION_OUT_OF_TIME": {
            "message": "Reservation out of time",
            "code": 302,
        },
        "USER_DOES_NOT_EXIST": {
            "message": "User do not exist",
            "code": 303,
        },
        "NOT_AVAILABLE_CLASSES": {
            "message": "No classes available",
            "code": 304,
        },
        "NOT_AVAILABLE_PASSES": {
            "message": "No passes available",
            "code": 305,
        },
        "NOT_COUNT_AVAILABLE": {
            "message": "No count available",
            "code": 306
        },
        "RESERVATION_EXISTS": {
            "message": "No count available",
            "code": 307
        },
        "RESERVATION_AFTER_HOURS": {
            "message": "No count available",
            "code": 308
        },
        "RESERVATION_NOT_AVAILABLE": {
            "message": "No count available",
            "code": 309
        },
        "SCHEDULE_CROSSED": {
            "message": "No count available",
            "code": 310
        },
        "CENTER_NOT_ALLOWED": {
            "message": "No count available",
            "code": 311
        },
        "CENTER_NOT_PERMITTED": {
            "message": "No count available",
            "code": 312
        },
        "FREEZE_PLAN": {
            "message": "No count available",
            "code": 313
        }
    }
}