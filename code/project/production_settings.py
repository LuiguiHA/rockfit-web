from project.settings import *

STATIC_ROOT = os.path.join(BASE_DIR, 'static') 
STATICFILES_FINDERS = [
    # 'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'sass_processor.finders.CssFinder',
]

DEBUG = True