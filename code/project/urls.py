"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from project.admin_functions import contact_rockfit, cancel_request_rockfit_admin
from django.conf.urls.static import static

urlpatterns = [
    path('jet/', include('jet.urls', 'jet')),
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    path('tinymce/', include('tinymce.urls')),
    path('admin/', admin.site.urls),
    # path('nested_admin/', include('nested_admin.urls')),
    path('', include('home.urls')), 
    path('auth/', include('users.urls')), 
    path('centros-deportivos/', include('sport_centers.urls')), 
    path('planes/', include('plans.urls')), 
    path('clases/', include('classes.urls')), 
    path('contact-rockfit-admin', contact_rockfit, name='contact-rockfit-admin'),
    path('cancel-request-rockfit-admin', cancel_request_rockfit_admin, name='cancel-request-rockfit-admin'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
