new Vue({
  el: '#assistance-validation',
  delimiters: ['${', '}'],
  data: {
    dni:null,
    code:null,
    information: {},
    userNotRecognized: false,
    minutesModalSession: 5,
    timeoutModalSession: 0,
  },
  watch:{
    
  },
  computed:{
    haveClasses: function(){
      return (
        (this.information.reservation && this.information.reservation.origin == "PLAN_PASS") ||
        (this.information.reservation == null && this.information.plan && this.information.plan.available_passes_on_sport_center > 0) ||
        (this.information.reservation && this.information.reservation.origin == "PASS") ||
        (this.information.reservation == null && this.information.passes_not_from_plan && this.information.passes_not_from_plan.length > 0)
      )
    },
    planWithoutClasses: function(){
      return this.information.plan != null && this.information.reservation == null && this.information.plan.available_passes_on_sport_center == 0
    },
    noPlanAndNoPasses: function(){
      return this.information.plan == null && this.information.reservation == null && this.information.passes_not_from_plan && this.information.passes_not_from_plan.length == 0
    }
  },
  methods:{
    startModalSession: function(){
      this.timeoutModalSession = setTimeout(function(){
        this.$modal.hide("information")
        this.$modal.show("session-timeout")
      }.bind(this), (this.minutesModalSession * 60) * 1000)
    },
    confirmReservation: function(){
      var passId = (this.information.reservation ? this.information.reservation.related_pass_id : this.information.pass_id)
      axios.post(confirmAssistanceUrl, {pass_id: passId})
      .then(function (response) {
        console.log(response)
        if(response.data.error){
          this.$modal.hide("information")
          this.$modal.show("registration-error")
        } else{
          clearTimeout(this.timeoutModalSession)
          this.$modal.hide("information")
          this.$modal.hide("registration-error")
          this.$modal.show("registration-confirm")
          this.dni = ""
          this.code = ""
        }

      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    closeModal: function(name){
      this.$modal.hide(name)
    },
    onModalClosed: function(){
      clearTimeout(this.timeoutModalSession)
    },
    retryRegistration: function(){
      this.confirmReservation()
    },
    reportError: function(){
      this.$modal.hide("registration-error")
      window.topMenu.contactRockfit()
    },
    validateUser: function(){

      clearTimeout(this.timeoutModalSession)

      this.userNotRecognized = false

      axios.post(validateAssistanceUrl, {
        dni: this.dni,
        code: this.code
      })
      .then(function (response) {
        if(response.data.error){
          if(response.data.code == 303){
            this.userNotRecognized = false  
            this.$modal.show("user-not-recognized")
          }

        } else{
          this.information = response.data
          this.$nextTick(function(){
            if(this.haveClasses){
              this.startModalSession()  
            }
            this.$modal.show("information")
          }.bind(this))  
        }

        
        
        
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    }
  },
  mounted: function(){
  }
})