var topMenu = new Vue({
  el: '#top-menu',
  delimiters: ['${', '}'],
  data: {
    contactInfo: "",
    contactSent: false,
    userName: "",
    userCode: "",
    userDocument: "",
    registrationDate: "",
    registrationHour: "",
    registrationClass: "",
    requestReason: "",
    cancellationSent: false,
  },
  watch:{
    
  },
  validations: {
    contactInfo: {
      requiredValidator,
    },
    userName: {
      requiredValidator,
    },
    userCode: {
      requiredValidator,
    },
    userDocument: {
      requiredValidator,
    },
    registrationDate: {
      requiredValidator,
    },
    registrationHour: {
      requiredValidator,
    },
    registrationClass: {
      requiredValidator,
    },
    requestReason: {
      requiredValidator,
    },
  },
  computed:{
    contactFormValid: function(){
      return this.$v.contactInfo.$error == false
    },
    cancellationFormValid: function(){
      return (
        this.$v.userName.$error == false &&
        this.$v.userCode.$error == false &&
        this.$v.userDocument.$error == false &&
        this.$v.registrationDate.$error == false &&
        this.$v.registrationHour.$error == false &&
        this.$v.registrationClass.$error == false &&
        this.$v.requestReason.$error == false
      )
    },
  },
  methods:{
    onModalClosed: function(){
      this.closeModal()
    },
    contactRockfit: function(){
        this.$modal.show("contact-rockfit-modal")
    },
    requestVisitCancel: function(){
        this.$modal.show("anulation-rockfit-modal")
    },
    closeModal: function(){
      this.$modal.hide("contact-rockfit-modal")
      this.$modal.hide("anulation-rockfit-modal")
      this.resetData()    
      this.contactSent = false
      this.cancellationSent = false
    },
    resetData: function(){
      this.$v.$reset()
      this.contactInfo = ""
      this.userName = ""
      this.userCode = ""
      this.userDocument = ""
      this.registrationDate = ""
      this.registrationHour = ""
      this.registrationClass = ""
      this.requestReason = ""
    },
    sendContact: function(){
      this.$v.$touch()
      console.log("this.$v.$invalid", this.$v)
      if (this.contactFormValid){
        axios.post(contactRockfitAdminUrl, {
          contact_info: this.contactInfo
          })
        .then(function (response) {
          this.contactSent = true
          this.resetData()
        }.bind(this))
        .catch(function (error) {
          console.log(error);
        });
      }
       
    },
    sendCancelation: function(){
      this.$v.$touch()
      if (this.cancellationFormValid){
        axios.post(cancelRequestRockfitAdminUrl, {
            user_name: this.userName,
            user_code: this.userCode,
            user_document: this.userDocument,
            registration_date: this.registrationDate,
            registration_hour: this.registrationHour,
            registration_class: this.registrationClass,
            request_reason: this.requestReason
        })
        .then(function (response) {
          this.cancellationSent = true
          this.resetData()
        }.bind(this))
        .catch(function (error) {
          console.log(error);
        });
      }
    }
  },
  mounted: function(){
    console.log("$", this.$v)  
  }
})

window.topMenu = topMenu