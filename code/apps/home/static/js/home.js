Vue.use(VueCarousel.default)
var BeatLoader = VueSpinner.BeatLoader
var home = new Vue({
  el: '#home',
  delimiters: ['${', '}'],
  data: {
    sportCenters: [],
    sportCentersFilter: [],
    registerCompanyUrl: window.registerCompanyUrl,
    registerCenterUrl: window.registerCenterUrl,
    sportCentersForMap: [],
    activities: [],
    districts: [],
    selectedSportCenter: { lat: 0, lng: 0 },
    sportCenterInfoWindowOpened: false,
    sportCenterNameFilter: null,
    sportCenterDistrictFilter: null,
    sportCenterActivityFilter: null,
    sportCenterExclusiveFilter: false,
    sportCenterOpenStatusFilter: false,
    firstTime: true,
    showMoreQuestions: false,
    step: 1,
    stepCarousel: 0,
    dialogEnterpriseVisible: false,
    dialogCenterVisible: false,
    showFilter: false,
    headerHeight: 125,
    mapStyle: [
      {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "saturation": 36
          },
          {
            "color": "#000000"
          },
          {
            "lightness": 40
          }
        ]
      },
      {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "visibility": "on"
          },
          {
            "color": "#000000"
          },
          {
            "lightness": 16
          }
        ]
      },
      {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 20
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 17
          },
          {
            "weight": 1.2
          }
        ]
      },
      {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 20
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 21
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 17
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 29
          },
          {
            "weight": 0.2
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 18
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 16
          }
        ]
      },
      {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 19
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#0f0f0f"
          },
          {
            "lightness": 17
          }
        ]
      }
    ],
    company: {
      name: '',
      district: null,
      numEmployees: null,
      contactPerson: '',
      email: '',
      phone: ''
    },
    center: {
      name: '',
      district: '',
      contactPerson: '',
      email: '',
      phone: ''
    },
    districtsTotal: window.districts,
    employeesTotal: [
      { 'id': 1, 'name': '5-50' },
      { 'id': 2, 'name': '51-100' },
      { 'id': 3, 'name': '101-200' },
      { 'id': 4, 'name': '201-300' },
      { 'id': 5, 'name': '+300' }
    ],
    loading: false,
    dialogConfirmEnterpriseVisible: false,
    dialogConfirmCenterVisible: false
  },
  components: {
    BeatLoader: BeatLoader
  },
  validations: {
    company: {
      name: {
        required: requiredValidator
      },
      district: {
        required: requiredValidator
      },
      numEmployees: {
        required: requiredValidator
      },
      contactPerson: {
        required: requiredValidator
      },
      phone: {
        integer: integerValidator
      },
      email: {
        email: emailValidator,
        required: requiredValidator
      }
    },
    center: {
      name: {
        required: requiredValidator
      },
      district: {
        required: requiredValidator
      },
      contactPerson: {
        required: requiredValidator
      },
      phone: {
        integer: integerValidator
      },
      email: {
        email: emailValidator,
        required: requiredValidator
      }
    }
  },
  watch: {
    sportCenterNameFilter: function (newValue, oldValue) {
      this.loadCenters(this.filters)
    },
    sportCenterDistrictFilter: function (newValue, oldValue) {
      this.loadCenters(this.filters)
    },
    sportCenterActivityFilter: function (newValue, oldValue) {
      this.loadCenters(this.filters)
    },
    sportCenterOpenStatusFilter: function (newValue, oldValue) {
      this.loadCenters(this.filters)
    },
    sportCenterExclusiveFilter: function (newValue, oldValue) {
      this.loadCenters(this.filters)
    }
  },
  computed: {
    filters: function () {
      return {
        name: this.sportCenterNameFilter ? this.sportCenterNameFilter.value : null,
        ubigeo: this.sportCenterDistrictFilter ? this.sportCenterDistrictFilter.value : null,
        activity: this.sportCenterActivityFilter ? this.sportCenterActivityFilter.label : null,
        exclusive: this.sportCenterExclusiveFilter ? "exclusive" : "all",
        status: this.sportCenterOpenStatusFilter
      }
    },
    sportCentersForDropdown: function () {
      var values = window._.map(function(element){
        return {'label': element.name, 'value': element.value}
      })(this.sportCentersFilter)
      return values
    },
    activitiesForDropdown: function () {
      var values = window._.map(function (element) {
        return { 'label': element.name, 'value': element.id }
      })(this.activities)
      return values
    },
    districtsForDropdown: function () {
      var values = window._.map(function (element) {
        return { 'label': element.name, 'value': element.id }
      })(this.districts)
      return values
    }
  },
  methods: {
    setPlaceholder: function(){
      console.log(1231)
    },
    registerCompany: function () {
      this.loading = true
      this.$v.company.$touch();
      if (this.$v.company.$invalid) {
        this.loading = false
        return;
      }
      axios.get(registerCompanyUrl, {
        params: this.company
      })
        .then(function(response){
          this.loading = false
          this.dialogEnterpriseVisible = false
          this.dialogConfirmEnterpriseVisible = true
        }.bind(this))
    },
    registerCenter: function () {
      this.loading = true
      this.$v.center.$touch();
      if (this.$v.center.$invalid) {
        this.loading = false
        return;
      }
      axios.get(registerCenterUrl, {
        params: this.center
      })
        .then(function(response){
          this.loading = false
          this.dialogCenterVisible = false
          this.dialogConfirmCenterVisible = true
        }.bind(this))
    },
    goToPlans: function () {
      var section = $("#prices-section").offset().top - this.headerHeight
      $('html,body').scrollTo(section, { duration: 500 });
    },
    goToSecondSection: function () {
      var section = $("#home-complementa").offset().top - this.headerHeight
      $('html,body').scrollTo(section, { duration: 500 });
    },
    gotoSportCenters: function () {
      var section = $("#home-sport-centers-section").offset().top - this.headerHeight
      $('html,body').scrollTo(section, { duration: 500 });
    },
    onClickDistrict: function (district) {
      this.sportCenterDistrictFilter = district
    },
    loadCenters: function (filters) {
      axios.get(sportCenterListUrl, { params: filters })
        .then(function (response) {
          if (this.firstTime) {
            this.firstTime = false
            this.sportCenters = response.data
          }
          this.sportCentersForMap = response.data
        }.bind(this))
        .catch(function (error) {
          console.log(error);
        });
    },
    loadCentersFilter: function(){
      axios.get(sportCenterFilterUrl)
      .then(function (response) {
        this.sportCentersFilter = response.data  
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    loadActivities: function () {
      axios.get(categoryActivitiesUrl, {})
        .then(function (response) {
          this.activities = response.data
        }.bind(this))
        .catch(function (error) {
          console.log(error);
        });
    },
    loadDistricts: function () {
      axios.get(sportCentersDistrictsUrl, {})
        .then(function (response) {
          this.districts = response.data
        }.bind(this))
        .catch(function (error) {
          console.log(error);
        });
    },
    getMarkerPosition: function (sportCenter) {
      var position = {
        'lat': parseFloat(sportCenter.latitude),
        'lng': parseFloat(sportCenter.longitude)
      }
      return position
    },
    selectSportCenter: function (sportCenter) {
      this.selectedSportCenter = sportCenter
    },
    mouseOverMarker: function (sportCenter) {
      var infoWindow = this.$refs['sportCenterInfoWindow' + sportCenter.id][0]
      infoWindow.opened = true
      infoWindow._openInfoWindow()
    },
    mouseOutMarker: function (sportCenter) {
      var infoWindow = this.$refs['sportCenterInfoWindow' + sportCenter.id][0]
      infoWindow.opened = true
      infoWindow.$infoWindowObject.close()
    },
    setStep: function (value) {
      this.step = value;
    },
    getWindowSize: function (event) {
      if (window.innerWidth > 992) {
        this.headerHeight = 125
      } else {
        this.headerHeight = 0
      }
    },
    openSportCenterModal: function () {
      this.dialogCenterVisible = true
    }
  },
  mounted: function () {
    this.loadCenters({})
    this.loadCentersFilter()
    this.loadActivities()
    this.loadDistricts()
    window.addEventListener('resize', this.getWindowSize);
    // this.$nextTick(function(){
    this.getWindowSize()
    // }.bind(this))
    this.districtsTotal = _.map(function (d) {
      var dict = {
        value: d.id,
        label: d.name
      }
      return dict
    })(this.districtsTotal)

    this.employeesTotal = _.map(function (d) {
      var dict = {
        value: d.id,
        label: d.name
      }
      return dict
    })(this.employeesTotal)
  }
})

window.home = home