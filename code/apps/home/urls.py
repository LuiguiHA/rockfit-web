from django.urls import path, re_path

from . import views

urlpatterns = [
    re_path('^$', views.Home.as_view()),
    re_path('^register-company$', views.RegisterCompany.as_view(), name="register-company"),
    re_path('^register-center$', views.RegisterCenter.as_view(), name="register-center"),
]