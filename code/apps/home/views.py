from django.shortcuts import render
from django.views import View
from config.models import Config
from ubigeo.models import Ubigeo
from plans.models import Plan
from sport_centers.models import CompanyContact, SportCenterContact
from django.conf import settings
from django.http import JsonResponse
import json
import arrow
# Create your views here.
class Home(View):
    def get(self, request):
        config = Config.objects.first()
        plans = Plan.objects.all().order_by("order")
        quantity_plans = len(plans.filter(is_related=False))
        user_plan = None
        districts = list(Ubigeo.objects.filter(parent_id=1501).values('id','name'))
        if request.user.is_authenticated:
            now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc').datetime
            user_plan = request.user.current_plan.filter(
                expires_at__gte=now,
            ).first()

        return render(request, 'home.html', {
            'config': config,
            'plans': plans,
            'quantity_plans': quantity_plans,
            'active': 1,
            'user_plan': user_plan,
            'districts': districts
             
        })


class RegisterCompany(View):
    def get(self, request):
        data = request.GET
        district = json.loads(data.get('district'))
        district = district.get('label')
        name = data.get('name')
        employees = int(data.get('numEmployees'))
        contact = data.get('contactPerson')
        email = data.get('email').lower()
        phone = data.get('phone')

        company = CompanyContact()
        company.name = name
        company.district = district
        company.num_employees = employees
        company.contact_person = contact
        company.email = email
        company.phone = phone
        company.save()
        return JsonResponse({'success': True})


class RegisterCenter(View):
    def get(self, request):
        data = request.GET
        district = json.loads(data.get('district'))
        district = district.get('label')
        name = data.get('name')
        contact = data.get('contactPerson')
        email = data.get('email').lower()
        phone = data.get('phone')

        center = SportCenterContact()
        center.name = name
        center.district = district
        center.contact_person = contact
        center.email = email
        center.phone = phone
        center.save()
        return JsonResponse({'success': True})