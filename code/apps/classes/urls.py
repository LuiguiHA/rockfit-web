from django.urls import path, re_path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    re_path('^$', views.ClassesView.as_view(), name="classes"),
    re_path('^activities$', views.ActivitiesView.as_view(), name="activities"),
    re_path('^confirm-reservation$', views.ConfirmReservationView.as_view(), name="confirm-reservation"),
    re_path('^cancel-reservation$', views.CancelReservationView.as_view(), name="cancel-reservation"),
    re_path('^assisted-reservation$', views.AssistedReservationView.as_view(), name="assisted-reservation"),
    re_path('^tag-classes$', views.TagClassesView.as_view(), name="tag-classes"),
]