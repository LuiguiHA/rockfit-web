from django.utils.decorators import method_decorator
from django.db import connection
from django.db.models import Sum
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect
from django.views import View
from django.db import transaction
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.contrib import messages
from django.db.models import Q
from django.template import defaultfilters
from django.template.loader import render_to_string
from django.utils import formats
from project.error_codes import CODES
from ubigeo.models import Ubigeo
from plans.models import Plan
from sport_centers.models import SportCenter, SportCenterActivitySchedule, SportCenterActivity, SportCenterActivityTag
from config.models import Config
from users.models import UserCard, UserPlan, UserPendingPlan, \
    UserPayment, UserPass, UserCode, UserUsedCoupon, UserActivityReservation, UserActivityReservationFriendpass
from project.utils import dictfetchall
from users.utils import get_repetitions, get_centers_allowed
import json
import arrow
import datetime

# Create your views here.
class ClassesView(View):
    def get(self, request):
        data = request.GET
        sport_center = data.get('sport_center') if data.get('sport_center') else 0  
        return render(request, 'classes.html', {'active': 2, 'sport_center': sport_center})

class TagClassesView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(TagClassesView, self).dispatch(*args, **kwargs)

    def get(self, request):
        classes = SportCenterActivity.objects.all()
        tags = []
        for clase in classes:
            classes_tags = list(clase.tag.all().values('id', 'name', 'synonymous'))
            for tag in classes_tags:
                if tag not in tags:
                    tags.append(tag)
        
        return JsonResponse(tags, safe=False)

class ActivitiesView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ActivitiesView, self).dispatch(*args, **kwargs)

    def post(self, request):
        data = json.loads(request.body)
        day = data.get("day")
        start_at = data.get("start_at")
        end_at = data.get("end_at")
        with_instructor = data.get("with_instructor")
        districts = data.get("districts")
        # classes = data.get("classes")
        tag_names = data.get("classes")
        sport_centers = data.get("sport_centers")
        now_hour = arrow.now(settings.PROJECT_TIME_ZONE).format("HH:mm")
        now = arrow.now(settings.PROJECT_TIME_ZONE)
        # and to_timestamp(%s || ' ' || (start_at + ((DATE_PART('hour', end_at - start_at) * 60 + DATE_PART('minute', end_at - start_at)) / 2) * interval '1 minute'), 'YYYY-MM-DD HH24:MI') >= to_timestamp(date(now()) || ' ' || %s, 'YYYY-MM-DD HH24:MI')
        # and case when %s is null then 1 = 1 else sport_centers_sportcenteractivity.name in %s end
        with connection.cursor() as cursor:
            cursor.execute(
                """
                select
                sport_centers_sportcenter.id as sport_center_id,
                sport_centers_sportcenter.ask_weight as ask_weight,
                sport_centers_sportcenter.name as sport_center_name, 
                sport_centers_sportcenter.is_premium as sport_center_premium,
                sport_centers_sportcenter.logo as sport_center_logo,
                sport_centers_sportcenter.address as sport_center_address,
                sport_centers_sportcenteractivityschedule.start_at,
                sport_centers_sportcenteractivityschedule.end_at,
                sport_centers_sportcenteractivity.id as activity_id,
                sport_centers_sportcenteractivity.name as activity_name,
                sport_centers_sportcenteractivity.description as description,
                sport_centers_sportcenteractivityschedule.id as activity_schedule_id,
                sport_centers_sportcenteractivity.has_teacher,
                sport_centers_sportcenteractivity.teacher_name,
                sport_centers_sportcenteractivity.image,
                sport_centers_sportcenteractivity.teacher_image
                from sport_centers_sportcenteractivityschedule
                inner join sport_centers_sportcenteractivity on sport_centers_sportcenteractivity.id = sport_centers_sportcenteractivityschedule.activity_id
                inner join sport_centers_sportcenter on sport_centers_sportcenter.id = sport_centers_sportcenteractivity.sport_center_id
                left join sport_centers_sportcenterscheduleexclusion on sport_centers_sportcenterscheduleexclusion.sport_center_id = sport_centers_sportcenter.id 
                left join users_useractivityreservation on users_useractivityreservation.activity_schedule_id = sport_centers_sportcenteractivityschedule.id
                where case when extract(dow from date(%s)) = 0 then sport_centers_sportcenteractivityschedule.day_of_week = 7 else sport_centers_sportcenteractivityschedule.day_of_week = extract(dow from date(%s)) end
                and to_timestamp(%s || ' ' || (start_at +  interval '15 minute'), 'YYYY-MM-DD HH24:MI') >= to_timestamp(date(now()) || ' ' || %s, 'YYYY-MM-DD HH24:MI')
                and start_at >= %s and end_at <= %s
                and case when %s is null then 1 = 1 else sport_centers_sportcenteractivity.has_teacher = %s end
                and case when sport_centers_sportcenterscheduleexclusion.day is null then 1 = 1 else sport_centers_sportcenterscheduleexclusion.day != %s end
                and case when %s is null then 1 = 1 else sport_centers_sportcenter.ubigeo_id in %s end
                and case when %s is null then 1 = 1 else sport_centers_sportcenteractivity.sport_center_id in %s end
                and users_useractivityreservation.deleted is null
                and sport_centers_sportcenter.deleted is null
                and sport_centers_sportcenteractivity.deleted is null
                and sport_centers_sportcenteractivityschedule.deleted is null
                group by 
                sport_centers_sportcenter.id, 
                sport_centers_sportcenteractivityschedule.start_at,
                sport_centers_sportcenteractivityschedule.end_at,
                sport_centers_sportcenteractivity.id,
                sport_centers_sportcenteractivity.name,
                sport_centers_sportcenteractivity.description,
                sport_centers_sportcenteractivityschedule.id,
                sport_centers_sportcenteractivity.has_teacher,
                sport_centers_sportcenteractivity.teacher_name,
                sport_centers_sportcenteractivity.image,
                sport_centers_sportcenteractivity.teacher_image
                order by sport_centers_sportcenteractivityschedule.start_at, sport_centers_sportcenteractivityschedule.end_at
                """, [
                    day, 
                    day, 
                    day, 
                    now_hour,
                    start_at, 
                    end_at, 
                    with_instructor,
                    with_instructor,
                    day,
                    districts,
                    tuple(districts) if districts else (0,),
                    sport_centers,
                    tuple(sport_centers) if sport_centers else (0,),
                ]
            )

            classes = dictfetchall(cursor)
            user_plan = None
            date = datetime.datetime.now()
            # ETIQUETAS
            if tag_names:
                tag_names = [name.strip() for name in tag_names]
                tags = list(SportCenterActivityTag.objects.filter(name__in=tag_names).values_list('id', flat=True))
            classes_final = []
            
            for clase in classes:
                valid = True
                if tag_names:
                    activity = SportCenterActivity.objects.get(id=clase.get('activity_id'))
                    activity_tags = list(activity.tag.all().values_list('id', flat=True))
                    activity_tags_selected = [a for a in activity_tags if a in tags]
                    if len(activity_tags_selected) == 0:
                        valid = False
                if valid:
                    schedule = SportCenterActivitySchedule.objects.get(id=clase.get('activity_schedule_id'))
                    reservations_count = UserActivityReservation.objects.filter(activity_schedule=clase.get('activity_schedule_id'), cancelled=False, reservation_day=day).count()
                    available_count  = schedule.max_users_allowed - reservations_count
                    available_count  = 0 if available_count < 0 else available_count
                    clase['available_count'] = available_count
                    clase['inviteds'] = 0
                    if request.user.is_authenticated:
                        reservation = UserActivityReservation.objects.filter(activity_schedule=clase.get('activity_schedule_id'), cancelled=False, reservation_day=day, related_pass__user=request.user).first()
                        # users_ids = [ reserve.related_pass.user.id for reserve in reservations]
                        if reservation:
                            inviteds = UserActivityReservationFriendpass.objects.filter(user_activity_reservation=reservation).count()
                            clase['weight'] = reservation.weight
                            clase['inviteds'] = inviteds
                            clase['is_reservated'] = True
                            # start = date.replace(
                            #     hour=clase['start_at'].hour,
                            #     minute=clase['start_at'].minute,
                            #     second=clase['start_at'].second
                            # )
                            # now_date = date + datetime.timedelta(hours=-5)
                            # difference = (now_date - start).total_seconds() / 60
                            # clase['show_enter'] = False
                            # if difference >= -10:
                            #     clase['show_enter'] = True
                        else:
                            clase['is_reservated'] = False
                    else:
                        clase['is_reservated'] = False
                    
                    classes_final.append(clase)

            
            allowed_centers_by_pass = []
            if request.user.is_authenticated:
                user_plan = request.user.current_plan.first()

                allowed_centers_by_pass = UserPass.objects.filter(
                    buy_reference=settings.PASS,
                    expires_at__gte=now.datetime,
                    used=False,
                    user=request.user
                ).values("sport_center_id")

                allowed_centers_by_pass = [x.get("sport_center_id") \
                    for x in allowed_centers_by_pass]

            allowed_centers_by_plan = []

            have_plan_premium = False

            if user_plan:
                cursor.execute(
                    """
                    select 
                    sport_centers_sportcenteractivity.sport_center_id
                    from users_useractivityreservation
                    inner join users_userpass on users_userpass.id = users_useractivityreservation.related_pass_id
                    inner join users_userplan on users_userplan.id = users_userpass.plan_id
                    inner join sport_centers_sportcenteractivityschedule on sport_centers_sportcenteractivityschedule.id = users_useractivityreservation.activity_schedule_id
                    inner join sport_centers_sportcenteractivity on sport_centers_sportcenteractivity.id = sport_centers_sportcenteractivityschedule.activity_id
                    where plan_id = %s
                    and users_useractivityreservation.cancelled = False
                    group by sport_centers_sportcenteractivity.sport_center_id
                    having count(sport_centers_sportcenteractivity.sport_center_id) = %s
                    """, [user_plan.id, user_plan.repetitions_quantity])

                consumed_sport_centers = cursor.fetchall()

                if user_plan.is_premium:
                    plan_sport_centers = SportCenter.objects.all().values("id")
                else:
                    plan_sport_centers = SportCenter.objects.filter(is_premium=False).values("id")

                plan_sport_centers = [x.get("id") for x in plan_sport_centers]

                consumed_sport_centers = [ cs[0] for cs in consumed_sport_centers]

                allowed_centers_by_plan = list(set(plan_sport_centers) - set(consumed_sport_centers))

                have_plan_premium = user_plan.is_premium

                # used_plan_passes = request.user.passes.filter(
                #     used=False
                # ).count()
                used_plan_passes = request.user.passes.filter(
                    Q(used=False),
                    Q(buy_reference='PLAN_PASS'),
                    Q(plan__isnull=False),
                    Q(reservation_passes__isnull=True) | Q(reservation_passes__isnull=False, reservation_passes__cancelled=True)
                ).count()

                if used_plan_passes == 0:
                    allowed_centers_by_plan = []

            return JsonResponse({
                "success": True,
                "classes": classes_final,
                "have_pass": len(allowed_centers_by_pass) > 0,
                "have_plan": user_plan is not None,
                "allowed_centers_by_pass": allowed_centers_by_pass,
                "allowed_centers_by_plan": allowed_centers_by_plan,
                "have_plan_premium": have_plan_premium
            })


class ConfirmReservationView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ConfirmReservationView, self).dispatch(*args, **kwargs)

    def post(self, request):
        data = json.loads(request.body)
        activity_schedule_id = data.get("activity_schedule_id")
        date = data.get("date")
        weight = data.get('weight')
        
        activity_schedule = SportCenterActivitySchedule.objects.get(pk=activity_schedule_id)

        sport_center = activity_schedule.activity.sport_center

        now = arrow.now(settings.PROJECT_TIME_ZONE)

        # VALIDAMOS SI EL HORARIO DE CLASE ES MENOR A LA HORA ACTUAL
        schedule_date =  arrow.get(date).replace(hour=activity_schedule.start_at.hour, minute=activity_schedule.start_at.minute, tzinfo=settings.PROJECT_TIME_ZONE).shift(minutes=15)
        if schedule_date <= now:
            return JsonResponse({
                "error": True,
                **CODES["RESERVATIONS"]["RESERVATION_AFTER_HOURS"],
            })
        
        # VALIDAMOS LA CANTIDAD DE CUPOS DISPONIBLES
        reservations = list(UserActivityReservation.objects.filter(activity_schedule=activity_schedule, cancelled=False, reservation_day=date))
        count_reservations = len(reservations)
        available_count = activity_schedule.max_users_allowed - count_reservations
        if available_count == 0:
            return JsonResponse({
                "error": True,
                **CODES["RESERVATIONS"]["NOT_COUNT_AVAILABLE"],
            })

        # VALIDAMOS SI YA ESTA RESERVADO EN EL HORARIO SELECCIONADO
        users_ids = [ reserve.related_pass.user.id for reserve in reservations]
        if request.user.id in users_ids:
            return JsonResponse({
                "error": True,
                **CODES["RESERVATIONS"]["RESERVATION_EXISTS"],
            })

        # MIS PASES
        passes_not_from_plan = None

        passes_not_from_plan = UserPass.objects.filter(
            Q(buy_reference=settings.PASS), 
            Q(expires_at__gte=now.datetime),
            Q(sport_center=sport_center),
            Q(used=False),
            Q(user=request.user),
            Q(assisted_at__isnull=True),
            Q(reservation_passes__isnull=True)| Q(reservation_passes__isnull=False, reservation_passes__cancelled=True)
        ).order_by("-expires_at")

        user_plan = request.user.current_plan.filter(
            expires_at__gte=now.datetime,
        ).first()

        count_passes = UserPass.objects.filter(
                        Q(buy_reference=settings.PASS), 
                        Q(expires_at__gte=now.datetime),
                        Q(sport_center=sport_center),
                        Q(used=False),
                        Q(user=request.user),
                        Q(assisted_at__isnull=True),
                        Q(reservation_passes__isnull=True)| Q(reservation_passes__isnull=False, reservation_passes__cancelled=True)
                    ).count()

        passes_to_use_from_plan = []

        # MIS PASES DE MI PLAN
        is_available = True
        if user_plan:
            if user_plan.is_freeze:
                start_freeze = arrow.get(user_plan.freeze_day).shift(hours=-5)
                end_freeze = start_freeze.shift(days=10)
                if schedule_date >= start_freeze and schedule_date <=end_freeze:
                    is_available = False
            if is_available:
                passes_to_use_from_plan = user_plan.plan_passes.filter(
                    Q(used=False),
                    Q(buy_reference='PLAN_PASS'),
                    Q(plan__isnull=False),
                    Q(reservation_passes__isnull=True) | Q(reservation_passes__isnull=False, reservation_passes__cancelled=True)
                )

        all_passes = list(passes_to_use_from_plan) + list(passes_not_from_plan)
        pass_to_use = list(sorted(all_passes, key=lambda x: x.expires_at))
        
        pass_id = None
        centers_allowed = []
        if len(pass_to_use) > 0:
            user_pass = None
            if user_plan:
                centers_allowed = get_centers_allowed(user_plan)
            if sport_center.id in centers_allowed:
                # VALIDAMOS SI LA RESERVA ES DE UN CENTRO DEPORTIVO PREMIUM Y SI TENGO PASES PARA ESE CENTRO O MI PLAN ME LO PERMITE
                if sport_center.is_premium:
                    for pase in pass_to_use:
                        if pase.buy_reference == 'PASS' and pase.sport_center.id == sport_center.id:
                            user_pass = pase
                            break
                        if pase.buy_reference == 'PLAN_PASS' and pase.plan.is_premium:
                            user_pass = pase
                            break
                    if user_pass is None:
                        return JsonResponse({
                            "error": True,
                            **CODES["RESERVATIONS"]["CENTER_NOT_ALLOWED"],
                        })
                else:
                    user_pass = pass_to_use[0]
            else:
                passes_not_plan = list(passes_not_from_plan)
                if len(passes_not_plan) > 0:
                    user_pass = passes_not_plan[0]
                else:
                    return JsonResponse({
                        "error": True,
                        **CODES["RESERVATIONS"]["CENTER_NOT_PERMITTED"],
                        'quantity_reservations': user_plan.repetitions_quantity,
                        'days_to_renew': user_plan.days_to_renew
                    })
            
            # VALIDAMOS SI LA FECHA ESTA DENTRO DE LA VIGENCIA DE MI PLAN O DE MIS PASES
            reservation_day_available = False
            for pase in pass_to_use:
                pase_date = arrow.get(pase.expires_at).shift(hours=-5).replace(tzinfo=settings.PROJECT_TIME_ZONE)
                if pase_date >  schedule_date:
                    reservation_day_available = True
                    break
            if reservation_day_available is False:
                return JsonResponse({
                    "error": True,
                    **CODES["RESERVATIONS"]["RESERVATION_NOT_AVAILABLE"],
                })
            
            # VALIDAMOS SI EL HORARIO SE CRUZA CON OTRA RESERVA
            schedule_is_crossed = False
            my_reservations = list(UserActivityReservation.objects.filter(cancelled=False, reservation_day=date, related_pass__user=request.user))
            for reserva in my_reservations:
                if activity_schedule.start_at == reserva.activity_schedule.start_at:
                    schedule_is_crossed = True
                    break
                if activity_schedule.start_at < reserva.activity_schedule.start_at:
                    if activity_schedule.end_at <= reserva.activity_schedule.start_at:
                        pass
                    else:
                        schedule_is_crossed = True
                        break
                if activity_schedule.start_at > reserva.activity_schedule.start_at:
                    if activity_schedule.start_at >= reserva.activity_schedule.end_at:
                        pass
                    else:
                        schedule_is_crossed = True
                        break
            if schedule_is_crossed:
                return JsonResponse({
                    "error": True,
                    **CODES["RESERVATIONS"]["SCHEDULE_CROSSED"],
                })

            user_pass = user_pass if user_pass else pass_to_use[0]

            current_reservations = UserActivityReservation.objects.filter(
                activity_schedule_id=activity_schedule_id,
                reservation_day=date,
                cancelled=False
            ).count()

            sport_center_activity_schedule =  SportCenterActivitySchedule.objects.get(
                pk=activity_schedule_id
            )

            available_count_activity = sport_center_activity_schedule.max_users_allowed

            if available_count_activity - current_reservations > 0:
                user_activity_reservation = UserActivityReservation()
                user_activity_reservation.related_pass_id = user_pass.id
                user_activity_reservation.activity_schedule_id = activity_schedule_id
                user_activity_reservation.reservation_day = date
                user_activity_reservation.weight = weight
                user_activity_reservation.save()

                class_datetime = arrow.get(date, "YYYY-MM-DD").replace(
                    hour=sport_center_activity_schedule.start_at.hour,
                    minute=sport_center_activity_schedule.start_at.minute,
                    second=sport_center_activity_schedule.start_at.second,
                    tzinfo=settings.PROJECT_TIME_ZONE
                )

                diff = class_datetime - now
                seconds = diff.seconds
                hours, seconds = divmod(seconds, 3600)

                # locale.setlocale(locale.LC_ALL, settings.LANGUAGE_CODE)

                email_template = render_to_string(
                    "mails/reservation_confirmation.html",
                    {
                        "name": request.user.first_name,
                        "class_name": sport_center_activity_schedule.activity.name,
                        "class_date": formats.date_format(class_datetime.datetime),
                        "class_time": "{} - {}".format(
                            sport_center_activity_schedule.start_at.strftime("%I:%M %p"),
                            sport_center_activity_schedule.end_at.strftime("%I:%M %p")
                        ),
                        "class_sportcenter": sport_center.name,
                        "class_address": "{} {}".format(sport_center.address, sport_center.address_reference) ,
                        "client_code": request.user.codes.first().code,
                        "class_can_be_cancelled": hours >= 4,
                    }
                )

                print("email_template", email_template)


                return JsonResponse({
                    "success": True,
                })

            else:
                return JsonResponse({
                    "error": True,
                    **CODES["RESERVATIONS"]["NOT_AVAILABLE_CLASSES"],
                })

        else:
            if is_available:
                return JsonResponse({
                    "error": True,
                    **CODES["RESERVATIONS"]["NOT_AVAILABLE_PASSES"],
                })
            else:
                return JsonResponse({
                    "error": True,
                    **CODES["RESERVATIONS"]["FREEZE_PLAN"],
                    "date_freeze": user_plan.freeze_day.strftime('%Y-%m-%d')
                })


class CancelReservationView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CancelReservationView, self).dispatch(*args, **kwargs)

    def post(self, request):
        data = json.loads(request.body)
        schedule_id = data.get("activity_schedule_id")
        day = data.get('date')
        now = arrow.now(settings.PROJECT_TIME_ZONE)
        # VALIDAMOS SI LA RESERVA SE PUEDE CANCELAR
        schedule = SportCenterActivitySchedule.objects.get(id=schedule_id)
        schedule_date =  arrow.get(day).replace(hour=schedule.start_at.hour, minute=schedule.start_at.minute, tzinfo=settings.PROJECT_TIME_ZONE).shift(hours=-4)
        
        if now < schedule_date:
            reservation = UserActivityReservation.objects.filter(activity_schedule__id=schedule_id, cancelled=False, related_pass__user=request.user, reservation_day=day).first()
            reservation.cancelled = True
            # reservation.related_pass = None
            reservation.save()
            return JsonResponse({'success': True})

        return JsonResponse({'success': False})


class AssistedReservationView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(AssistedReservationView, self).dispatch(*args, **kwargs)
    
    def post(self, request):
        data = json.loads(request.body)
        schedule_id = data.get("activity_schedule_id")
        day = data.get('date')
        now = arrow.now(settings.PROJECT_TIME_ZONE)
        schedule = SportCenterActivitySchedule.objects.get(id=schedule_id)
        start = now.replace(hour=schedule.start_at.hour, minute=schedule.start_at.minute, tzinfo=settings.PROJECT_TIME_ZONE)
        end = now.replace(hour=schedule.end_at.hour, minute=schedule.end_at.minute, tzinfo=settings.PROJECT_TIME_ZONE)
        time_extra = ( (end - start).total_seconds() / 60 ) / 2
        new_date = start.shift(minutes=time_extra)
        if now <= new_date:
            reservation = UserActivityReservation.objects.filter(activity_schedule__id=schedule_id, cancelled=False, related_pass__user=request.user, reservation_day=day).first()
            related_pass = reservation.related_pass
            related_pass.used = True
            related_pass.assisted_at = now.datetime
            related_pass.save()
            return JsonResponse({'success': True})

        return JsonResponse({'success': False})