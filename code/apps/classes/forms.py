from django import forms
from sport_centers.models import SportCenterActivity
from django.conf import settings
from tinymce.widgets import TinyMCE
from .models import *

class SportCenterActivityForm(forms.ModelForm):
    description = forms.CharField(
        label="Descripción de la clase",
        widget=TinyMCE(attrs={'cols': 80, 'rows': 30})
    )
    def __init__(self, *args, **kwargs):

        super(SportCenterActivityForm, self).__init__(*args, **kwargs)

        if self.instance.pk:
            self.initial['description'] = self.instance.description

