from django.contrib import admin
from sport_centers.models import SportCenterActivity, SportCenterActivitySchedule
from django.forms import widgets
from django.shortcuts import redirect
from django.contrib import messages
from .forms import SportCenterActivityForm
from django.conf import settings
from .models import *
# Register your models here.
class SportCenterActivityScheduleInline(admin.StackedInline):
    model = SportCenterActivitySchedule
    extra = 0
@admin.register(SportCenterActivity)
class SportCenterActivityAdmin(admin.ModelAdmin):

    class Media:
        js = (settings.STATIC_URL + 'admin/js/activity_admin.js',)
        css = {
            "all": (settings.STATIC_URL + 'admin/css/activity_admin.css',)
        }

    form = SportCenterActivityForm
    model = SportCenterActivity
    list_display = ["name", "get_teacher_name", "sport_center"]
    list_filter = ["sport_center", "teacher_name"]
    search_fields = ['name', 'sport_center']
    fields = ["name", "image", "has_teacher", "teacher_name", "teacher_image", "sport_center"]
    extra = 0

    fields = []
    inlines = [SportCenterActivityScheduleInline]



    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}

        if object_id is not None:
            extra_context['show_duplicate'] = True

        if request.method == 'POST' and '_duplicate' in request.POST:
            sport_center_activity = SportCenterActivity.objects.get(pk=object_id)

            sportcenteractivityschedule_set = sport_center_activity.sportcenteractivityschedule_set.all()

            sport_center_activity.id = None
            sport_center_activity.save()

            for new_sport_center_activity in sportcenteractivityschedule_set:
                new_sport_center_activity.id = None
                new_sport_center_activity.activity = sport_center_activity
                new_sport_center_activity.save()

            self.message_user(request, "Clase duplicada exitosamente", messages.SUCCESS)

            return redirect("admin:sport_centers_sportcenteractivity_change", sport_center_activity.id)
        else:
            return super(SportCenterActivityAdmin, self).changeform_view(request, object_id, extra_context=extra_context)