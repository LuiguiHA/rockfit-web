var app = new Vue({
  el: '#classes',
  delimiters: ['${', '}'],
  data: {
    // classInfo: {},
    startDay: "",
    // selectedDay: "",
    selectedTime: [6, 23],
    selectedInstructor: [],
    selectedDistricts: [],
    selectedActivities: [],
    selectedSportCenters: [],
    classes:[],
    allowedCentersByPass:[],
    allowedCentersByPlan:[],
    havePass:false,
    havePlan:false,
    havePlanPremium:false,
    daysToShow: 7,
    withInstructor: null,
    sportCenters:[],
    activities:[],
    districts:[],
    reservation: {},
    searchSportCenterText: "",
    expandedFilters: [],
    showHideFiltersText:"Mostrar filtros",
    searchDistrictText: "",
    showFilters: true,
    filtersCollapsedFirstTime: false,
    sportCenter: window.sportCenter,
    tagClassesUrl: window.tagClassesUrl,
    tags: [],
    searchTagText: '',
    swiperOptionA: {
      slidesPerView: 7,
      slidesPerGroup: 7,
      loopFillGroupWithBlank: true,
      // spaceBetween: 60,
      pagination: {
        el: '.swiper-pagination'
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      breakpoints: {
        1200: {
          slidesPerView: 5,
          slidesPerGroup: 5,
          spaceBetween: 10
        },
        900: {
          slidesPerView: 4,
          slidesPerGroup: 4,
          spaceBetween: 10
        },
        768: {
          slidesPerView: 5,
          slidesPerGroup: 5,
          spaceBetween: 10
        },
        640: {
          slidesPerView: 3,
          slidesPerGroup: 3,
          spaceBetween: 5
        },
        480: {
          slidesPerView: 2,
          slidesPerGroup: 2,
          spaceBetween: 5
        }
      }
   }
  },
  methods:{
    resize: function(){
      console.log('entro')
    },
    // openClassInfo: function(selectedClass){
    //   this.classInfo = selectedClass
    //   var classHourOpen = moment(selectedClass.start_at, "HH:mm:ss")
    //   var classHourClose = moment(selectedClass.end_at, "HH:mm:ss")
    //   this.classInfo.openAt = classHourOpen.format("hh:mm")
    //   console.log(this.classInfo.openAt)
    //   this.classInfo.closeAt = classHourClose.format("hh:mm a")
    //   this.$modal.show("class-info")
    //   return false
    // },
    showHideFilters: function(){
      this.showFilters = !this.showFilters
      this.$nextTick(function(){
        if(this.showFilters){
          this.showHideFiltersText = "Esconder filtros"
        } else {
          this.showHideFiltersText = "Mostrar filtros"
        }
      })
    },
    isGreaterThanNow: function(hour){
      var now = moment(window.today, "YYYY-MM-DD HH:mm:ss")
      var classHour = moment(this.selectedDay.split(" ")[0] + ' ' + hour, "YYYY-MM-DD HH:mm a")
      return classHour.isAfter(now)
    },
    hideConfirmationModal: function(){
      this.$modal.hide('reservation-confirmation')
      this.reservation = {}
      this.loadFilteredActivities()
    },
    confirmReservation: function(){
      this.$modal.show('reservation-confirmation')
      axios.post(confirmReservationUrl, {
        activity_schedule_id: this.reservation.activityScheduleId,
        date: this.selectedDay.split(" ")[0]
      })
      .then(function (response) {
        if(response.data.success){
          this.$modal.hide('reservation')
          this.$nextTick(function(){
            this.$modal.show('reservation-confirmation')
          }.bind(this))

        } else {
          var errorMessage = 'Hubo un error al realizar la reserva'
          if(response.data.code == 304){
            errorMessage = 'Lo sentimos, la reserva fue rechazada debido a que la clase no tiene cupos disponibles'
          }
          if(response.data.code == 305){
            errorMessage = 'Lo sentimos, la reserva fue rechazada debido a que no cuentas con pases disponibles'
          }
          
          new Noty({
            type: 'error',
            layout: 'topCenter',
            timeout: 2000,
            text: errorMessage,
            killer: true,
            force: true
          }).show();
        }
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    reserve: function(activityScheduleId){
      var activity = _.find(function(e){
        return e.activity_schedule_id == activityScheduleId 
      })(this.classes)
      moment.locale("es")
      var today = moment(this.startDay, "YYYY-MM-DD HH:mm:ss")
      var classHourOpen = moment(activity.start_at, "HH:mm:ss").format("hh:mm a")
      var classHourClose = moment(activity.end_at, "HH:mm:ss").format("hh:mm a")
      var reservation = {
        activityScheduleId: activityScheduleId,
        sportCenterName: activity.sport_center_name,
        sportCenterLogo:  activity.sport_center_logo,
        activityName: activity.activity_name,
        teacherName: activity.teacher_name,
        day: today.format("D [de] MMMM"),
        time: classHourOpen + " - " + classHourClose,
        sportCenterAddress: activity.sport_center_address,
      }
      this.reservation = reservation
      this.$nextTick(function(){
        this.$modal.show("reservation")
      }.bind(this))
      

    },
    loadCenters: function(filters){
      axios.get(sportCenterListUrl, {params:filters})
      .then(function (response) {
        this.sportCenters = response.data  
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    loadActivities: function(){
      axios.get(categoryActivitiesUrl, {})
      .then(function (response) {
        this.activities = response.data
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    loadDistricts: function(){
      axios.get(sportCentersDistrictsUrl, {})
      .then(function (response) {
        this.districts = response.data
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    showNextWeek:function(){
      var today = moment(this.startDay, "YYYY-MM-DD HH:mm:ss")
      this.startDay = today.clone().add(7, 'days').format("YYYY-MM-DD")
      this.selectedDay = this.startDay
      this.$nextTick(function(){
        this.loadFilteredActivities()
      })
    },
    showPrevWeek: function(){
      var today = moment(this.startDay, "YYYY-MM-DD HH:mm:ss")
      this.startDay = today.clone().subtract(7, 'days').format("YYYY-MM-DD")
      this.selectedDay = this.startDay
      this.$nextTick(function(){
        this.loadFilteredActivities()
      })
    },
    // loadActivitiesFromDay: function(day){
    //   this.selectedDay = day.format("YYYY-MM-DD")
    //   this.$nextTick(function(){
    //     this.loadFilteredActivities()
    //   }.bind(this))
    // },
    // loadFilteredActivities: _.debounce(500, function(){
    //   axios.post(activitiesUrl, this.filters)
    //   .then(function (response) {
    //     if(response.data.success){
          
    //       this.classes = response.data.classes
    //       this.allowedCentersByPass = response.data.allowed_centers_by_pass
    //       this.allowedCentersByPlan = response.data.allowed_centers_by_plan
    //       this.havePass = response.data.have_pass
    //       this.havePlan = response.data.have_plan
    //       this.havePlanPremium = response.data.have_plan_premium

    //     } else {
    //       new Noty({
    //         type: 'error',
    //         layout: 'topCenter',
    //         timeout: 2000,
    //         text: 'Hubo un error al cargar las actividades',
    //         killer: true,
    //         force: true
    //       }).show();
    //     }
        
    //   }.bind(this))
    //   .catch(function (error) {
    //     console.log(error);
    //   });
    // }),
    detectShowHideFilters: function(){
      var showHideFiltersIsVisible = $("#showHideFilters").is(":visible")
      if(showHideFiltersIsVisible){
        if(this.filtersCollapsedFirstTime == false){
          this.filtersCollapsedFirstTime = true
          this.expandedFilters = []
          this.showFilters = false
        }
      }
      return showHideFiltersIsVisible
    },
    loadTags: function(){
      axios.get(this.tagClassesUrl)
      .then(function (response) {
        this.tags = response.data
      }.bind(this))
    }
  },
  watch: {
    // selectedDistricts: function(){
    //   this.loadFilteredActivities()
    // },
    // selectedActivities: function(){
    //   this.loadFilteredActivities()
    // },
    // selectedSportCenters: function(){
    //   this.loadFilteredActivities()
    // },
    // selectedTime: function(){
    //   this.loadFilteredActivities()
    // },
    selectedInstructor: function(newValue){
      var withInstructor = _.find(function(e){
        return e.toLowerCase().indexOf("con") >= 0
      })(newValue) != undefined

      var withoutInstructor = _.find(function(e){
        return e.toLowerCase().indexOf("sin") >= 0
      })(newValue) != undefined

      if((withInstructor && withoutInstructor) || (withInstructor == false && withoutInstructor == false)){
        this.withInstructor = null
      }
      if(withInstructor && !withoutInstructor){
        this.withInstructor = true
      }
      if(!withInstructor && withoutInstructor){
        this.withInstructor = false
      }
      // this.loadFilteredActivities()
    }
  },
  computed:{
    tagsFiltered: function(){
      var tags = this.tags
      if(this.searchTagText != ""){
        var tags = _.filter(function(t){
          var find = false
          if(t.name.toLowerCase().indexOf(this.searchTagText.toLowerCase()) >= 0){
            find = true
          }
          if(find == false){
            _.forEach(function(s){
              if(s.toLowerCase().indexOf(this.searchTagText.toLowerCase()) >= 0){
                find = true
                return
              }
            }.bind(this))(t.synonymous)
          }
          return find == true
        }.bind(this))(this.tags)
      }
      return tags
    },
    sportCentersFiltered: function(){
      var sportCenters = this.sportCenters
      if(this.searchSportCenterText != ""){
        sportCenters = _.filter(function(e){
          return e.name.toLowerCase().indexOf(this.searchSportCenterText.toLowerCase()) >= 0
        }.bind(this))(this.sportCenters)
      }
      return sportCenters
    },
    districtsFiltered: function(){
      var districts = this.districts
      if(this.searchDistrictText != ""){
        districts = _.filter(function(e){
          return e.name.toLowerCase().indexOf(this.searchDistrictText.toLowerCase()) >= 0
        }.bind(this))(this.districts)
      }
      return districts
    },
    allowedSportCenters: function(){
      return _.uniq(
        this.allowedCentersByPass.concat(this.allowedCentersByPlan)
      )
    },
    sportCentersForDropdown: function(){
      var values = window._.map(function(element){
        return {'label': element.name, 'value': element.id}
      })(this.sportCenters)
      return values
    },
    activitiesForDropdown: function(){
      var values = window._.map(function(element){
        return {'label': element.name, 'value': element.id}
      })(this.activities)
      return values
    },
    districtsForDropdown: function(){
      var values = window._.map(function(element){
        return {'label': element.name, 'value': element.id}
      })(this.districts)
      return values
    },
    selectedTimeStart: function(){
      return _.padCharsStart(0)(2)(this.selectedTime[0]) + ":00"
    },
    selectedTimeEnd: function(){
      return _.padCharsStart(0)(2)(this.selectedTime[1]) + ":00"
    },
    // filters: function(){
    //   return {
    //     "day": this.selectedDay,
    //     "start_at": this.selectedTimeStart,
    //     "end_at": this.selectedTimeEnd,
    //     "with_instructor": this.withInstructor,
    //     "districts": (this.selectedDistricts.length > 0 ? this.selectedDistricts : null),
    //     "classes": (this.selectedActivities.length > 0 ? this.selectedActivities : null),
    //     "sport_centers": (this.selectedSportCenters.length > 0 ? this.selectedSportCenters : null),
    //   }
    // },
    // days: function(){
    //   var today = moment(window.today, "YYYY-MM-DD HH:mm:ss")
    //   var daysToGenerate = _.range(1, 31)
    //   return _.map(function(x){
    //     return today.clone().add(x - 1, 'days').clone()
    //   })(daysToGenerate)
    // }
  },  
  mounted: function(){
    // this.startDay = window.today
    // var today = moment(this.startDay, "YYYY-MM-DD HH:mm:ss")
    // this.startDay = today.clone().format("YYYY-MM-DD")
    // this.selectedDay = this.startDay
    // this.loadFilteredActivities()
    this.loadCenters({})
    this.loadDistricts()
    this.loadActivities()
    this.loadTags()
    this.detectShowHideFilters()
    if(this.sportCenter != 0){
      this.selectedSportCenters.push(this.sportCenter)
      this.expandedFilters = ['3']
    }
    window.addEventListener('resize', this.resize);
    document.getElementById("classes").style.display = "block";
  }
})