from django.apps import AppConfig


class RockfitConfig(AppConfig):
    name = 'rockfit'
