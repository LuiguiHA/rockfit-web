from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE, SOFT_DELETE_CASCADE
from django.utils.html import mark_safe
from django.contrib.postgres.fields import ArrayField
# Create your models here.


# class SportCenterCategory(SafeDeleteModel):

#     _safedelete_policy = SOFT_DELETE

#     name = models.CharField(
#         max_length=250,
#         verbose_name="Nombre",
#     )

class SportCenterCategoryService(SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE

    name = models.CharField(
        max_length=250,
        verbose_name="Nombre",
    )

    order = models.IntegerField()

class SportCenterCategory(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE
    name = models.CharField(
        max_length=250
    )
    
    def __str__(self):
        return self.name

class SportCenterGroup(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE
    name = models.CharField(
        max_length=250,
        unique=True
    )

    def __str__(self):
        return self.name

    class Meta():
        verbose_name = "Grupo"
        verbose_name_plural = "Grupos"


class SportCenter(SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE

    name = models.CharField(
        max_length=250,
        verbose_name="Nombre",
    )

    category = models.ForeignKey(
        SportCenterCategory,
        verbose_name="Categoría",
        on_delete=models.CASCADE,
        null=True
    )
    slug = models.SlugField(
        max_length=100,
        verbose_name="Slug"
    )

    description = models.TextField(
        verbose_name="Description"
    )

    speciality = models.CharField(
        max_length=250,
        verbose_name="Nombre",
        null=True
    )

    pass_amount = models.FloatField(
        verbose_name="Valor de pase",
        null=True,
        blank=True
    )

    sell_pass = models.BooleanField(
        verbose_name="Vende pase individual",
    )

    logo = models.ImageField(
        verbose_name="Nombre",   
        upload_to="sport_centers"
    )

    rate = models.DecimalField(
        max_digits=2,
        decimal_places=1,
        verbose_name="Estrellas",
    )

    total_califications = models.IntegerField(
        verbose_name="Calificaciones",
    )

    ubigeo = models.ForeignKey(
        'ubigeo.Ubigeo',
        verbose_name="Distrito",
        on_delete=models.CASCADE,
        limit_choices_to={'parent_id': 1501}
    )

    address_reference = models.CharField(
        max_length=500,
        verbose_name="Referencia",
    )

    address = models.CharField(
        max_length=250,
        verbose_name="Dirección",
    )

    latitude = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        null=True
    )

    longitude = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        null=True
    )

    is_premium = models.BooleanField(
        default=False, 
        verbose_name="Es premium"
    )

    email = models.CharField(
        max_length=250,
        verbose_name="Email",
        null=True,
        blank=True
    )

    phones = models.CharField(
        max_length=250,
        verbose_name="Teléfonos",
        null=True,
        blank=True
    )

    contact_person = models.CharField(
        max_length=250,
        verbose_name="Persona de contacto",
        null=True,
        blank=True
    )

    ask_weight = models.BooleanField(
        default=False,
        verbose_name="Requiere peso(kg)"
    )
    
    group = models.ForeignKey(
        SportCenterGroup,
        verbose_name="Grupo",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def sorted_media(self):
        return self.media.order_by('id').all()

    def __str__(self):
        return self.name
    
    def get_price(self):
        return "S/" + "{:.2f}".format(self.pass_amount) if self.pass_amount else "-"
    get_price.short_description="Precio"

    def get_is_premium(self):
        if self.is_premium:
            return mark_safe('<i class="fas fa-times-circle" style="color: red;"></i>')
        
        return mark_safe('<i class="fas fa-check-circle" style="color: #0ad10a"></i>')
    get_is_premium.short_description = "Es premium"

    class Meta():
        verbose_name = "Centro Deportivo"
        verbose_name_plural = "Centros Deportivos"


class SportCenterSuggested(SafeDeleteModel):
    sport_center = models.ForeignKey(
        SportCenter,
        on_delete=models.CASCADE,
        related_name="sport_centers_suggested",
        verbose_name="Centro deportivo"
    )
    sport_center_suggested = models.ForeignKey(
        SportCenter,
        on_delete=models.CASCADE,
        verbose_name="Centro deportivo sugerido"
    )


class SportCenterSchedule(SafeDeleteModel):
    
    day_of_week = models.IntegerField(
        verbose_name="Día de la semana"
    )

    open_at = models.TimeField(
        null=True,
        verbose_name="Abre a"
    )

    close_at = models.TimeField(
        null=True,
        verbose_name="Cierra a"
    )

    sport_center = models.ForeignKey(
        SportCenter,
        related_name="schedule",
        on_delete=models.CASCADE
    )


class SportCenterScheduleExclusion(SafeDeleteModel):

    day = models.DateField(
        verbose_name="Día"
    )

    sport_center = models.ForeignKey(
        SportCenter,
        related_name="schedule_exclusion",
        on_delete=models.CASCADE
    )


class SportCenterMedia(SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE

    SPORT_CENTER_MEDIA_CHOICES = (
        ("image", "Image"),
        ("video", "Video"),
    )

    url = models.TextField(
        verbose_name="Url",
    )

    media_type = models.CharField(
        max_length=250,
        verbose_name="Url",
        choices=SPORT_CENTER_MEDIA_CHOICES,
        default=SPORT_CENTER_MEDIA_CHOICES[0][0]
    )

    sport_center = models.ForeignKey(
        SportCenter,
        related_name="media",
        on_delete=models.CASCADE
    )


class SportCenterLink(SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE

    name = models.CharField(
        max_length=250,
        verbose_name="Nombre",
    )

    url = models.TextField(
        verbose_name="Url",
    )

    redirect_url = models.TextField(
        verbose_name="Redirect Url",
    )
    
    sport_center = models.ForeignKey(
        SportCenter,
        related_name="link",
        on_delete=models.CASCADE
    )


class SportCenterService(SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE

    description = models.CharField(
        max_length=250,
        verbose_name="Description",
    )


    service = models.ForeignKey(
        SportCenterCategoryService,
        on_delete=models.CASCADE
    )

    sport_center = models.ForeignKey(
        SportCenter,
        related_name="service",
        on_delete=models.CASCADE
    )

    order = models.IntegerField()


# class SportCenterActivityCategory(SafeDeleteModel):

#     _safedelete_policy = SOFT_DELETE

#     name = models.CharField(
#         max_length=250,
#         verbose_name="Nombre",
#     )

#     image = models.ImageField(
#         upload_to="activity_categories",
#         verbose_name="Imagen",
#     )

class SportCenterActivityTag(models.Model):

    name = models.CharField(
        max_length=250,
        verbose_name="Etiqueta de clase",
        unique=True
    )

    synonymous = ArrayField(models.CharField(max_length=250), null=True, verbose_name="Sinónimos")

    def __str__(self):
        return self.name
    
    class Meta():
        verbose_name = "Etiqueta"
        verbose_name_plural = "Etiquetas"

class SportCenterActivity(SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE_CASCADE

    name = models.CharField(
        max_length=250,
        verbose_name="Nombre",
    )

    image = models.ImageField(
        upload_to="activities",
        verbose_name="Imagen",
    )

    has_teacher = models.BooleanField(
        verbose_name="Tiene profesor"
    )

    teacher_name = models.CharField(
        max_length=250,
        verbose_name="Profesor",
        null=True,
        blank=True
    )

    teacher_image = models.ImageField(
        verbose_name="Imagen del profesor",   
        upload_to="activities",
        null=True,
        blank=True
    )

    description = models.TextField(
        verbose_name="Descripción de la clase",
        null=True
    )

    sport_center = models.ForeignKey(
        SportCenter,
        related_name="activities",
        on_delete=models.CASCADE,
        verbose_name="Centro deportivo"
    )

    tag = models.ManyToManyField(
        SportCenterActivityTag,
        verbose_name="Etiqueta"
    )   


    class Meta():
        verbose_name = "Clase"
        verbose_name_plural = "Clases"
    
    def __str__(self):
        return self.name
    
    def get_teacher_name(self):
        if self.has_teacher:
            return self.teacher_name
        else:
            return '-'
    get_teacher_name.short_description = "Profesor"

class SportCenterActivitySchedule(SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE
    DAYS = (
        (1, 'LUNES'),
        (2, 'MARTES'),
        (3, 'MIÉRCOLES'),
        (4, 'JUEVES'),
        (5, 'VIERNES'),
        (6, 'SÁBADO'),
        (7, 'DOMINGO'),
    )
    activity = models.ForeignKey(
        "SportCenterActivity",
        on_delete=models.CASCADE,
        verbose_name="Actividad"
    )

    day_of_week = models.IntegerField(
        verbose_name="Día de la semana",
        choices=DAYS
    )

    start_at = models.TimeField(
        null=True,
        verbose_name="Empieza a"
    )

    end_at = models.TimeField(
        null=True,
        verbose_name="Termina a"
    )

    max_users_allowed = models.IntegerField(
        verbose_name="Máximo de usuarios",
        default=0,
    )

    class Meta():
        verbose_name = "Horario"
        verbose_name_plural = "Horarios"
    
    def __str__(self):
        return str(self.id)

class SportCenterContact(SafeDeleteModel):
    name = models.CharField(
        max_length=250,
        verbose_name="Nombre",
    )
    district = models.CharField(
        max_length=250,
        verbose_name="Distrito",
    )
    contact_person = models.CharField(
        max_length=250,
        verbose_name="Distrito",
    )
    email = models.CharField(
        max_length=250,
        verbose_name="Email",
    )
    phone = models.CharField(
        max_length=250,
        null=True,
        verbose_name="Teléfono",
    )


class CompanyContact(SafeDeleteModel):
    name = models.CharField(
        max_length=250,
        verbose_name="Nombre",
    )
    district = models.CharField(
        max_length=250,
        verbose_name="Distrito",
    )
    contact_person = models.CharField(
        max_length=250,
        verbose_name="Distrito",
    )
    email = models.CharField(
        max_length=250,
        verbose_name="Email",
    )
    phone = models.CharField(
        max_length=250,
        null=True,
        verbose_name="Teléfono",
    )

    num_employees = models.IntegerField(
        verbose_name="Número de empleados",
    )