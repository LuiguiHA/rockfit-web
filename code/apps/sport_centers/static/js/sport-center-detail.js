var app = new Vue({
  el: '#sport-center-detail',
  delimiters: ['${', '}'],
  data: {
    sportCenterCoords: {lat:0, lng:0},
    havePlanUrl: window.havePlanUrl,
    showData: 1,
    classes: window.classes,
    servicesList: window.servicesList,
    areas: ['Estacionamiento', 'Dos salas de clases'],
    links: window.links,
    showMap: false,
    schedules: window.schedules,
    otherSedes: window.others_sedes,
    rate: window.rate,
    heightIndicators: 0,
    havePlan: false,
    havePlanPremium: false
  },
  watch:{
  },
  computed:{
    servicesFormated: function(){
      var services = _.groupBy(function(s){
        return s.title
      })(this.servicesList)

      // var ordered = {}
      // _(services).keys().sort().each(function (key) {
      //   ordered[key] = services[key];
      // });
      return services
    },
    schedulesFormated: function(){
      this.schedules = _.groupBy(function(s){
        return s.day
      })(this.schedules)

      var fp = _.noConflict();

      var a = _.mapValues(this.schedules, function(s){
        return _.map(s, function(v){
          return [v.open, v.close]
        })
      })
      var b =_.transform(a, function(result, value, key){
        var element = JSON.stringify(value)
        var days = result[element]
      
        if(days == undefined)
          days = []
      
        days.push(key)
        result[element] = days
        return result

      }, {});
      return _.transform(b, function(result, value, key){
              var daysAsString = _.map(value, function(d){
                return _.capitalize(moment().weekday(parseInt(d) - 1).format("ddd"))
              }).join("/").replace(/\./g, '')
              
              var hours = JSON.parse(key)
              var hoursAsString = _.map(hours, function(h){
                var open = moment("2018-01-01 " + h[0])
                var close = moment("2018-01-01 " + h[1])
                return open.format("hh:mm a") + " - " + close.format("hh:mm a")
              }).join(" / ")
            
              result.push({"hours": hoursAsString, "days": daysAsString})
              return result
            }, []);
    }
  },
  methods:{
    loadHavePlan: function(){
      axios.get(this.havePlanUrl)
      .then(function(response){
        this.havePlan = response.data.success
        this.havePlanPremium = response.data.is_premium
      }.bind(this))
    }
  },
  mounted: function(){
    document.getElementById('sport-center-detail').style.display = 'block';
    // $('html,body').scrollTo(0);
    this.sportCenterCoords = {
      lat: window.sportCenterLatitude,
      lng: window.sportCenterLongitude
    }
    this.loadHavePlan();
    this.heightIndicators = $('.el-carousel__indicators').height() + 'px';
  }
})