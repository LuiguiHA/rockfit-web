var app = new Vue({
  el: '#sport-centers',
  delimiters: ['${', '}'],
  data: {
    sportCenters:[],
    sportCentersFilter: [],
    districts: [],
    categories: [],
    selectedSportCenter: {lat:0, lng:0},
    sportCenterInfoWindowOpened: false,
    sportCenterNameFilter: null,
    sportCenterDistrictFilter: null,
    sportCenterActivityFilter: null,
    sportCenterExclusiveFilter: null,
    sportCenterOpenStatusFilter: null,
    firstTime: true,
    roundKm:10,
    premium: false,
    visited: false,
    openedNow: false,
    windowHeight: 0,
    windowHeightMap: 0,
    mapOptions: {mapTypeControl: false, streetViewControl: false},
    havePlanUrl: window.havePlanUrl,
    havePlan: false,
    sportCenterCategoryFilter: null,
    havePlanPremium: false
  },
  watch:{
    sportCenterNameFilter: function(newValue, oldValue){
      this.loadCenters(this.filters)
    },
    sportCenterDistrictFilter: function(newValue, oldValue){
      this.loadCenters(this.filters)
    },
    // sportCenterActivityFilter: function(newValue, oldValue){
    //   this.loadCenters(this.filters)
    // },
    visited: function(newValue, oldValue){
      this.loadCenters(this.filters)
    },
    openedNow: function(newValue, oldValue){
      this.loadCenters(this.filters)
    },
    premium: function(newValue, oldValue){
      this.loadCenters(this.filters)
    },
    sportCenterCategoryFilter: function(){
      this.loadCenters(this.filters)
    }
  },
  computed:{
    filters: function(){
      return {
        name: this.sportCenterNameFilter ? this.sportCenterNameFilter.value : null,
        visited: this.visited,
        category: this.sportCenterCategoryFilter ? this.sportCenterCategoryFilter.value : null,
        ubigeo: this.sportCenterDistrictFilter ? this.sportCenterDistrictFilter.value : null,
        activity: this.sportCenterActivityFilter ? this.sportCenterActivityFilter.label : null,
        exclusive: this.premium ? "exclusive" : "all",
        status: this.openedNow ? "opened" : null
      }
    },
    sportCentersForDropdown: function(){
      var values = window._.map(function(element){
        return {'label': element.name, 'value': element.value}
      })(this.sportCentersFilter)
      return values
    },
    activitiesForDropdown: function(){
      var values = window._.map(function(element){
        return {'label': element.name, 'value': element.id}
      })(this.activities)
      return values
    },
    districtsForDropdown: function(){
      var values = window._.map(function(element){
        return {'label': element.name, 'value': element.id}
      })(this.districts)
      return values
    },
    categoriesForDropdown: function(){
      var values = window._.map(function(element){
        return {'label': element.name, 'value': element.id}
      })(this.categories)
      return values
    }
  },
  methods:{
    getSportCenterActivities: function(activities){
      var actitiviesNames = window._.map(function(element){
        return element.name
      })(activities)

      return actitiviesNames.join(" - ")
    },
    loadCenters: function(filters){
      axios.get(sportCenterListUrl, {params:filters})
      .then(function (response) {
        if(this.firstTime){
          this.firstTime = false
          this.sportCenters = response.data  
        }
        this.sportCenters = response.data  
        // this.sportCentersForMap = response.data  
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    loadCentersFilter: function(filters){
      axios.get(sportCenterFilterUrl)
      .then(function (response) {
        this.sportCentersFilter = response.data  
        // this.sportCentersForMap = response.data  
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    loadActivities: function(){
      axios.get(categoryActivitiesUrl, {})
      .then(function (response) {
        this.activities = response.data
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    loadDistricts: function(){
      axios.get(sportCentersDistrictsUrl, {})
      .then(function (response) {
        this.districts = response.data
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    loadCategories: function(){
      axios.get(sportCentersCategoryUrl)
      .then(function (response) {
        this.categories = response.data
        console.log(this.categories)
      }.bind(this))
    },
    getMarkerPosition: function(sportCenter){
      var position = {
        'lat': parseFloat(sportCenter.latitude), 
        'lng': parseFloat(sportCenter.longitude)
      }
      return position
    },
    selectSportCenter: function(sportCenter){
      this.selectedSportCenter = sportCenter
    },
    mouseOverMarker: function(sportCenter){
      var infoWindow = this.$refs['sportCenterInfoWindow' + sportCenter.id][0]
      infoWindow.opened = true
      infoWindow._openInfoWindow()
    },
    mouseOutMarker: function(sportCenter){
      var infoWindow = this.$refs['sportCenterInfoWindow' + sportCenter.id][0]
      infoWindow.opened = true
      infoWindow.$infoWindowObject.close()
    },
    formatTooltipKm:  function(val){
      return val + ' km'
    },
    loadHavePlan: function(){
      axios.get(this.havePlanUrl)
      .then(function(response){
        this.havePlan = response.data.success
        this.havePlanPremium = response.data.is_premium
      }.bind(this))
    }
  },
  mounted: function(){
    // this.loadCenters()
    this.loadHavePlan();
    this.loadActivities()
    this.loadDistricts()
    this.loadCategories()
    document.getElementById("sport-centers").style.display = "block"
    this.windowHeight = (document.documentElement.clientHeight - 250) + 'px';
    this.windowHeightMap = (document.documentElement.clientHeight - 155) + 'px';
  },
  created: function(){
    this.loadCenters()
    this.loadCentersFilter()
  }
})