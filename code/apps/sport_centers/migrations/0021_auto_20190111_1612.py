# Generated by Django 2.0 on 2019-01-11 16:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sport_centers', '0020_sportcenter_ask_weight'),
    ]

    operations = [
        migrations.CreateModel(
            name='SportCenterGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('name', models.CharField(max_length=250)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='sportcenter',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='sport_centers.SportCenterGroup', verbose_name='Grupo'),
        ),
    ]
