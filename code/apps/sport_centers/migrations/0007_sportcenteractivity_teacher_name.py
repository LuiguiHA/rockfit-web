# Generated by Django 2.0.5 on 2018-08-23 21:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sport_centers', '0006_sportcenterscheduleexclusion'),
    ]

    operations = [
        migrations.AddField(
            model_name='sportcenteractivity',
            name='teacher_name',
            field=models.CharField(max_length=250, null=True, verbose_name='Nombre del profesor'),
        ),
    ]
