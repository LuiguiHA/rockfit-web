from django.contrib import admin
from .models import *
from .forms import *
from django.contrib import messages
from django.shortcuts import redirect
# Register your models here.

@admin.register(SportCenter)
class SportCenterAdmin(admin.ModelAdmin):
    class Media:
        css = {
            'all':  ('https://use.fontawesome.com/releases/v5.6.3/css/all.css',)
        }
    search_fields = ['name', 'speciality', 'slug']
    list_display = ["name", "speciality", "slug", "ubigeo", "get_price", "get_is_premium"]
    list_filter = ["is_premium", "ubigeo"]

@admin.register(SportCenterActivityTag)
class SportCenterActivityTagAdmin(admin.ModelAdmin):
    search_fields = ['name', 'synonymous']
    list_display = ["name", "synonymous"]
    list_filter = ["name"]
    form = SportCenterActivityTagForm
    # def save_model(self, request, obj, form, change):
    #     name = obj.name.strip()
    #     tag_exists = SportCenterActivityTag.objects.filter(name__iexact=name).exists()
    #     if tag_exists and change is False:
    #         raise self.message_user(request, "El nombre de la etiqueta ya existe.", messages.ERROR)
    #         return redirect("admin:sport_centers_sportcenteractivitytag_change")
    #     else:
    #         obj.name = name
    #         obj.save()

@admin.register(SportCenterGroup)
class SportCenterGroupAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ["name"]
    list_filter = ["name"]