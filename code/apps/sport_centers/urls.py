from django.urls import path, re_path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    re_path('^$', views.SportCenterView.as_view(), name="sport-centers"),
    re_path('^(?P<slug>[\w-]+)/$', views.SportCenterDetailView.as_view(), name="sport-center-detail"),
    re_path('^list$', views.CenterList.as_view(), name="sport-centers-list"),
    re_path('^filter$', views.CenterFilterList.as_view(), name="sport-centers-filter"),
    re_path('^category-activities$', views.ActivitiesList.as_view(), name="category-activities"),
    re_path('^districts$', views.DistrictList.as_view(), name="sport-centers-districts"),
    re_path('^categories$', views.CategoriesList.as_view(), name="sport-centers-categories"),
]