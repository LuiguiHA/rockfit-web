from django import forms
from sport_centers.models import SportCenterActivity
from django.conf import settings
from tinymce.widgets import TinyMCE
from .models import *

class SportCenterForm(forms.ModelForm):
    descriptiom = forms.CharField(
        label="Descripción",
        widget=TinyMCE(attrs={'cols': 80, 'rows': 30})
    )
    def __init__(self, *args, **kwargs):

        super(SportCenterForm, self).__init__(*args, **kwargs)

        if self.instance.pk:
            self.initial['descriptiom'] = self.instance.description



class SportCenterActivityTagForm(forms.ModelForm):
    def clean_name(self):
        name = self.cleaned_data['name']
        name = name.strip()
        if self.instance.pk:
            tag_exists = SportCenterActivityTag.objects.exclude(id=self.instance.pk).filter(name__iexact=name).exists()
        else:
            tag_exists = SportCenterActivityTag.objects.filter(name__iexact=name).exists()
        if tag_exists:
            raise forms.ValidationError('Ya existe una etiqueta con este nombre')
        return name