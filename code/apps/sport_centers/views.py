from django.conf import settings
from django.shortcuts import render, redirect
from django.views import View
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.db import connection
from . import models
from .models import *
from ubigeo.models import Ubigeo
from plans.models import Plan
from users.models import UserActivityReservation
from users.utils import get_centers_allowed
from django.db.models import Q
import json
import arrow
import re

# Create your views here.
class CenterList(View):
    def get(self, request):
        name = request.GET.get("name")
        activity = request.GET.get("activity")
        ubigeo = request.GET.get("ubigeo")
        status = eval(request.GET.get("status").capitalize()) if request.GET.get("status") else None
        exclusive = request.GET.get("exclusive")
        visited = eval(request.GET.get("visited").capitalize()) if request.GET.get("visited") else None
        category = int(request.GET.get('category')) if request.GET.get('category') else None
        # instructor = request.GET.get("instructor")
        center_list = SportCenter.objects.all()
        if name:
            is_group = name.split('-')[0]
            if is_group == 'g':
                group_id = int(name.split('-')[1])
                center_list = center_list.filter(group__id=group_id)
            else:
                center_id = int(name.split('-')[1])
                center_list = center_list.filter(id=center_id)

        if activity:
            center_list = center_list.filter(activities__name=activity)

        if ubigeo:
            center_list = center_list.filter(ubigeo_id=ubigeo)

        if exclusive == 'exclusive':
            center_list = center_list.filter(is_premium=True)

        if status:
            now = arrow.utcnow().to(settings.PROJECT_TIME_ZONE).datetime.time()
            weekday = arrow.utcnow().isoweekday()
            centers_id = [center.id for center in center_list]
            centers_opened = list(SportCenterSchedule.objects.filter(day_of_week=weekday, sport_center__id__in=centers_id, open_at__lte=now, close_at__gte=now).values_list('sport_center__id', flat=True))
            center_list = center_list.filter(id__in=centers_opened)

        if visited and request.user.is_authenticated:
            reservations = list(UserActivityReservation.objects.filter(cancelled=False, related_pass__user=request.user, related_pass__used=True))
            centers_ids = [reserve.activity_schedule.activity.sport_center.id for reserve in reservations]
            center_list = center_list.filter(id__in=centers_ids)
        
        if category:
            center_list = center_list.filter(category__id=category)
        # if instructor:
        #     if instructor == "with-instructor":
        #         center_list = center_list.filter()
        #     if instructor == "without-instructor":

        center_list = center_list.distinct().values(
            "id", 
            "name", 
            "speciality",
            "ubigeo__name",
            "slug", 
            "logo",
            "latitude", 
            "longitude",
            "address",
            "address_reference",
            "rate",
            "total_califications",
            "sell_pass",
            "is_premium",
        )
        user_plan = None
        if request.user.is_authenticated:
            now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc').datetime
            user_plan = request.user.current_plan.filter(
                expires_at__gte=now,
            ).first()
            if user_plan:
                passes_to_use_from_plan = 0
                if not user_plan.is_freeze:
                    passes_to_use_from_plan = user_plan.plan_passes.filter(
                        Q(used=False),
                        Q(buy_reference='PLAN_PASS'),
                        Q(plan__isnull=False),
                        Q(reservation_passes__isnull=True) | Q(reservation_passes__isnull=False, reservation_passes__cancelled=True)
                    ).count()
                    
                center_allowed = get_centers_allowed(user_plan)

        for center in center_list:
            activities = SportCenterActivity.objects \
                .filter(sport_center_id=center["id"]) \
                .values(
                    "id", 
                    "name"
                )
            # center["activities_categories"] = list(activities_categories)
            center["activities"] = list(activities)
            # VALIDAMOS SI SE PUEDE COMPRAR PASES DE ESTE CENTRO
            center['can_buy_pase'] = True
            if user_plan:
                if center.get('id') in center_allowed and passes_to_use_from_plan > 0:
                    center['can_buy_pase'] = False

        return JsonResponse(list(center_list), safe=False)


class ActivitiesList(View):
    def get(self, request):

        activities_list = list(SportCenterActivity.objects.all().distinct("name").values("id", "name"))
        return JsonResponse(list(activities_list), safe=False)


class DistrictList(View):
    def get(self, request):

        sport_center_ubigeo = \
            SportCenter.objects \
            .all() \
            .distinct()

        sport_center_ubigeo_ids = [x.ubigeo.id for x in sport_center_ubigeo]
        districts = Ubigeo.objects.filter(id__in=sport_center_ubigeo_ids).values("id", "name")

        return JsonResponse(list(districts), safe=False)


class CategoriesList(View):
    def get(self, request):
        centers = \
            SportCenter.objects \
            .all() \
            .distinct()

        categories_ids = [x.category.id for x in centers if x.category != None]
        categories = list(SportCenterCategory.objects.filter(id__in=categories_ids).order_by('name').values("id", "name"))

        return JsonResponse(categories, safe=False)


class CenterFilterList(View):
    def get(self, request):
        groups = list(SportCenterGroup.objects.all().values("id", "name"))
        groups_ids = list(SportCenterGroup.objects.all() \
                     .values_list("id", flat=True))
        centers_ids = list(SportCenter.objects.filter(group__id__in=groups_ids)\
                  .values_list("id", flat=True))
        centers = list(SportCenter.objects.all().exclude(id__in=centers_ids) \
                  .values("id", "name"))
        
        for group in groups:
            group['value'] = 'g-' + str(group.get('id'))

        for center in centers:
            center['value'] = 'c-' + str(center.get('id'))

        total_list = groups + centers
        return JsonResponse(total_list, safe=False)


# Create your views here.
class SportCenterView(View):
    def get(self, request):
        return render(request, 'sport-centers.html', {'active': 3})


class SportCenterDetailView(View):
    def get(self, request, slug):
        sport_center = None
        try:
            sport_center = SportCenter.objects.get(
                slug=slug
            )
        except:
            pass

        # plans = Plan.objects.all()

        # plans_list = []
        # for plan in plans:
        #     sport_centers_by_plan_count = models.SportCenter.objects.all().exclude(pk=sport_center.id).count()

        #     plans_list.append({
        #         'name': plan.name,
        #         'sport_centers_count': sport_centers_by_plan_count,
        #         'friendpass_quantity': plan.friendpass_quantity,
        #         'pass_quantity': plan.pass_quantity,
        #         'repetitions_quantity': plan.repetitions_quantity
        #     })

        # with connection.cursor() as cursor:
        #     cursor.execute(
        #         """select name from sport_centers_sportcenter where 
        #         ST_DistanceSphere(ST_Point(longitude, latitude), ST_Point(%s, %s)) < 5000 
        #         AND id != %s 
        #         limit 10""", [
        #             sport_center.longitude,
        #             sport_center.latitude,
        #             sport_center.id,
        #         ]
        #     )
            
        #     near_sport_centers = cursor.fetchall()

        # with connection.cursor() as cursor:
        #     activity_ids = [x.category.id for x in sport_center.activities.all()]
        #     count_activities = 1 if sport_center.activities.all().count() < 3 else 2
            
        #     cursor.execute(
        #     """select sportcenter.name, sportcenter.slug, count(activities.id) from sport_centers_sportcenter sportcenter 
        #     inner join sport_centers_sportcenteractivity activities on activities.sport_center_id = sportcenter.id 
        #     and activities.category_id = ANY(%s)
        #     AND sportcenter.id != %s 
        #     group by sportcenter.name, sportcenter.slug 
        #     having count(activities.id) >= %s""", [activity_ids, sport_center.id, count_activities])
            
        #     sport_centers_with_same_activities = cursor.fetchall()

        # with connection.cursor() as cursor:

        #     name = re.sub(r"\(.*?\)", "", sport_center.name).strip()
            
        #     cursor.execute("""select id, name, slug from sport_centers_sportcenter sportcenter 
        #     where lower( (regexp_split_to_array(sportcenter.name, E'\\\\s\\\\(' ))[1]) = lower(%s) 
        #     AND id != %s""", [name, sport_center.id])

        #     sport_centers_related = cursor.fetchall()

        # CENTROS CERCANOS
        near_sport_centers = list(SportCenter.objects.raw(
                """select * from sport_centers_sportcenter where 
                ST_DistanceSphere(ST_Point(longitude, latitude), ST_Point(%s, %s)) < 2000 
                AND id != %s
                AND deleted is NULL""", [
                    sport_center.longitude,
                    sport_center.latitude,
                    sport_center.id,
                ]))
        # CLASES
        # activities = list(sport_center.activities.all().distinct('name').values_list('name', flat=True))

        # CENTROS CON LAS MISMAS ACTIVIDADES
        # activities = list(sport_center.service.filter(sport_center=sport_center, service__name='Clases').values_list('description', flat=True))
        # sport_centers_same_ids = list(SportCenterService.objects.filter(service__name='Clases', description__in=activities).exclude(sport_center=sport_center).values_list("sport_center__id", flat=True))
        # sport_centers_same = list(SportCenter.objects.filter(category=sport_center.category).exclude(id=sport_center.id))
        

        # LINKS DEL CENTRO
        links = list(sport_center.link.all().order_by('id').values('name', 'url', 'redirect_url'))

        schedules = list(sport_center.schedule.all())
        schedules_list = []
        for schedule in schedules:
            info = {}
            info['day'] = schedule.day_of_week
            info['open'] = schedule.open_at.strftime('%H:%M')
            info['close'] = schedule.close_at.strftime('%H:%M')
            schedules_list.append(info)

        # OTRAS SEDES DEL MISMO 
        other_sedes_ids = list(sport_center.sport_centers_suggested.all() \
                       .values_list('sport_center_suggested_id', flat=True))
        other_sedes = list(SportCenter.objects.filter(id__in=other_sedes_ids))
        other_sedes_list = []
        for sede in other_sedes:
            info ={}
            info['slug'] = sede.slug
            info['name'] = sede.name
            info['logo'] = sede.logo.url
            info['image_responsive'] = sede.sorted_media().first().url \
                                       if sede.sorted_media() else 0
            info['ubigeo'] = sede.ubigeo.name
            info['rate'] = int(sede.rate)
            other_sedes_list.append(info)

        # CENTROS CON LAS MISMAS ACTIVIDADES
        other_sedes_ids.append(sport_center.id)
        sport_centers_same = list(SportCenter.objects.filter(category=sport_center.category).exclude(id__in=other_sedes_ids))

        # SERVICIOS DEL CENTRO

        center_services = list(sport_center.service.all().order_by('service__order', 'order'))
        services_list = []
        for center_service in center_services:
            info = {}
            info['title'] = center_service.service.name.upper()
            info['description'] = center_service.description
            services_list.append(info)

        # VALIDAMOS SI SE PUEDE COMPRAR PASES DE ESTE CENTRO
        can_buy_pase = True
        if request.user.is_authenticated:
            now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc').datetime
            user_plan = request.user.current_plan.filter(
                expires_at__gte=now,
            ).first()
            if user_plan:
                passes_to_use_from_plan = 0
                if not user_plan.is_freeze:
                    passes_to_use_from_plan = user_plan.plan_passes.filter(
                        Q(used=False),
                        Q(buy_reference='PLAN_PASS'),
                        Q(plan__isnull=False),
                        Q(reservation_passes__isnull=True) | Q(reservation_passes__isnull=False, reservation_passes__cancelled=True)
                    ).count()

                center_allowed = get_centers_allowed(user_plan)
                if sport_center.id in center_allowed and passes_to_use_from_plan > 0:
                    can_buy_pase = False

        if sport_center:
            return render(request, 'sport-center-detail.html', {
                'sport_center': sport_center,
                # 'plans_list': plans_list,
                'near_sport_centers': near_sport_centers,
                'sport_centers_same': sport_centers_same,
                # 'sport_centers_related': sport_centers_related,
                'links': links,
                'schedules': schedules_list,
                'others_sedes': other_sedes_list,
                # 'classes': activities,
                'services_list': services_list,
                'active': 3,
                'can_buy_pase': can_buy_pase
            })
        else:
            return redirect("/")
