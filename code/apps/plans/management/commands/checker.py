from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.db import transaction, connection
from django.template.loader import render_to_string
from django.contrib.auth.models import User
from django.utils import formats
from users.models import *
from plans import utils
import arrow
import datetime

class Command(BaseCommand):
    help = 'Check status for plans and activities'

    def notify_not_assisted_activities(self):
        with connection.cursor() as cursor:

            now = arrow.now(
                settings.PROJECT_TIME_ZONE
            ).datetime


            cursor.execute("""
                select * from (
                    select 
                        user_reservation.id as user_reservation_id,
                        user_pass.id as pass_id,  
                        to_timestamp(concat(
                            user_reservation.reservation_day, 
                            ' ', 
                            activity_schedule.start_at
                        ), 'YYYY-MM-DD HH24:MI:SS') as reservation_start,
                        user_pass.user_id
                    from users_useractivityreservation as user_reservation
                    inner join sport_centers_sportcenteractivityschedule as activity_schedule on activity_schedule.id = user_reservation.activity_schedule_id
                    inner join users_userpass as user_pass on user_pass.id = user_reservation.related_pass_id
                    where user_pass.used = true
                    and user_pass.assisted_at is null
                    and user_reservation.notified_not_assisted = false
                    ) reservations
                where reservation_start <= %s""", [now])

            columns = [col[0] for col in cursor.description]
            reservations = [
                dict(zip(columns, row))
                for row in cursor.fetchall()
            ]

            for reservation in reservations:
                user_reservation = UserActivityReservation.objects.get(
                    pk=reservation["user_reservation_id"]
                )
                user_reservation.notified_not_assisted = True
                user_reservation.save()

                user = User.objects.get(pk=reservation["user_id"])

                current_plan = user.current_plan.filter(
                    expires_at__gte=now,
                ).first()

                if current_plan:

                    sport_center_activity_schedule = user_reservation.activity_schedule
                    sport_center = sport_center_activity_schedule.activity.sport_center
                    class_datetime = arrow.get(user_reservation.reservation_day).replace(
                        hour=sport_center_activity_schedule.start_at.hour,
                        minute=sport_center_activity_schedule.start_at.minute,
                        second=sport_center_activity_schedule.start_at.second,
                        tzinfo=settings.PROJECT_TIME_ZONE
                    )

                    passes_to_use_from_plan = current_plan.plan_passes.filter(
                        used=False,
                        assisted_at__isnull=True,
                    )

                    active_reservations = UserActivityReservation.objects.filter(
                        related_pass__user=user,
                        reservation_day=now.date()
                    )

                    plan_expires_in_days = (current_plan.expires_at - now)

                    email_template = render_to_string(
                        "mails/plan_update_notification.html",
                        {
                            'name': user.first_name,
                            'plan_name': current_plan.name,
                            'plan_date_end_days': plan_expires_in_days.days,
                            'plan_date_end': current_plan.expires_at.strftime("%y/%m/%d"),
                            'plan_is_ilimitaded': current_plan.pass_quantity < 0,
                            'plan_classes': passes_to_use_from_plan.count(),
                            'plan_active_reservations': active_reservations.count(),
                            'site_url': settings.SITE_URL,
                        }
                    )

                    print("email_template", email_template)


    def remind_renovations(sef):
        now = arrow.now(settings.PROJECT_TIME_ZONE)\
            .shift(days=settings.PLAN_RENOVATION_DAYS_BEFORE)\
            .replace(tzinfo="UTC")

        pending_plans = UserPendingPlan.objects.filter(
            renew_at__lte=now.datetime, 
            renewed=False,
            notified_count=0
        )

        for pending_plan in pending_plans:
            with transaction.atomic():

                user = pending_plan.user

                if pending_plan.days:
                    plan_duration_units = "días" if pending_plan.days > 1 else "día"
                    plan_date_end = arrow.get(pending_plan.renew_at).shift(days=pending_plan.days)
                else:
                    plan_duration_units = "meses" if pending_plan.months > 1 else "mes"
                    plan_date_end = arrow.get(pending_plan.renew_at).shift(months=pending_plan.months)
                
                pending_plan.notified_count = 1
                pending_plan.save()

                email_template = render_to_string(
                    "mails/renovation_reminder.html",
                    {
                        'name': user.first_name,
                        'plan_name': pending_plan.name,
                        'plan_duration': pending_plan.days,
                        'plan_duration_units': plan_duration_units,
                        'plan_date_start': pending_plan.renew_at.strftime("%y/%m/%d"),
                        'plan_date_end': plan_date_end.datetime.strftime("%y/%m/%d"),
                        'plan_classes': pending_plan.pass_quantity,
                        'plan_repetitions': pending_plan.repetitions_quantity,
                        'plan_friendpass': pending_plan.friendpass_quantity,
                        'plan_price': pending_plan.current_price,
                        'client_code': user.codes.first().code,
                        'site_url': settings.SITE_URL,
                    }
                )

                print("email_template", email_template)

    def remind_activities(self):
        with connection.cursor() as cursor:

            now_plus_4_hours = arrow.now(
                settings.PROJECT_TIME_ZONE
            ).datetime + datetime.timedelta(hours=settings.HOURS_BEFORE_RESERVATION_USED)

            cursor.execute("""
                select * from (
                    select 
                        user_reservation.id as user_reservation_id,
                        user_pass.id as pass_id,  
                        to_timestamp(concat(
                            user_reservation.reservation_day, 
                            ' ', 
                            activity_schedule.start_at
                        ), 'YYYY-MM-DD HH24:MI:SS') as reservation_start,
                        user_reservation.created_at as created_at,
                        (to_timestamp(concat(
                            user_reservation.reservation_day, 
                            ' ', 
                            '13:00:00'
                        ), 'YYYY-MM-DD HH24:MI:SS') - INTERVAL '1 DAY') as class_one_day_before,
                        user_pass.user_id
                    from users_useractivityreservation as user_reservation
                    inner join sport_centers_sportcenteractivityschedule as activity_schedule on activity_schedule.id = user_reservation.activity_schedule_id
                    inner join users_userpass as user_pass on user_pass.id = user_reservation.related_pass_id
                    where user_pass.used = false
                    and user_reservation.notified_count = 0
                    ) reservations
                where reservation_start <= %s
                and created_at <= class_one_day_before""", [now_plus_4_hours])

            columns = [col[0] for col in cursor.description]
            reservations = [
                dict(zip(columns, row))
                for row in cursor.fetchall()
            ]

            for reservation in reservations:
                user_reservation = UserActivityReservation.objects.get(
                    pk=reservation["user_reservation_id"]
                )
                user_reservation.notified_count = user_reservation.notified_count + 1
                user_reservation.save()

                user = User.objects.get(pk=reservation["user_id"])
                sport_center_activity_schedule = user_reservation.activity_schedule
                sport_center = sport_center_activity_schedule.activity.sport_center
                class_datetime = arrow.get(user_reservation.reservation_day).replace(
                    hour=sport_center_activity_schedule.start_at.hour,
                    minute=sport_center_activity_schedule.start_at.minute,
                    second=sport_center_activity_schedule.start_at.second,
                    tzinfo=settings.PROJECT_TIME_ZONE
                )

                email_template = render_to_string(
                    "mails/reservation_reminder.html",
                    {
                        "name": user.first_name,
                        "class_name": sport_center_activity_schedule.activity.name,
                        "class_date": formats.date_format(class_datetime.datetime),
                        "class_time": "{} - {}".format(
                            sport_center_activity_schedule.start_at.strftime("%I:%M %p"),
                            sport_center_activity_schedule.end_at.strftime("%I:%M %p")
                        ),
                        "class_sportcenter": sport_center.name,
                        "class_address": "{} {}".format(sport_center.address, sport_center.address_reference) ,
                        "client_code": user.codes.first().code,
                    }
                )

                print("email_template", email_template)


                # user_pass = UserPass.objects.get(pk=reservation["pass_id"])
                # user_pass.used = True
                # user_pass.save()

    def check_activities(self):
        with connection.cursor() as cursor:

            now_plus_4_hours = arrow.now(
                settings.PROJECT_TIME_ZONE
            ).datetime + datetime.timedelta(hours=settings.HOURS_BEFORE_RESERVATION_USED)

            cursor.execute("""
                select * from (
                    select 
                        user_pass.id as pass_id,  
                        to_timestamp(concat(
                            user_reservation.reservation_day, 
                            ' ', 
                            activity_schedule.start_at
                        ), 'YYYY-MM-DD HH24:MI:SS') as reservation_start,
                        user_pass.user_id
                    from users_useractivityreservation as user_reservation
                    inner join sport_centers_sportcenteractivityschedule as activity_schedule on activity_schedule.id = user_reservation.activity_schedule_id
                    inner join users_userpass as user_pass on user_pass.id = user_reservation.related_pass_id
                    where user_pass.used = false
                    ) reservations
                where reservation_start <= %s""", [now_plus_4_hours])

            columns = [col[0] for col in cursor.description]
            reservations = [
                dict(zip(columns, row))
                for row in cursor.fetchall()
            ]

            for reservation in reservations:
                user_pass = UserPass.objects.get(pk=reservation["pass_id"])
                user_pass.used = True
                user_pass.save()

                #TODO: Send mail

    def check_plans(self):
        now = arrow.now(settings.PROJECT_TIME_ZONE).datetime

        pending_plans = UserPendingPlan.objects.filter(renew_at__lte=now, renewed=False)

        for pending_plan in pending_plans:
            try:
                with transaction.atomic():
                    user_plan_payment = utils.create_charge(
                        pending_plan.user, 
                        pending_plan.current_price, 
                        pending_plan.discount
                    )

                    now = arrow.now(settings.PROJECT_TIME_ZONE)
                    if pending_plan.days:
                        next_payment = now.shift(days=pending_plan.days)
                    else:
                        next_payment = now.shift(months=pending_plan.months)

                    #Copy of plan for user
                    user_plan = utils.create_user_plan(
                        pending_plan.user, 
                        pending_plan, 
                        pending_plan.discount, 
                        user_plan_payment, 
                        next_payment.datetime
                    )

                    #Mark plan as renewed
                    pending_plan.renewed = True
                    pending_plan.save()

                    #Create new pending plan for renew
                    user_pending_plan = utils.create_user_pending_plan(
                        pending_plan.user, 
                        pending_plan, 
                        0, 
                        user_plan_payment, 
                        next_payment.datetime + datetime.timedelta(microseconds=1)
                    )


                    if pending_plan.days:
                        plan_duration_units = "días" if pending_plan.days > 1 else "día"
                    else:
                        plan_duration_units = "meses" if pending_plan.months > 1 else "mes"

                    email_template = render_to_string(
                        "mails/renew_plan_notification.html",
                        {
                            'name': pending_plan.user.first_name,
                            'plan_name': pending_plan.name,
                            'plan_duration': pending_plan.days,
                            'plan_duration_units': plan_duration_units,
                            'plan_date_start': pending_plan.renew_at.strftime("%y/%m/%d"),
                            'plan_date_end': next_payment.datetime.strftime("%y/%m/%d"),
                            'plan_classes': pending_plan.pass_quantity,
                            'plan_repetitions': pending_plan.repetitions_quantity,
                            'plan_friendpass': pending_plan.friendpass_quantity,
                            'plan_price': pending_plan.current_price,
                            'client_code': pending_plan.user.codes.first().code,
                            'site_url': settings.SITE_URL,
                        }
                    )

                    print("email_template", email_template)
            except:
                email_template = render_to_string(
                    "mails/renovation_error_notification.html",
                    {
                        'name': pending_plan.user.first_name,
                        'site_url': settings.SITE_URL,
                    }
                )
                
                print("email_template", email_template)

                    #TODO: Send mail

    def handle(self, *args, **options):
        # self.check_plans()
        # self.check_activities()
        # self.remind_activities()
        # self.remind_renovations()
        self.notify_not_assisted_activities()
