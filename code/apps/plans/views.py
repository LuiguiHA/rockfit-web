from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect
from django.views import View
from django.db import transaction
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.contrib import messages
from django.db.models import Q
from django.template import defaultfilters
from django.template.loader import render_to_string
from project.error_codes import CODES
from . import models, utils
from .exceptions import CvvInvalidException, chargeException
from ubigeo.models import Ubigeo
from plans.models import Plan
from sport_centers.models import SportCenter
from config.models import Config
from users.models import UserCard, UserPlan, UserPendingPlan, \
    UserPayment, UserPass, UserCode, UserUsedCoupon
from users.utils import get_centers_allowed, generate_user_code
import json
import arrow
import datetime

# Create your views here.
class PlanBuyView(View):
    def get(self, request):
        data = request.GET
        plus = int(data.get('plus')) if data.get('plus') else 0
        plans = Plan.objects.all().order_by("order")
        user_plan = None
        if request.user.is_authenticated:
            now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc').datetime
            user_plan = request.user.current_plan.filter(
                expires_at__gte=now,
            ).first()
        
        sport_centers = list(SportCenter.objects.filter(sell_pass=True,pass_amount__isnull=False).values("id", "name", "pass_amount"))

        if user_plan:
            passes_to_use_from_plan = 0
            if not user_plan.is_freeze:
                passes_to_use_from_plan = user_plan.plan_passes.filter(
                    Q(used=False),
                    Q(buy_reference='PLAN_PASS'),
                    Q(plan__isnull=False),
                    Q(reservation_passes__isnull=True) | Q(reservation_passes__isnull=False, reservation_passes__cancelled=True)
                ).count()
            sport_centers_ids = get_centers_allowed(user_plan)
            # CENTROS EN LOS QUE EL USUARIO PUEDE COMPRAR UN PASE
            if passes_to_use_from_plan > 0:
                sport_centers = list(SportCenter.objects.all(sell_pass=True,pass_amount__isnull=False).exclude(id__in=sport_centers_ids).values("id", "name", "pass_amount"))

        return render(request, 'buy-plan.html', {
            'plans': plans,
            'user_plan': user_plan,
            'sport_centers': json.dumps(sport_centers),
            'active': 4,
            'plus': plus
        })

def get_serialized_card(request):
    card = request.user.cards.first()
    card_dict = None
    if card:
        card_dict = {}
        card_dict["card_id"] = card.card_id
        card_dict["card_brand"] = card.card_brand
        card_dict["card_number_masked"] = card.card_number_masked
        card_dict["card_expire_month"] = card.card_expire_month
        card_dict["card_expire_year"] = card.card_expire_year
    return json.dumps(card_dict)

def get_buy_plan_dict(request, selected_plan):
    try:
        plan = Plan.objects.get(pk=selected_plan)
    except:
        raise ValueError('Wrong plan selected')

    user_plan = None
    if request.user.is_authenticated:
        now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc').datetime
        user_plan = request.user.current_plan.filter(
            expires_at__gte=now,
        ).first()

    now = arrow.now(settings.PROJECT_TIME_ZONE)

    if plan.days:
        next_payment = now.shift(days=plan.days)
    else:
        next_payment = now.shift(months=plan.months)

    plans = Plan.objects.all().order_by("order")
 
    if user_plan:
        next_payment = user_plan.expires_at
        if user_plan.is_premium:
            sport_centers = []
        else:
            sport_centers = list(SportCenter.objects.filter(sell_pass=True,pass_amount__isnull=False).values("id", "name", "pass_amount"))     
    else:
        sport_centers = list(SportCenter.objects.filter(sell_pass=True,pass_amount__isnull=False).values("id", "name", "pass_amount"))

    return {
        'plan': plan, 
        'next_payment': next_payment,
        'card': get_serialized_card(request) if request.user.is_authenticated else None,
        'CULQI_PUBLIC_KEY': settings.CULQI_PUBLIC_KEY,
        'active': 4,
        'plans': plans,
        'user_plan': user_plan,
        'sport_centers': json.dumps(sport_centers)
    }

class PlanBuySelectedView(View):
    def get(self, request, selected_plan): 
        if request.user.is_authenticated:
            try:
                data = get_buy_plan_dict(request, selected_plan)
                now = arrow.now(settings.PROJECT_TIME_ZONE)
                user_plan = request.user.current_plan.filter(
                    expires_at__gte=now.datetime,
                ).first()
                user_had_plan = True if user_plan else False
                data['user_had_plan'] = user_had_plan
                return render(request, 'buy-plan-selected.html', data)
            except Exception as e:
                print("----", e)
                return redirect("/")
        else:
            buy_selected_url = reverse("buy-plan-selected", args=[selected_plan])
            buy_selected_url_auth = reverse("buy-plan-selected-auth", args=[selected_plan])
            return redirect(buy_selected_url_auth + "?next={}&login=1".format(buy_selected_url))


class PlanBuySelectedAuthView(View):
    def get(self, request, selected_plan):
        data = request.GET
        plus = int(data.get('plus')) if data.get('plus') else 0

        if request.user.is_authenticated:
            buy_selected_url = reverse("buy-plan-selected", args=[selected_plan])
            return redirect(buy_selected_url)
        else:
            if request.GET.get("next") and request.GET.get("login"):
                print("entrooooooooooooooooooooo")
                try:
                    data = get_buy_plan_dict(request, selected_plan)
                    data['plus'] = plus
                except Exception as e:
                    print("----", e)
                    return redirect("/")
                return render(request, 'buy-plan.html', data)
            else:
                return redirect("/")


class PlanUserBuyCompleteView(View):
    def post(self, request, selected_plan):
        data = json.loads(request.body)
        with_card = data["with_card"]
        card_token = data["card_token"]
        card_expire_month = data["card_expire_month"]
        card_expire_year = data["card_expire_year"]
        coupon = data["coupon"]

        discount = 0
        try:
            with transaction.atomic():
        
                if not request.user.profile.culqi_client_id:
                    culqi_client_id = utils.create_client(request.user)
                    request.user.profile.culqi_client_id = culqi_client_id
                    request.user.profile.save()

                #Validate if it's paying with saved card
                if not with_card:
                    request.user.cards.all().delete()

                    result_culqi_card = utils.create_card(request.user, card_token)
                    if result_culqi_card.get('success') is False:
                        raise CvvInvalidException(result_culqi_card.get('message'))

                    culqi_card = result_culqi_card.get('culqi_card')
                    card = UserCard()
                    card.card_id = culqi_card["id"]
                    card.card_brand = culqi_card["source"]["iin"]["card_brand"]
                    card.card_number_masked = culqi_card["source"]["card_number"]
                    card.card_expire_month = card_expire_month
                    card.card_expire_year = card_expire_year
                    card.all_card_data = culqi_card
                    card.user = request.user
                    card.save()

                #TODO: Validate if user already has a plan

                plan = Plan.objects.get(pk=selected_plan)
                if not request.user.codes.first():
                    UserCode.objects.create(user=request.user, \
                                        code=generate_user_code())
                if coupon:
                    try:
                        user_code = UserCode.objects.exclude(user=request.user).get(code=coupon)
                        coupon_used = UserUsedCoupon.objects.filter(
                            related_user=user_code.user
                        ).exists()
                        if coupon_used:
                            pass   
                        else:
                            config = Config.objects.first()
                            discount = config.discount_percent_on_coupon
                            UserUsedCoupon.objects.create(
                                user=request.user, 
                                related_user=user_code.user,
                                coupon=coupon,
                                plan=plan
                            ).save()

                            #TODO: Send referer email

                    except ObjectDoesNotExist:
                        pass

                # CREAMOS CARGO O NO
                now = arrow.now(settings.PROJECT_TIME_ZONE)
                user_plan = request.user.current_plan.filter(
                    expires_at__gte=now.datetime,
                ).first()
                user_had_plan = True
                user_plan_payment = None
                if not user_plan:
                    result_payment = utils.create_charge(request.user, plan.current_price, discount)
                    if result_payment.get('success') is False:
                        raise chargeException(result_payment.get('message'))
                
                    user_plan_payment = result_payment.get('payment')

                    if plan.days:
                        next_payment = now.shift(days=plan.days)
                    else:
                        next_payment = now.shift(months=plan.months)

                    user_had_plan = False
                    #Copy of plan for user
                    user_plan = utils.create_user_plan(
                        request.user, 
                        plan, 
                        discount, 
                        user_plan_payment, 
                        next_payment.datetime
                    )
                    discount_last_plan = 0
                else:
                    next_payment = arrow.get(user_plan.expires_at)

                last_pending_plan = UserPendingPlan.objects.filter(user=request.user).last()
                if last_pending_plan:
                    discount_last_plan = last_pending_plan.discount
                    # next_payment = arrow.get(last_pending_plan.renew_at)
                    UserPendingPlan.objects.filter(user=request.user).delete()

                #Pending plan for renew
                user_pending_plan = utils.create_user_pending_plan(
                    request.user, 
                    plan, 
                    discount_last_plan,
                    next_payment.datetime + datetime.timedelta(microseconds=1)
                )
            

                messages.add_message(request, messages.INFO, settings.BUY_PLAN_SUCCESS_KEY)

                if plan.days:
                    plan_duration_units = "días" if plan.days > 1 else "día"
                else:
                    plan_duration_units = "meses" if plan.months > 1 else "mes"

                if user_had_plan == False:
                    email_template = render_to_string(
                        "mails/buy_plan_confirmation.html",
                        {
                            'name': request.user.first_name,
                            'plan_name': user_plan.name,
                            'plan_duration': user_plan.days,
                            'plan_duration_units': plan_duration_units,
                            'plan_date_start': user_plan.created_at.strftime("%y/%m/%d"),
                            'plan_date_end': user_plan.expires_at.strftime("%y/%m/%d"),
                            'plan_classes': user_plan.pass_quantity,
                            'plan_repetitions': user_plan.repetitions_quantity,
                            'plan_friendpass': user_plan.friendpass_quantity,
                            'plan_price': user_plan.current_price,
                            'client_code': request.user.codes.first().code,
                            'site_url': settings.SITE_URL,
                        }
                    )
                else:

                    renew_at = arrow.get(user_pending_plan.renew_at, settings.PROJECT_TIME_ZONE)
                    if user_pending_plan.days:
                        new_plan_expires_at = renew_at.shift(days=user_pending_plan.days)
                    else:
                        new_plan_expires_at = renew_at.shift(months=user_pending_plan.months)

                    email_template = render_to_string(
                        "mails/change_plan_confirmation.html",
                        {
                            'name': request.user.first_name,
                            'plan_name': user_pending_plan.name,
                            'plan_duration': user_pending_plan.days,
                            'plan_duration_units': plan_duration_units,
                            'plan_date_start': user_pending_plan.renew_at.strftime("%y/%m/%d"),
                            'plan_date_end': new_plan_expires_at.datetime.strftime("%y/%m/%d"),
                            'plan_classes': user_pending_plan.pass_quantity,
                            'plan_repetitions': user_pending_plan.repetitions_quantity,
                            'plan_friendpass': user_pending_plan.friendpass_quantity,
                            'plan_price': user_pending_plan.current_price,
                            'client_code': request.user.codes.first().code,
                            'site_url': settings.SITE_URL,
                        }
                    )

                print('email_template', email_template)
            
            return JsonResponse({"success":True, 'user_had_plan': user_had_plan})

        except CvvInvalidException as e:
            return JsonResponse({"success":False, "message": str(e)})
        
        except chargeException as e:
            return JsonResponse({"success":False, "message": str(e)})
            
            
class PlanUserBuyConfirmationView(View):
    def get(self, request, selected_plan):
    
        current_messages = messages.get_messages(request)
        data = request.GET
        user_have_plan = data.get('user-have-plan')
        user_have_plan = eval(user_have_plan.capitalize())
        # for message in current_messages:
        #     if str(message.message) == settings.BUY_PLAN_SUCCESS_KEY:
        plan = Plan.objects.get(pk=selected_plan)
        last_pending_plan = UserPendingPlan.objects.filter(user=request.user).last()

        return render(request, 'buy-plan-selected-confirmation.html', {'plan': plan, 'active': 4, 'user_have_plan': user_have_plan, 'date_renew': last_pending_plan.renew_at})


def get_buy_pass_dict(request, selected_sport_center):
    try:
        sport_center = SportCenter.objects.get(pk=selected_sport_center)
    except:
        raise ValueError('Wrong plan selected')

    now = arrow.now(settings.PROJECT_TIME_ZONE)
    
    next_payment = now.shift(months=1)
    
    plans = Plan.objects.all().order_by("order")

    user_plan = None
    if request.user.is_authenticated:
        now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc').datetime
        user_plan = request.user.current_plan.filter(
            expires_at__gte=now,
        ).first()
        
    if user_plan:
        if user_plan.is_premium:
            sport_centers = []
        else:
            sport_centers = list(SportCenter.objects.filter(sell_pass=True,pass_amount__isnull=False).values("id", "name", "pass_amount"))     
    else:
        sport_centers = list(SportCenter.objects.filter(sell_pass=True,pass_amount__isnull=False).values("id", "name", "pass_amount"))

    return {
        'sport_center': sport_center, 
        'now': now,
        'card': get_serialized_card(request) if request.user.is_authenticated else None,
        'next_payment': next_payment,
        'CULQI_PUBLIC_KEY': settings.CULQI_PUBLIC_KEY,
        'active': 4,
        'plans': plans,
        'user_plan': user_plan,
        'sport_centers': json.dumps(sport_centers)
    }

class PassBuySelectedView(View):
    def get(self, request, selected_sport_center): 
        if request.user.is_authenticated:
            try:
                data = get_buy_pass_dict(request, selected_sport_center)
                return render(request, 'buy-pass-selected.html', data)
            except Exception as e:
                print("----", e)
                return redirect("/")
        else:
            buy_selected_url = reverse("buy-pass-selected", args=[selected_sport_center])
            buy_selected_url_auth = reverse("buy-pass-selected-auth", args=[selected_sport_center])
            return redirect(buy_selected_url_auth + "?next={}&login=1".format(buy_selected_url))


class PassBuySelectedAuthView(View):
    def get(self, request, selected_sport_center):
        if request.user.is_authenticated:
            buy_selected_url = reverse("buy-pass-selected", args=[selected_sport_center])
            return redirect(buy_selected_url)
        else:
            if request.GET.get("next") and request.GET.get("login"):
                try:
                    data = get_buy_pass_dict(request, selected_sport_center)
                except Exception as e:
                    return redirect("/")
                return render(request, 'buy-plan.html', data)
            else:
                return redirect("/")


class PassUserBuyCompleteView(View):
    def post(self, request, selected_sport_center):
        data = json.loads(request.body)
        card_token = data["card_token"]
        with_card = data["with_card"]
        card_expire_month = data["card_expire_month"]
        card_expire_year = data["card_expire_year"]
        sport_center = SportCenter.objects.get(pk=selected_sport_center)
        
        # VALIDAMOS SI SE ESTA COMPRANDO UN PASE DE UN CENTRO QUE AUN PUEDO ASISTIR CON MI PLAN
        if request.user.is_authenticated:
            now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc').datetime
            user_plan = request.user.current_plan.filter(
                expires_at__gte=now,
            ).first()
            if user_plan:
                passes_to_use_from_plan = 0
                if not user_plan.is_freeze:
                    passes_to_use_from_plan = user_plan.plan_passes.filter(
                        Q(used=False),
                        Q(buy_reference='PLAN_PASS'),
                        Q(plan__isnull=False),
                        Q(reservation_passes__isnull=True) | Q(reservation_passes__isnull=False, reservation_passes__cancelled=True)
                    ).count()
                if passes_to_use_from_plan > 0:
                    centers_allowed = get_centers_allowed(user_plan)
                    if sport_center.id in centers_allowed:
                        return JsonResponse({"success":False, "message": 'El pase para el centro seleccionado no puede ser adquirido debido a que tu plan actual tiene acceso.'})
        try:
            if not request.user.profile.culqi_client_id:
                culqi_client_id = utils.create_client(request.user)
                request.user.profile.culqi_client_id = culqi_client_id
                request.user.profile.save()

            #Validate if it's paying with saved card
            if not with_card:
                request.user.cards.all().delete()

                result_culqi_card = utils.create_card(request.user, card_token)
                if result_culqi_card.get('success') is False:
                    raise CvvInvalidException(result_culqi_card.get('message'))

                culqi_card = result_culqi_card.get('culqi_card')

                card = UserCard()
                card.card_id = culqi_card["id"]
                card.card_brand = culqi_card["source"]["iin"]["card_brand"]
                card.card_number_masked = culqi_card["source"]["card_number"]
                card.card_expire_month = card_expire_month
                card.card_expire_year = card_expire_year
                card.all_card_data = culqi_card
                card.user = request.user
                card.save()

            #TODO: Validate if user already has a plan
            if not request.user.codes.first():
                UserCode.objects.create(user=request.user, \
                                        code=generate_user_code())

            # CREAMOS CARGO
            result_payment = utils.create_charge(request.user, sport_center.pass_amount, 0)
            if result_payment.get('success') is False:
                raise chargeException(result_payment.get('message'))
            
            user_payment = result_payment.get('payment')

            now = arrow.now(settings.PROJECT_TIME_ZONE)
            next_payment = now.shift(days=settings.PASS_VALID_DAYS)

            user_pass = UserPass()
            user_pass.user = request.user
            user_pass.sport_center = sport_center
            user_pass.transaction = user_payment
            user_pass.buy_reference = settings.PASS
            user_pass.expires_at = next_payment.datetime
            user_pass.save()

            messages.add_message(request, messages.INFO, settings.BUY_PASS_SUCCESS_KEY)

            email_template = render_to_string(
                "mails/buy_pass_confirmation.html",
                {
                    'name': request.user.first_name,
                    'sport_center_name': sport_center.name,
                    'pass_duration': settings.PASS_VALID_DAYS,
                    'pass_date_start': user_pass.created_at.strftime("%y/%m/%d"),
                    'pass_date_end': user_pass.expires_at.strftime("%y/%m/%d"),
                    'pass_price': sport_center.pass_amount,
                    'client_code': request.user.codes.first().code,
                    'site_url': settings.SITE_URL,
                }
            )

            print('email_template', email_template)

            return JsonResponse({"success":True})
        
        except CvvInvalidException as e:
            return JsonResponse({"success":False, "message": str(e)})
        
        except chargeException as e:
            return JsonResponse({"success":False, "message": str(e)})


class PassUserBuyConfirmationView(View):
    def get(self, request, selected_sport_center):
    
        current_messages = messages.get_messages(request)
        
        # for message in current_messages:
        #     if str(message.message) == settings.BUY_PASS_SUCCESS_KEY:
        sport_center = SportCenter.objects.get(pk=selected_sport_center)
        return render(request, 'buy-pass-selected-confirmation.html', {'sport_center': sport_center, 'active': 4})

        return redirect("/")


class ValidateCoupon(View):

    def post(self, request):
        data = json.loads(request.body)
        coupon = data["coupon"].upper()
        plan_id = data["plan_id"]

        if coupon:
            try:
                user_code = UserCode.objects.exclude(user=request.user).get(code=coupon)
                coupon_used = UserUsedCoupon.objects.filter(
                    related_user=user_code.user
                ).exists()

                if coupon_used:
                    return JsonResponse({
                        "error":True,
                        **CODES["COUPON"]["COUPON_USED"],
                    })    
                else:
                    config = Config.objects.first()
                    plan = Plan.objects.get(pk=plan_id)
                    discounted_price = plan.current_price * ((100 - config.discount_percent_on_coupon) / 100)
                    return JsonResponse({
                        "success":True,
                        "discount": config.discount_percent_on_coupon,
                        "discounted_price": '{:.2f}'.format(discounted_price),
                    })    

            except ObjectDoesNotExist:
                return JsonResponse({
                    "error":True,
                    **CODES["COUPON"]["WRONG_COUPON"],
                })
            
        else:
            return JsonResponse({
                "error":True,
            })


class PlanFreezeView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(PlanFreezeView, self).dispatch(*args, **kwargs)

    def post(self, request):
        now = arrow.now(settings.PROJECT_TIME_ZONE)
        user_plan = request.user.current_plan.filter(
            expires_at__gte=now.datetime,
        ).first()
        user_pending_plan = request.user.pending_plan.first()

        if not user_plan:
            return JsonResponse({'success': False, 'message': 'No cuentas con un plan este momento.'})

        if user_plan.is_freeze:
            return JsonResponse({'success': False, 'message': 'Tu plan ya se encuentra congelado.'})
        else:
            if user_plan.months == None or user_plan.months == 1:
                return JsonResponse({'success': False, 'message': 'Tu plan no se puede congelar.'})
        
        # ALARGAMOS LA FECHA DE EXPIRACION EN 10 DÍAS
        new_expires_at = arrow.get(user_plan.expires_at, settings.PROJECT_TIME_ZONE).shift(hours=-5).shift(days=10)
        user_plan.expires_at = new_expires_at.datetime
        # SETEAMOS LA FECHA QUE SE CONGELA
        user_plan.freeze_day = now.datetime
        user_plan.save()

        # ALARGAMOS LA FECHA DE LOS PASES
        passes_to_use_from_plan = user_plan.plan_passes.filter(
            Q(used=False),
            Q(buy_reference='PLAN_PASS'),
            Q(plan__isnull=False),
            Q(reservation_passes__isnull=True) | Q(reservation_passes__isnull=False, reservation_passes__cancelled=True)
        )
        for pase in passes_to_use_from_plan:
            pase.expires_at = new_expires_at.datetime
            pase.save()

        # ALARGAMOS EL PENDING PLAN
        if user_pending_plan and user_pending_plan.renewed is False:
            user_pending_plan.renew_at = new_expires_at.datetime + datetime.timedelta(microseconds=1)
            user_pending_plan.save()

        return JsonResponse({'success': True})