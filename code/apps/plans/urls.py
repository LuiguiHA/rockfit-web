from django.urls import path, re_path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    re_path('^comprar$', views.PlanBuyView.as_view(), name="buy-plan"),
    re_path('^comprar/plan/(\d+)/$', views.PlanBuySelectedView.as_view(), name="buy-plan-selected"),
    re_path('^comprar/plan/(\d+)/auth$', views.PlanBuySelectedAuthView.as_view(), name="buy-plan-selected-auth"),
    re_path('^comprar/plan/(\d+)/complete$', views.PlanUserBuyCompleteView.as_view(), name="buy-plan-selected-complete"),
    re_path('^comprar/plan/(\d+)/confirmacion$', views.PlanUserBuyConfirmationView.as_view(), name="buy-plan-selected-confirmation"),
    re_path('^comprar/pase/(\d+)/$', views.PassBuySelectedView.as_view(), name="buy-pass-selected"),
    re_path('^comprar/pase/(\d+)/auth$', views.PassBuySelectedAuthView.as_view(), name="buy-pass-selected-auth"),
    re_path('^comprar/pase/(\d+)/complete$', views.PassUserBuyCompleteView.as_view(), name="buy-pass-selected-complete"),
    re_path('^comprar/pase/(\d+)/confirmacion$', views.PassUserBuyConfirmationView.as_view(), name="buy-pass-selected-confirmation"),
    re_path('^validate-coupon$', views.ValidateCoupon.as_view(), name="validate-coupon"),
    re_path('^plan-freeze$', views.PlanFreezeView.as_view(), name="plan-freeze"),
]