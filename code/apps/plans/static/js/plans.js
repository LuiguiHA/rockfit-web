// ELEMENT.locale(ELEMENT.lang.es)
var app = new Vue({
  el: '#plans',
  delimiters: ['${', '}'],
  data: {
    sportCenters: window.sportCenters,
    selectedPlanForPass: null,
    passPrice: null,
    plus: window.plus
  },
  watch: {
    selectedPlanForPass: function(){
      var selectedCenter = _.find(function(sc){
        return sc.id == this.selectedPlanForPass
      }.bind(this))(this.sportCenters)
      this.passPrice = selectedCenter.pass_amount
    }
  },
  methods:{
    showRelatedPlan: function(planId){
      var display = document.getElementById("plan-related-"+planId).style.display
      if (display == 'block'){
        document.getElementById("container-"+planId).style.height = "390px"
        document.getElementById("plan-related-"+planId).style.display = "none"
        return
      }
      document.getElementById("container-"+planId).style.height = "755px"
      document.getElementById("plan-related-"+planId).style.display = "block"

    },
    onClickBuyPass: function(e){
      if(this.selectedPlanForPass == null){
        e.preventDefault()
      }
    },
    getPriceString: function(price){
      return price.toFixed(0)
    }
  },
  computed: {
    buyPassUrl: function(){
      var url = window.buyPassUrlFirst.slice(0, -2)
      console.log(url, 'url')
      if(this.selectedPlanForPass == null){
        return ""
      }
      return url + this.selectedPlanForPass
    },
    sportCentersDropdown: function(){
      var values = window._.map(function(element){
        return {'label': element.name, 'value': element.id}
      })(this.sportCenters)
      return values
    }
  },  
  mounted: function(){
    document.getElementById('plans').style.display = 'block';
    // this.selectedPlanForPass = this.sportCenters[0].id
  }
})