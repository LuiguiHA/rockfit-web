// Vue.use(vuelidate.default)
var BeatLoader = VueSpinner.BeatLoader
var app = new Vue({
  el: '#buy-pass',
  delimiters: ['${', '}'],
  components: {
    BeatLoader: BeatLoader
  },
  // mixins: [
  //   window.vuelidate.validationMixin
  // ],
  data: {
    planId: window.planId,
    priceSportCenter: window.priceSportCenter,
    creditCardNumber:"",
    creditCardDate:"",
    creditCardCVV:"",
    card: window.card,
    showCard: false,
    loading: false,
    withCard: null
  },
  validations: {
    creditCardNumber: {
      required: requiredValidator,
      minLength: minLengthValidator(17)
    },
    creditCardDate: {
      required: requiredValidator,
      minLength: minLengthValidator(9)
    },
    creditCardCVV: {
      required: requiredValidator,
      minLength: minLengthValidator(3)
    }
  },
  computed:{
    cardNumber: function(){
      return this.creditCardNumber.replace(" ", "")
    },
    cardMonth: function () {
      var dateSplitted = this.creditCardDate.split("/")

      if(dateSplitted.length == 2){
        return dateSplitted[0].replace(/\s/g, '');
      }
      return '';
    },
    cardYear: function () {
      var dateSplitted = this.creditCardDate.split("/")
      if(dateSplitted.length == 2){
        var year = dateSplitted[1].replace(/\s/g, '');
        return year;
      }
      return '';
    }
  },
  mounted: function(){
    document.getElementById('buy-pass').style.display = 'block';
  },
  created: function () {
    Culqi.publicKey = window.CULQI_PUBLIC_KEY;
    Culqi.init();

    if(this.card){
      this.withCard = true
      this.showCard = false
    } else {
      this.withCard = false
      this.showCard = true
    }
    
  },
  methods:{
    registerPayment: function(cardToken){
      var instance = axios.create({
        timeout: 60000000,
        headers: {'X-CSRFToken': csrf_token}
      })
      instance.post(buyPassSelectedUrl, {
        card_token:cardToken.id,
        card_expire_month: this.cardMonth,
        card_expire_year: this.cardYear,
        with_card: this.withCard
      })
      .then(function (response) {
        this.loading = false
        if(response.data.success){
          window.location = buyConfirmationUrl
        } else {
          if(this.card){ this.withCard = true }
          new Noty({
            type: 'error',
            layout: 'top',
            timeout: 4000,
            text: response.data.message,
            killer: true,
            force: true
          }).show();
        }
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    onClickBuy: function(){
      var showNewCard = document.getElementById('new-card').style.display
      if(showNewCard == 'none'){
        this.loading = true
        this.registerPayment({id: Math.random()})
      } else {
        this.loading = true
        this.$v.$touch();
        if (this.$v.$invalid) {
          this.loading = false
          return;
        }
        Culqi.createToken();
        window.culqi = function() {
          if(Culqi.token) {
            this.withCard = false
            this.registerPayment(Culqi.token)
          } else {
            this.loading = false
            var message = "Hubo un error al registrar la tarjeta."
            switch(Culqi.error.code){
              case "invalid_number":
              message = "El número de tarjeta es inválido."
              break;

              case "invalid_expiration_year":
              message = "El año de expiración es inválido."
              break;

              case "invalid_expiration_month":
              message = "El mes de expiración es inválido."
              break;
              case "invalid_cvv":
              message = "El cvc/cvv es inválido."
              break;
            }
            new Noty({
              type:"error",
              text: message,
              layout: "top",
              timeout: 5000,
              force: true,
              killer: true
            }).show();
          }
        }.bind(this)
        
      }
    }
  },
  
})