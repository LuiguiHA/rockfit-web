// Vue.use(vuelidate.default)
var BeatLoader = VueSpinner.BeatLoader
var app = new Vue({
  el: '#buyPlan',
  delimiters: ['${', '}'],
  components: {
    BeatLoader: BeatLoader
  },
  // mixins: [
  //   window.vuelidate.validationMixin
  // ],
  data: {
    planId: window.planId,
    planPrice: "",
    discount: "",
    ultimatePrice: "",
    creditCardNumber:"",
    creditCardDate:"",
    creditCardCVV:"",
    document: "",
    card: window.card,
    showCard: false,
    coupon: null,
    hasCoupon: false,
    couponButtonTitle: "VALIDAR",
    stateCupon: 0,
    loading: false,
    withCard: null
  },
  validations: {
    creditCardNumber: {
      required: requiredValidator,
      minLength: minLengthValidator(17)
    },
    creditCardDate: {
      required: requiredValidator,
      minLength: minLengthValidator(9)
    },
    creditCardCVV: {
      required: requiredValidator,
      minLength: minLengthValidator(3)
    }
  },
  watch: {
    coupon: function(value){
      if (value == '') { 
        this.stateCupon = 0 
        this.discount = '-'
      }
    }
  },
  computed:{
    cardNumber: function(){
      return this.creditCardNumber.replace(" ", "")
    },
    cardMonth: function () {
      var dateSplitted = this.creditCardDate.split("/")

      if(dateSplitted.length == 2){
        return dateSplitted[0].replace(/\s/g, '');
      }
      return '';
    },
    cardYear: function () {
      var dateSplitted = this.creditCardDate.split("/")
      if(dateSplitted.length == 2){
        var year = dateSplitted[1].replace(/\s/g, '');
        return year;
      }
      return '';
    },
    showPricePlan: function(){
      return parseInt(this.planPrice).toFixed(0)
    },
    showUltimatePrice: function(){
      return parseInt(this.ultimatePrice).toFixed(0)
    }
  },
  mounted: function(){
    this.planPrice = window.planPrice
    this.ultimatePrice = window.planPrice
    document.getElementById('buyPlan').style.display = 'block';
  },
  created: function () {
    Culqi.publicKey = window.CULQI_PUBLIC_KEY;
    Culqi.init();

    
    if(this.card){
      this.withCard = true
      this.showCard = false
    } else {
      this.withCard = false
      this.showCard = true
    }

    var _this = this
    $(document).ready(function(){
      $("input[name='card-radio']").on('change', function(e) {
          _this.cardSelected = ($(this).val() == 'card')
      });  
    })
    
  },
  methods:{
    onValidateCoupon: function(){
      // PARA REMOVER
      if(this.hasCoupon){
        this.hasCoupon = false
        this.planPrice = window.planPrice
        this.ultimatePrice = window.planPrice
        this.couponButtonTitle = "VALIDAR"
        this.coupon = ""
        this.discount = ''
        this.stateCupon = 0
      } else{
        var instance = axios.create({
          timeout: 60000000,
          headers: {'X-CSRFToken': csrf_token}
        })
        instance.post(validateCouponUrl, {
          coupon: this.coupon,
          plan_id: this.planId
        })
        .then(function (response) {
          if(response.data.success){
            this.stateCupon = 1
            this.discount = String(response.data.discount) + "%"
            this.ultimatePrice = response.data.discounted_price
            this.hasCoupon = true
            this.couponButtonTitle = "REMOVER"
          } else {
            this.stateCupon = -1
            this.discount = "Inválido"
            // var message = "Hubo un error validando el cupón."
            // switch(response.data.code){
            //   case 100:
            //     message = "Código de cupón incorrecto."
            //   break;
            //   case 101:
            //     message = "Ya has usado un código de este usuario."
            //   break;
            // }
            // new Noty({
            //   type: 'error',
            //   layout: 'topCenter',
            //   timeout: 2000,
            //   text: message,
            //   killer: true,
            //   force: true
            // }).show();
          }
        }.bind(this))
        .catch(function (error) {
          console.log(error);
        });
      }
      
    },
    registerPayment: function(cardToken){
      var instance = axios.create({
        timeout: 60000000,
        headers: {'X-CSRFToken': csrf_token}
      })
      instance.post(buyPlanSelectedUrl, {
        card_token: cardToken.id,
        coupon: this.coupon,
        card_expire_month: this.cardMonth,
        card_expire_year: this.cardYear,
        with_card: this.withCard
      })
      .then(function (response) {
        this.loading = false
        if(response.data.success){
          buyConfirmationUrl = buyConfirmationUrl + '?user-have-plan=' + response.data.user_had_plan
          console.log(buyConfirmationUrl)
          window.location = buyConfirmationUrl
        } else {
          new Noty({
            type: 'error',
            layout: 'top',
            timeout: 4000,
            text: response.data.message,
            killer: true,
            force: true
          }).show();
        }
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    onClickBuy: function(){
      var showNewCard = document.getElementById('new-card')
      if(showNewCard == null){
        this.loading = true
        this.registerPayment({id: Math.random()})
        return
      }
      if(showNewCard.style.display == 'none'){
        this.loading = true
        this.registerPayment({id: Math.random()})
      } else {
        this.loading = true
        this.$v.$touch();
        if (this.$v.$invalid) {
          this.loading = false
          return;
        }
        Culqi.createToken();
        window.culqi = function() {
          if(Culqi.token) {
            console.log(1)
            this.withCard = false
            console.log("==============")
            console.log(Culqi.token)
            this.registerPayment(Culqi.token)
          } else {
            console.log(2)
            this.loading = false;
            var message = "Hubo un error al registrar la tarjeta."
            switch(Culqi.error.code){
              case "invalid_number":
              message = "El número de tarjeta es inválido."
              break;

              case "invalid_expiration_year":
              message = "El año de expiración es inválido."
              break;

              case "invalid_expiration_month":
              message = "El mes de expiración es inválido."
              break;
              case "invalid_cvv":
              message = "El cvc/cvv es inválido."
              break;
            }
            new Noty({
              type:"error",
              text: message,
              layout: "top",
              timeout: 5000,
              force: true,
              killer: true
            }).show();
          }
        }.bind(this)
        
      }
    }
  },
  
})