from django.conf import settings
from users.models import *
import culqipy
import arrow

culqipy.public_key = settings.CULQI_PUBLIC_KEY
culqipy.secret_key = settings.CULQI_PRIVATE_KEY

def create_client(user):
    data = {
        'address': 'not set yet',
        'address_city': 'Lima',
        'country_code': 'PE',
        'email': user.email,
        'first_name': user.first_name, 
        'last_name': user.last_name,
        'metadata': None, 
        'phone_number': "9000000000"
    }

    response = culqipy.Customer.create(data)
    return response["id"]


def create_card(user, token):
    data = {
        "customer_id": user.profile.culqi_client_id,
        "token_id": token
    }

    response = culqipy.Card.create(data)
    if response.get('object') == 'error':
        return {'success': False, 'message': response.get('user_message')}

    return {'success': True, 'culqi_card': response}


def create_charge(user, amount, discount):
    amount = amount - (amount * (discount / 100))

    if amount < 0:
        amount = 0

    data = {
        "amount": int(amount * 100), 
        "currency_code": "PEN",
        "email": user.email,
        "antifraud_details": {
            "address" : "not set yet",
            "address_city" : "not set yet",
            "country_code" : "PE",
            "first_name" : user.first_name,
            "last_name" : user.last_name,
            "phone_number" : "9000000000"
        },
        "source_id": user.cards.first().card_id
    }

    charge = culqipy.Charge.create(data)
    print('CARGOOOOOOOOOOO')
    print(charge)
    if charge.get('object') == 'charge':
        user_plan_payment = UserPayment()
        user_plan_payment.transaction_id = charge["id"]
        user_plan_payment.amount = charge["amount"]
        user_plan_payment.transaction_date = arrow.get(int(charge["creation_date"]) / 1000).datetime
        user_plan_payment.card_number = charge["source"]["source"]["card_number"]
        user_plan_payment.card_brand = charge["source"]["source"]["iin"]["card_brand"]
        user_plan_payment.card_bank = charge["source"]["source"]["iin"]["issuer"]["name"]
        user_plan_payment.card_bank_country = charge["source"]["source"]["iin"]["issuer"]["country"]
        user_plan_payment.card_bank_country_code = charge["source"]["source"]["iin"]["issuer"]["country_code"]
        user_plan_payment.all_charge_data = charge
        user_plan_payment.save()

        return {'success': True, 'payment': user_plan_payment}
    
    else:
        message = "Error de pago (code: 1000)"

        if charge.get('type') == 'parameter_error':
            message = "Error de parámetro de pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1001)"

        if charge.get('type') == 'invalid_request_error':
            message = "Ocurrió un error inesperado. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1002)"

        if charge.get('type') == 'authentication_error':
            message = "Ocurrió un error inesperado procesando el pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1003)"

        if charge.get('type') == 'limit_api_error':
            message = "Ocurrió un error con la pasarela de pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1005)"

        if charge.get('type') == 'resource_error':
            message = "Ocurrió un error con la pasarela de pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1005)"

        if charge.get('type') == 'api_error':
            message = "Ocurrió un error inesperado procesando el pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1007)"

        if charge.get('type') == 'card_error':

            if charge.get('decline_code'):
                if charge.get('decline_code') == 'insufficient_funds':
                    message = "Fondos insuficientes. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2001)"

                if charge.get('decline_code') == 'expired_card':
                    message = "Tarjeta vencida. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2002)"

                if charge.get('decline_code') == 'stolen_card':
                    message = "La tarjeta ha sido reportada como perdida. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2004)"

                if charge.get('decline_code') == 'contact_issuer':
                    message = "El banco ha rechazado la transacción, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2005)"

                if charge.get('decline_code') == 'invalid_cvv':
                    message = "Código CVV inválido. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2006)"

                if charge.get('decline_code') == 'incorrect_cvv':
                    message = "Código CVV incorrecto. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2007)"

                if charge.get('decline_code') == 'too_many_attempts_cvv':
                    message = "Demasiados intentos para ingresar el código CVV. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2008)"

                if charge.get('decline_code') == 'issuer_not_available':
                    message = "El banco no responde, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2009)"

                if charge.get('decline_code') == 'issuer_decline_operation':
                    message = "El banco ha denegado la transacción, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2010)"

                if charge.get('decline_code') == 'invalid_card':
                    message = "La tarjeta no está permitida para este tipo de transacciones, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2011)"

                if charge.get('decline_code') == 'processing_error':
                    message = "Ocurrió un error inesperado mientras se procesaba el pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2012)"

                if charge.get('decline_code') == 'fraudulent':
                    message = "Transacción sospechosa, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2013)"

                if charge.get('decline_code') == 'culqi_card':
                    message = "Tarjeta no permitida. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2014)"

            if charge.get('code'):
                if charge.get('code') == 'card_declined':
                    message = "Tarjeta rechazada. (code: 2000)"
                else:
                    message = "Ocurrío un error con la tarjeta (code: 2015)"
        
        return {'success': False, 'message': message}


def create_user_plan(user, plan, discount, transaction, next_payment):
    user_plan = UserPlan()
    user_plan.user = user
    user_plan.name = plan.name
    user_plan.discount = discount
    user_plan.last_price = plan.last_price
    user_plan.current_price = plan.current_price
    user_plan.is_premium = plan.is_premium
    user_plan.days = plan.days
    user_plan.months = plan.months
    user_plan.pass_quantity = plan.pass_quantity
    user_plan.repetitions_quantity = plan.repetitions_quantity
    user_plan.friendpass_quantity = plan.friendpass_quantity
    user_plan.transaction = transaction
    user_plan.expires_at = next_payment
    user_plan.save()

    if user_plan.pass_quantity == 0:
        for i in range(150):
            user_pass = UserPass()
            user_pass.user = user
            user_pass.buy_reference = settings.PLAN_PASS
            user_pass.plan = user_plan
            user_pass.transaction = transaction
            user_pass.expires_at = next_payment
            user_pass.save()
    else:
        for i in range(user_plan.pass_quantity):
            user_pass = UserPass()
            user_pass.user = user
            user_pass.buy_reference = settings.PLAN_PASS
            user_pass.plan = user_plan
            user_pass.transaction = transaction
            user_pass.expires_at = next_payment
            user_pass.save()

    for i in range(user_plan.friendpass_quantity):
        user_pass = UserFriendPass()
        user_pass.user = user
        user_pass.plan = user_plan
        user_pass.expires_at = next_payment
        user_pass.save()

    return user_plan


def create_user_pending_plan(user, plan, discount, next_payment):


    user_pending_plan = UserPendingPlan()
    user_pending_plan.user = user
    user_pending_plan.name = plan.name
    user_pending_plan.discount = discount
    user_pending_plan.last_price = plan.last_price
    user_pending_plan.current_price = plan.current_price
    user_pending_plan.is_premium = plan.is_premium
    user_pending_plan.days = plan.days
    user_pending_plan.months = plan.months
    user_pending_plan.pass_quantity = plan.pass_quantity
    user_pending_plan.repetitions_quantity = plan.repetitions_quantity
    user_pending_plan.friendpass_quantity = plan.friendpass_quantity
    # user_pending_plan.transaction = user_pending_plan
    user_pending_plan.renew_at = next_payment
    user_pending_plan.renewed = False
    user_pending_plan.save()

    return user_pending_plan