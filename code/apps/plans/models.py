from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE
# Create your models here.


class Plan(SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE

    name = models.CharField(
        max_length=250,
        verbose_name="Nombre",
    )

    current_price = models.FloatField(
        verbose_name="Precio actual"   
    )

    is_premium = models.BooleanField(
        default=False, 
        verbose_name="Es premium"
    )

    last_price = models.FloatField(
        verbose_name="Precio anterior"
    )

    days = models.IntegerField(
        verbose_name="Días transcurridos después de la compra",
        null=True
    )

    months = models.IntegerField(
        verbose_name="Días transcurridos después de la compra",
        null=True
    )

    pass_quantity = models.IntegerField(
        verbose_name="Cantidad de pases"
    )

    repetitions_quantity = models.IntegerField(
        verbose_name="Cantidad de repeticiones"
    )

    friendpass_quantity = models.IntegerField(
        verbose_name="Cantidad de friendpass"
    )

    is_related = models.BooleanField(
        default=False,
        verbose_name="Es relacionado"
    )

    related_plan = models.ForeignKey(
        'Plan', 
        on_delete=models.CASCADE,
        related_name='related',
        null=True
    )

    related_plan_title = models.CharField(
        max_length=250,
        verbose_name="Título plan relacionado",
        null=True
    )

    related_plan_description = models.CharField(
        max_length=250,
        verbose_name="Descripción plan relacionado",
        null=True
    )

    show_in_home = models.BooleanField(
        default=False,
        verbose_name="Mostrar en home"
    )

    accept_coupons = models.BooleanField(
        default=False,
        verbose_name="Acepta cupones"
    )

    email_message = models.TextField(
        verbose_name="Mensaje para email",
        null=True   
    )

    order = models.IntegerField(
        verbose_name="Orden",
        default=0
    )

    left_color = models.CharField(
        max_length=10
    )

    right_color = models.CharField(
        max_length=10
    )

    home_color = models.CharField(
        max_length=10
    )

    def price_as_string(self):
        return "{}".format(self.current_price)

class ReturnCustomer(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    client = models.CharField(
        max_length=500
    )

    buy_date = models.CharField(
        max_length=50
    ) 

    buy_name = models.CharField(
        max_length=500
    )

    sport_center = models.CharField(
        max_length=500,
        null=True
    )

    amount = models.FloatField()

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name="Fecha de creación"
    )