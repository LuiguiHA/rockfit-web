# -*- coding: utf-8 -*-

import uuid 
import datetime

from django.db import models


class Ubigeo(models.Model):
    name = models.CharField(
        'nombre', 
        max_length=255
    )
    
    parent = models.ForeignKey(
        'Ubigeo', 
        null=True, 
        blank=True, 
        on_delete=models.CASCADE
    )

    @staticmethod
    def get_provinces():
        depaments_ids = Ubigeo.\
                            objects\
                            .filter(parent=None).values('id')

        return Ubigeo.objects.filter(parent__in=depaments_ids)

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Ubigeo'
        verbose_name_plural = 'Ubigeos'
        ordering = ['id']

