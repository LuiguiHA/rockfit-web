from django.db import models

# Create your models here.

class Config(models.Model):
    
    show_premium_filter = models.BooleanField(default=False)
    discount_percent_on_coupon = models.IntegerField()