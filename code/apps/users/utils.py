
from django.db import connection
import shortuuid
from project.utils import dictfetchall
from sport_centers.models import SportCenter
import uuid

def generate_user_code():
     u = shortuuid.uuid()
     return u[:5].upper()

def get_user_activity(user_id, symbol, date_time):
    with connection.cursor() as cursor:
        # "select 
        #         users_useractivityreservation.id as reservation_id,
        #         sport_centers_sportcenter.name as sport_center_name,
        #         sport_centers_sportcenter.id as sport_center_id,
        #         sport_centers_sportcenter.slug,
        #         users_useractivityreservation.reservation_day as day,
        #         users_useractivityreservation.weight as weight,
        #         to_char(sport_centers_sportcenteractivityschedule.start_at, 'fmHH12:MI pm') as time,
        #         to_char(sport_centers_sportcenteractivityschedule.end_at, 'fmHH12:MI pm') as time_finish,
        #         sport_centers_sportcenteractivity.name as activity_name,
        #         sport_centers_sportcenteractivity.has_teacher as has_teacher,
        #         sport_centers_sportcenteractivity.teacher_name as teacher_name,
        #         CASE 
        #             WHEN users_userpass.plan_id is NULL THEN 'Pase individual'
        #             WHEN users_userpass.plan_id is NOT NULL THEN 
        #                 CASE 
        #                     WHEN users_userplan.expires_at < now() THEN CONCAT('Plan: ', to_char(users_userplan.created_at at time zone 'America/Lima', 'DD/MM/YYYY'), ' - ', to_char(users_userplan.expires_at at time zone 'America/Lima', 'DD/MM/YYYY'))
        #                     WHEN users_userplan.expires_at > now() THEN 'Membresía actual'
        #                 END
        #         END as buy_type,
        #         CASE
        #             WHEN EXTRACT('epoch' FROM to_timestamp(concat(
        #                         users_useractivityreservation.reservation_day,
        #                         ' ',
        #                         sport_centers_sportcenteractivityschedule.start_at
        #                     ), 'YYYY-MM-DD HH24:MI:SS'
        #                 )::timestamp - %s::timestamptz::timestamp) / 60 >= 240 THEN True
        #             WHEN EXTRACT('epoch' FROM to_timestamp(concat(
        #                         users_useractivityreservation.reservation_day,
        #                         ' ',
        #                         sport_centers_sportcenteractivityschedule.start_at
        #                     ), 'YYYY-MM-DD HH24:MI:SS'
        #                 )::timestamp - %s::timestamptz::timestamp) / 60 < 240 THEN False
        #         END as cancellable
        #     from users_useractivityreservation
        #     inner join sport_centers_sportcenteractivityschedule on users_useractivityreservation.activity_schedule_id = sport_centers_sportcenteractivityschedule.id
        #     inner join users_userpass on users_userpass.id = users_useractivityreservation.related_pass_id
        #     inner join sport_centers_sportcenteractivity on sport_centers_sportcenteractivity.id = sport_centers_sportcenteractivityschedule.activity_id
        #     inner join sport_centers_sportcenter on sport_centers_sportcenter.id = sport_centers_sportcenteractivity.sport_center_id
        #     join users_userplan on users_userplan.id = users_userpass.plan_id
        #     and to_timestamp(concat(
        #             users_useractivityreservation.reservation_day, 
        #             ' ', 
        #             sport_centers_sportcenteractivityschedule.start_at
        #         ), 'YYYY-MM-DD HH24:MI:SS'
        #     )::timestamp {} %s
        #     and users_userpass.user_id = %s
        #     and users_useractivityreservation.cancelled = false"
        query = """select 
                sport_centers_sportcenteractivityschedule.id as activity_schedule_id,
                users_useractivityreservation.id as reservation_id,
                sport_centers_sportcenter.name as sport_center_name,
                sport_centers_sportcenter.id as sport_center_id,
                sport_centers_sportcenter.slug,
                users_useractivityreservation.reservation_day as day,
                users_useractivityreservation.weight as weight,
                to_char(sport_centers_sportcenteractivityschedule.start_at, 'HH12:MI pm') as time,
                sport_centers_sportcenteractivityschedule.start_at as time_origin,
                to_char(sport_centers_sportcenteractivityschedule.end_at, 'HH12:MI pm') as time_finish,
                sport_centers_sportcenteractivity.name as activity_name,
                sport_centers_sportcenteractivity.has_teacher as has_teacher,
                sport_centers_sportcenteractivity.teacher_name as teacher_name,
                to_char(users_userpass.created_at, 'YYYY-MM-DD HH24:MI:SS') as start_at,
                to_char(users_userpass.expires_at, 'YYYY-MM-DD HH24:MI:SS') as end_at,
                users_userpass.plan_id as plan,
                CASE
                    WHEN EXTRACT('epoch' FROM to_timestamp(concat(
                                users_useractivityreservation.reservation_day,
                                ' ',
                                sport_centers_sportcenteractivityschedule.start_at
                            ), 'YYYY-MM-DD HH24:MI:SS'
                        )::timestamp - %s::timestamptz::timestamp) / 60 >= 240 THEN True
                    WHEN EXTRACT('epoch' FROM to_timestamp(concat(
                                users_useractivityreservation.reservation_day,
                                ' ',
                                sport_centers_sportcenteractivityschedule.start_at
                            ), 'YYYY-MM-DD HH24:MI:SS'
                        )::timestamp - %s::timestamptz::timestamp) / 60 < 240 THEN False
                END as cancellable
                from users_useractivityreservation
                inner join sport_centers_sportcenteractivityschedule on users_useractivityreservation.activity_schedule_id = sport_centers_sportcenteractivityschedule.id
                inner join users_userpass on users_userpass.id = users_useractivityreservation.related_pass_id
                inner join sport_centers_sportcenteractivity on sport_centers_sportcenteractivity.id = sport_centers_sportcenteractivityschedule.activity_id
                inner join sport_centers_sportcenter on sport_centers_sportcenter.id = sport_centers_sportcenteractivity.sport_center_id
                and to_timestamp(concat(
                        users_useractivityreservation.reservation_day, 
                        ' ', 
                        sport_centers_sportcenteractivityschedule.start_at
                    ), 'YYYY-MM-DD HH24:MI:SS'
                )::timestamp {} %s
                and users_userpass.user_id = %s
                and users_useractivityreservation.cancelled = false""".format(symbol)
        cursor.execute(
            query, 
            [date_time, date_time, date_time, user_id] 
        )

        # print("query", query)
        # print("date_time", date_time)
        # print("user_id", user_id)

        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

def get_repetitions(plan_id):
    used_repetitons = []
    with connection.cursor() as cursor:
        # REPETICIONES UTILIZADAS
        cursor.execute(
            """
                SELECT  
                sport_centers_sportcenter.slug,  
                sport_centers_sportcenter.name,
                sport_centers_sportcentergroup.name as group_name,
                sport_centers_sportcenter.group_id as group_id
                from users_userpass
                INNER JOIN users_useractivityreservation on users_useractivityreservation.related_pass_id = users_userpass.id
                    inner join sport_centers_sportcenteractivityschedule on sport_centers_sportcenteractivityschedule.id = users_useractivityreservation.activity_schedule_id
                inner join sport_centers_sportcenteractivity on sport_centers_sportcenteractivity.id = sport_centers_sportcenteractivityschedule.activity_id
                inner join sport_centers_sportcenter on sport_centers_sportcenter.id = sport_centers_sportcenteractivity.sport_center_id
                left join sport_centers_sportcentergroup on sport_centers_sportcentergroup.id = sport_centers_sportcenter.group_id
                WHERE buy_reference = 'PLAN_PASS' 
                AND plan_id = %s 
                AND users_useractivityreservation.cancelled = false
            """, [
                plan_id
            ]
        )
        
        repetitons = dictfetchall(cursor)
        centers = []
        for repetition in repetitons:
            is_group = True if repetition.get('group_name') else False
            if is_group:
                name = repetition.get('group_name')
            else: 
                name = repetition.get('name')

            if name not in centers:
                if is_group:
                    new_dict = {
                        'name': repetition.get('group_name'),
                        'count': 1,
                        'centers': [{'name': repetition.get('name'), 'slug': repetition.get('slug')}]
                    }
                else:
                    new_dict = repetition
                    new_dict['count'] = 1

                used_repetitons.append(new_dict)
                centers.append(name)
            else:
                for used_repetiton in used_repetitons:
                    if used_repetiton.get('name') == name:
                        if is_group:
                            used_repetiton['count'] = used_repetiton.get('count') + 1
                            center_exists = False
                            for center in used_repetiton.get('centers'):
                                if repetition.get('name') == center.get('name'):
                                    center_exists = True
                                    break
                            
                            if not center_exists:
                                sport_centers = used_repetiton.get('centers')
                                sport_centers.append({'name': repetition.get('name'), 'slug': repetition.get('slug')})
                                used_repetiton['centers'] = sport_centers
                            break
                        else:
                            used_repetiton['count'] = used_repetiton.get('count') + 1
                            break

    return used_repetitons


def get_centers_allowed(user_plan):
    used_repetitions = get_repetitions(user_plan.id)
    centers_not_allowed = []
    group_not_allowed = []
    for used_repetition in used_repetitions:
        if used_repetition.get('count') == user_plan.repetitions_quantity:
            if used_repetition.get('centers'):
                group_not_allowed.append(used_repetition.get('name'))
            else:
                centers_not_allowed.append(used_repetition.get('name'))

    sport_centers = list(SportCenter.objects.filter(name__in=centers_not_allowed).values_list("id", flat=True))
    sport_centers = sport_centers + list(SportCenter.objects.filter(group__name__in=group_not_allowed).values_list("id", flat=True))

    if not user_plan.is_premium:
        sport_centers = list(SportCenter.objects.filter(is_premium=False, sell_pass=True,pass_amount__isnull=False).exclude(id__in=sport_centers).values_list("id", flat=True))
    else:
        sport_centers = list(SportCenter.objects.all(sell_pass=True,pass_amount__isnull=False).exclude(id__in=sport_centers).values_list("id", flat=True))
    # if not user_plan.is_premium:
    #     sport_centers = sport_centers + list(SportCenter.objects.filter(is_premium=True, sell_pass=True,pass_amount__isnull=False).values_list("id", flat=True))
    
    return sport_centers