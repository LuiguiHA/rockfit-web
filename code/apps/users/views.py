from django.conf import settings
from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.core.exceptions import ObjectDoesNotExist
from django.views import View
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout,update_session_auth_hash
from django.db import connection
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.template.loader import render_to_string
from project.error_codes import CODES
from django.core.mail import send_mail
from project.utils import dictfetchall
from users.models import UserCard, UserCode, UserPlan, UserPass, \
    UserActivityReservation, UserActivityReservationFriendpass
from users.utils import * 
from plans.utils import create_card, create_user_pending_plan
import json
import math
import arrow
import locale
import pytz
import datetime

# Create your views here.
class Register(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(Register, self).dispatch(request, *args, **kwargs)

    def post(self, request):
        data = json.loads(request.body)
        email = data.get("email").lower()
        password = data.get("password")

        user_exists = User.objects.filter(email=email).exists()
        if user_exists:
            return JsonResponse({"email": False})
        else:
            user = User()
            user.first_name = data.get("name")
            user.last_name = data.get("lastname")
            user.email = email
            user.username = email
            user.set_password(password)
            user.save()
            if data.get('phone'):
                user.profile.phone = data.get("phone")
            if data.get('birthday'):
                user.profile.birthday = data.get('birthday')
            user.profile.save()

            auth_user = authenticate(username=email, password=password)
            login(request, auth_user)

            subject = "Confirmación de Registro" 
            email_template = render_to_string(
                "mails/registration_confirmation.html",
                {
                    'name': user.first_name,
                    'site_url': settings.SITE_URL,
                }
            )
            print('envio correoooooooo')
            send_mail(
                subject,
                'Texto plano',
                settings.DEFAULT_FROM_EMAIL,
                [email],
                fail_silently=True,
                html_message=email_template,
            )
            print('finish correooooooooo')
            return JsonResponse({"success": True})


class Login(View):
    def post(self, request):
        data = json.loads(request.body)
        email = data.get("email").lower()
        password = data.get("password")

        user = authenticate(request, username=email, password=password)
        if user is not None:
            login(request, user)
            return JsonResponse({'success': True})
        else:
            return JsonResponse({'success': False})

class Logout(View):
    def get(self, request):
        logout(request)
        return redirect("/")


class MyAccount(View):
    def get(self, request):
        used_plan_passes = 0
        total_passes = 0
        now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc').datetime
        current_plan = request.user.current_plan.filter(
            expires_at__gte=now,
        ).first()
        current_plan_price = None
        plan_is_freeze = False
        can_plan_freeze = False
        if current_plan:
            plan_is_freeze = current_plan.is_freeze
            if not plan_is_freeze:
                can_plan_freeze = True if current_plan.months != None and current_plan.months > 1 else False
            total_passes = current_plan.pass_quantity
            current_plan_price = '{:.2f}'.format(current_plan.current_price)

        passes = UserPass.objects.filter(user=request.user, buy_reference='PASS', expires_at__gte=now).count()
        total_passes = total_passes + passes
        pending_plan = request.user.pending_plan.filter(
            renew_at__gte=now,
            renewed=False
        ).first()

        pending_plan_price = None
        if pending_plan:
            pending_plan_price = '{:.2f}'.format(pending_plan.current_price)
        
        coming_activities = get_user_activity(request.user.pk, ">", now)
        coming_activities = sorted(coming_activities, key=lambda item: item['time_origin'], reverse=False)
        coming_activities = sorted(coming_activities, key=lambda item: item['day'], reverse=False)
        for activity in coming_activities:
            del activity['time_origin']
            activity['day_for_cancel'] = activity.get('day').strftime('%Y-%m-%d')
            activity['day'] = activity.get('day').strftime('%d/%m/%Y')
            inviteds = UserActivityReservationFriendpass.objects.filter(user_activity_reservation__id=activity.get('reservation_id')).count()
            activity['inviteds'] = inviteds
        
        plan_active = []
        if current_plan:
            used_plan_passes = []
            plan_passes = list(request.user.passes.filter(
                transaction=current_plan.transaction,
                buy_reference="PLAN_PASS"
            ).values_list('id', flat=True))
            used_plan_passes = UserActivityReservation.objects.filter(related_pass__id__in=plan_passes, cancelled=False).count()
            
            plan_active = [{'created_at': current_plan.created_at, 
                            'name': current_plan.name, 
                            'transaction__amount': current_plan.transaction.amount,
                            'days_to_expire': current_plan.days_to_renew,
                            'expires_at': current_plan.expires_at}
                          ]

        # PASES ACTIVOS
        active_passes = list(UserPass.objects.filter(buy_reference=settings.PASS, expires_at__gte=now, user=request.user).values('created_at', 'sport_center__name', 'sport_center__slug', 'transaction__amount', 'expires_at'))
        active_passes_total = active_passes + plan_active
        active_passes_total = sorted(active_passes_total, key=lambda item: item['created_at'], reverse=True)
        for passe in active_passes_total:
            passe['transaction__amount'] = passe['transaction__amount'] / 100
            if passe.get('sport_center__name'):
                seconds = (passe['expires_at'] - arrow.now(settings.PROJECT_TIME_ZONE).datetime).total_seconds()
                minutes = (seconds / 60) / 1440
                passe['days_to_expire'] = round(minutes)
                

        used_repetitons = []
        friendpass_available = []
        friendpass_used = []
        friendpass_available_count = 0
        centers_visit = []
        repetitions_final = []
        if current_plan:
            used_repetitons = get_repetitions(current_plan.id)
            repetitions_final = []
            for used_repetiton in used_repetitons:
                if used_repetiton.get('count') == current_plan.repetitions_quantity:
                    repetitions_final.append(used_repetiton)
                    
            with connection.cursor() as cursor:
                # VISITAS POR CENTRO DEPORTIVO
                cursor.execute(
                    """
                        select
                        CASE
		                    WHEN sport_centers_sportcenter.group_id is NULL THEN sport_centers_sportcenter.name
		                    WHEN sport_centers_sportcenter.group_id is NOT NULL THEN sport_centers_sportcentergroup.name
		                END as sport_name,
                        count(users_useractivityreservation.id) as y
                        from auth_user
                        inner join users_userplan on users_userplan.user_id = auth_user.id
                        inner join users_userpass on users_userpass.plan_id = users_userplan.id
                        inner join users_useractivityreservation on users_useractivityreservation.related_pass_id = users_userpass.id
                        inner join sport_centers_sportcenteractivityschedule on sport_centers_sportcenteractivityschedule.id = users_useractivityreservation.activity_schedule_id
                        inner join sport_centers_sportcenteractivity on sport_centers_sportcenteractivity.id = sport_centers_sportcenteractivityschedule.activity_id
                        inner join sport_centers_sportcenter on sport_centers_sportcenter.id = sport_centers_sportcenteractivity.sport_center_id
                        left join sport_centers_sportcentergroup on sport_centers_sportcentergroup.id = sport_centers_sportcenter.group_id
                        where  auth_user.id = %s
                        and users_userplan.expires_at > now()
                        and users_userplan.deleted is null
                        and users_userpass.buy_reference = 'PLAN_PASS'
                        and users_useractivityreservation.cancelled = false
                        and users_useractivityreservation.deleted is NULL
                        group by sport_centers_sportcenter.group_id, sport_name
                        order by count(users_useractivityreservation.id) desc
                    """, [request.user.id]
                )
                centers_visit = dictfetchall(cursor)
            

            friendpass_available_count = current_plan.plan_friend_passes.filter(
                reservation_friendpass__isnull=True
            ).count()

            friendpass_used = current_plan.plan_friend_passes.filter(
                reservation_friendpass__isnull=False
            )

            friendpass_used_date_format = "%Y-%m-%d"  
            friendpass_used = [\
                {\
                    "date": x.reservation_friendpass.first().user_activity_reservation.reservation_day.strftime(friendpass_used_date_format), \
                    "sport_center_name": x.reservation_friendpass.first().get_sport_center().name,\
                    "sport_center_slug": x.reservation_friendpass.first().get_sport_center().slug,\
                    "reservation_id": x.reservation_friendpass.first().user_activity_reservation.id
                } \
                for x in friendpass_used]
        
        return render(request, 'my-account.html', {
            'current_plan': current_plan,
            'plan_is_freeze': plan_is_freeze,
            'can_plan_freeze': can_plan_freeze,
            'price_plan': current_plan_price,
            'pending_plan': pending_plan,
            'pending_plan_price': pending_plan_price,
            'total_passes': total_passes,
            'coming_activities': json.dumps(coming_activities),
            'used_plan_passes': used_plan_passes,
            'used_repetitions': repetitions_final,
            'friendpass_available_count': friendpass_available_count,
            'friendpass_used': friendpass_used,
            'CULQI_PUBLIC_KEY': settings.CULQI_PUBLIC_KEY,
            'active_passes_total': active_passes_total,
            'centers_visit': centers_visit
        })

    def post(self, request):
        data = json.loads(request.body)
        page = data.get('page')
        mode = int(data.get('mode'))
        now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc')
        if mode == 1:
            # PLANES EXPIRADOS
            expired_plans = list(UserPlan.objects.filter(expires_at__lt=now.datetime, user=request.user).values('created_at', 'name', 'transaction__amount', 'expires_at'))

            # PASES EXPIRADOS
            expired_passes = list(UserPass.objects.filter(buy_reference=settings.PASS, expires_at__lt=now.datetime, user=request.user).values('created_at', 'sport_center__name', 'sport_center__slug', 'transaction__amount', 'expires_at'))
            
            # PASES TOTALES
            data_list = expired_plans + expired_passes
            data_list = sorted(data_list, key=lambda item: item['created_at'], reverse=True)
            for passe in data_list:
                passe['transaction__amount'] = passe['transaction__amount'] / 100
        
        if mode == 2:
            # VISITAS PASADAS
            data_list = get_user_activity(request.user.pk, "<", now.datetime - datetime.timedelta(seconds=1))
            data_list = sorted(data_list, key=lambda item: item['day'], reverse=True)
            for data in data_list:
                data['buy_type'] = 'Pase individual'
                if data.get('plan'):
                    created_day = arrow.get(data.get('start_at')).shift(hours=-5)
                    expired_day = arrow.get(data.get('end_at')).shift(hours=-5)
                    if now > expired_day:
                        data['buy_type'] = 'Plan: ' + created_day.format('DD/MM/YYYY') + ' - ' + expired_day.format('DD/MM/YYYY')
                    else:
                        data['buy_type'] = 'Membresía actual'


        paginator = Paginator(data_list, 7)
        data_list = paginator.page(page)
        have_more = data_list.has_next()
        return JsonResponse({'data_list': data_list.object_list, 'have_more': have_more})


class UpdateMyInformation(View):
    def post(self, request):
        data = json.loads(request.body)
        email = data.get("email")
        first_name = data.get("firstName")
        last_name = data.get("lastName")
        document_type = data.get("documentType")
        document_number = data.get("documentNumber")
        mobile_phone = data.get("mobilePhone")
        phone = data.get("phone")
        birthday = None
        if data.get('birthday'):
            birthday = arrow.get(data.get('birthday')).format('YYYY-MM-DD')

        user = User.objects.filter(email=email).exclude(pk=request.user.id)
        user_exists = user.exists()


        if user_exists:
            return JsonResponse({
                "error": "Email ya está siendo usado", 
                "success": False
            })
        else:
            request.user.first_name = first_name
            request.user.last_name = last_name
            request.user.email = email
            request.user.username = email
            request.user.save()

            request.user.profile.document_type = document_type
            request.user.profile.document_number = document_number
            request.user.profile.mobile_phone = mobile_phone
            request.user.profile.phone = phone
            request.user.profile.birthday = birthday
            request.user.profile.save()

            return JsonResponse({
                "success": True
            })

class UpdateMyCode(View):
    def post(self, request):    
        UserCode.objects.filter(user=request.user).delete()
        new_code = generate_user_code()
        UserCode.objects.create(user=request.user, code=new_code)
        return JsonResponse({
            "success": True,
            "code": new_code
        })


class UpdateCard(View):
    def post(self, request):
        data = json.loads(request.body)
        card_token = data.get("card_token")
        card_expire_month = data.get("card_expire_month")
        card_expire_year = data.get("card_expire_year")

        request.user.cards.all().delete()

        culqi_card = create_card(request.user, card_token)
        culqi_card = culqi_card['culqi_card']
        card = UserCard()
        card.card_id = culqi_card["id"]
        card.card_brand = culqi_card["source"]["iin"]["card_brand"]
        card.card_number_masked = culqi_card["source"]["card_number"]
        card.card_expire_month = card_expire_month
        card.card_expire_year = card_expire_year
        card.all_card_data = culqi_card
        card.user = request.user
        card.save()

        card = request.user.cards.first()

        return JsonResponse({
            "success": True,
            "card_brand": card.card_brand,
            "card_number_masked": card.card_number_masked,
            "card_expire_month": card.card_expire_month,
            "card_expire_year": card.card_expire_year,
        })

class CancelPendingPlan(View):
    def post(self, request):
        now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc').datetime
        pending_plan = request.user.pending_plan.filter(
            renew_at__gte=now,
            renewed=False
        ).first()
        if pending_plan:
            pending_plan.delete()

        current_plan = request.user.current_plan.filter(
            expires_at__gte=now,
        ).first()
        email_template = render_to_string(
            "mails/cancel_plan_notification.html",
            {
                'name': request.user.first_name,
                'plan_end_date': current_plan.expires_at.strftime("%y-%m-%d"),
                'site_url': settings.SITE_URL,
            }
        )

        print('email_template', email_template)

        return JsonResponse({
            "success": True,
        })


class CancelReservation(View):
    def post(self, request):
        now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc').datetime
        data = json.loads(request.body)
        reservation_id = data.get("reservation_id")
        
        try:
            reservation = request.user.passes.get(
                reservation_passes__id=reservation_id,
                used=False,
                reservation_passes__cancelled=False
            ).reservation_passes.first()

            reservation_date = arrow.get(
                datetime.datetime.combine(
                    reservation.reservation_day, 
                    reservation.activity_schedule.start_at
                )
            , settings.PROJECT_TIME_ZONE).datetime

            minutes = (reservation_date - now).total_seconds() / 60.0

            if minutes > settings.HOURS_BEFORE_RESERVATION_USED * 60:
                
                reservation.cancelled = True
                reservation.save()

                return JsonResponse({
                    "success": True
                })
            else:
                return JsonResponse({
                    "error": True,
                    **CODES["RESERVATIONS"]["RESEVATION_OUT_OF_TIME"],
                })
        except ObjectDoesNotExist:
            return JsonResponse({
                "error": True,
                **CODES["RESERVATIONS"]["RESERVATION_DO_NOT_EXIST"],
            })

@method_decorator(csrf_exempt, name='dispatch')
class ValidateAssistanceAdmin(View):
    def post(self, request):
        data = json.loads(request.body)
        dni = data.get("dni")
        code = data.get("code")

        user = None

        sport_center = request.user.admin_sport_center.sport_center

        try:

            if dni and code:
                user = User.objects.get(
                    profile__document_number=dni,
                    codes__code=code,
                )
            elif dni and not code:
                user = User.objects.get(
                    profile__document_number=dni,
                )
            elif code and not dni:
                user = User.objects.get(
                    codes__code=code,
                )
        except ObjectDoesNotExist:
            pass
        
        if not user:
            return JsonResponse({
                "error": True,
                **CODES["RESERVATIONS"]["USER_DOES_NOT_EXIST"],
            })
        else:

            #TODO: Is consumed the pass that is closest to the current date, no matter if it comes from plan or individual pass

            user_dict = {
                "name": user.first_name + " " + user.last_name,
                "client_code": user.codes.last().code,
                "dni": user.profile.document_number,
            }

            now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc').datetime
            current_plan = user.current_plan.filter(
                expires_at__gte=now,
            ).first()

            have_plan_pass = False 

            current_plan_information = None

            current_plan_date_format = "%y-%m-%d"

            if current_plan:
                passes_used_on_sport_center = current_plan.plan_passes.filter(
                    sport_center=sport_center,
                    used=True,
                ).count()

                available_passes = current_plan.plan_passes.filter(
                    used=False,
                    assisted_at__isnull=True,
                ).count()

                available_friendpass = current_plan.plan_friend_passes.filter(
                    reservation_friendpass__isnull=True
                ).count()

                current_plan_information = {
                    "name": current_plan.name,
                    "start": current_plan.created_at.strftime(current_plan_date_format),
                    "end": current_plan.expires_at.strftime(current_plan_date_format),
                    "available_passes_on_sport_center": 2 - passes_used_on_sport_center if available_passes > 0 else 0,
                    "available_passes": available_passes,
                    "available_friendpass": available_friendpass
                }

            reservation_info = None

            reservations = UserActivityReservation.objects.filter(
                related_pass__user=user,
                reservation_day=now.date()
            )

            reservation_datetime_format = "%I:%M %p"
            date_format = "%y/%m/%d"

            for reservation in reservations:
                reservation_start = \
                    reservation.activity_schedule.start_at
                date_start = \
                    now.replace(
                        hour = reservation_start.hour,
                        minute = reservation_start.minute,
                        second = reservation_start.second,
                        microsecond = reservation_start.microsecond,
                    )
                reservation_end = \
                    reservation.activity_schedule.end_at
                date_end = \
                    now.replace(
                        hour = reservation_end.hour,
                        minute = reservation_end.minute,
                        second = reservation_end.second,
                        microsecond = reservation_end.microsecond,
                    )

                date_start_for_validation = date_start - datetime.timedelta(minutes=30)
                date_end_for_validation = date_start + datetime.timedelta(minutes=30)

                # print("date_start_for_validation", date_start_for_validation, now)
                # print("date_end_for_validation", date_end_for_validation, now)
                print()

                friendpass_count = reservation.reservation_friendpass.all().count()

                if date_start_for_validation < now and date_end_for_validation > now:
                    reservation_info = {
                        "reservation": reservation.activity_schedule.activity.name,
                        "start": reservation.activity_schedule.start_at.strftime(reservation_datetime_format).upper(),
                        "end": reservation.activity_schedule.end_at.strftime(reservation_datetime_format).upper(),
                        "origin": reservation.related_pass.buy_reference,
                        "related_pass_id": reservation.related_pass.id,
                        "related_pass_expire_date": reservation.related_pass.expires_at.strftime(date_format),
                        "friendpass_count": friendpass_count
                        # ""
                    }
                    break

            passes_not_from_plan = None

            passes_not_from_plan = UserPass.objects.filter(
                buy_reference=settings.PASS, 
                expires_at__gte=now,
                sport_center=sport_center,
                used=False,
                assisted_at__isnull=True,
            ).order_by("-expires_at")

            passes_not_from_plan_dates = [x.expires_at.strftime(current_plan_date_format) for x in passes_not_from_plan]

            passes_to_use_from_plan = []

            if current_plan_information and current_plan_information.get("available_passes_on_sport_center") > 0:
                passes_to_use_from_plan = current_plan.plan_passes.filter(
                    used=False,
                    assisted_at__isnull=True,
                )

            all_passes = list(passes_to_use_from_plan) + list(passes_not_from_plan)
            pass_to_use = list(sorted(all_passes, key=lambda x: x.expires_at))

            print("pass_to_use", pass_to_use)

            pass_id = None

            if len(pass_to_use) > 0:
                pass_id = pass_to_use[0].id


            return JsonResponse({
                "success": True,
                "user": user_dict,
                "have_reservation": reservation_info is not None,
                "reservation": reservation_info,
                "plan": current_plan_information,
                "passes_not_from_plan": passes_not_from_plan_dates,
                "pass_id": pass_id
            })


@method_decorator(csrf_exempt, name='dispatch')
class ConfirmAssistanceAdmin(View):
    def post(self, request):
        data = json.loads(request.body)
        pass_id = data.get("pass_id")

        now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc').datetime

        try:
            user_pass = UserPass.objects.get(pk=pass_id)
            user_pass.assisted_at = now
            user_pass.used = True
            user_pass.save()

            user = user_pass.user
            current_plan = user.current_plan.filter(
                expires_at__gte=now,
            ).first()

            if current_plan:
                plan_expires_in_days = (current_plan.expires_at - now)

                passes_to_use_from_plan = current_plan.plan_passes.filter(
                    used=False,
                    assisted_at__isnull=True,
                )

                reservations = UserActivityReservation.objects.filter(
                    related_pass__user=user,
                    reservation_day__gte=now.date()
                )

                email_template = render_to_string(
                    "mails/plan_update_notification.html",
                    {
                        'name': user.first_name,
                        'plan_name': current_plan.name,
                        'plan_date_end_days': plan_expires_in_days.days,
                        'plan_date_end': current_plan.expires_at.strftime("%y/%m/%d"),
                        'plan_is_ilimitaded': current_plan.pass_quantity < 0,
                        'plan_classes': passes_to_use_from_plan.count(),
                        'plan_active_reservations': reservations.count(),
                        'site_url': settings.SITE_URL,
                    }
                )

                print('email_template', email_template)

            return JsonResponse({
                "success": True,
            })
        except Exception as e:
            print(e)
            return JsonResponse({
                "error": True,
            })


class HavePlan(View):
    def get(self, request):
        have_plan = False
        is_premium = False
        plan_is_freeze = False
        can_plan_freeze = False
        have_pending_plan = False
        if request.user.is_authenticated:
            now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc').datetime
            current_plan = request.user.current_plan.filter(
                expires_at__gte=now,
            ).first()
            
            if current_plan:
                have_plan = True
                pending_plan = request.user.pending_plan.filter(
                    renew_at__gte=now,
                    renewed=False
                ).first()
                if pending_plan:
                    have_pending_plan = True

                plan_is_freeze = current_plan.is_freeze
                if not plan_is_freeze:
                    can_plan_freeze = True if current_plan.months != None and current_plan.months > 1 else False
                is_premium = current_plan.is_premium
        return JsonResponse({'success': have_plan, 'is_premium': is_premium, 'plan_is_freeze': plan_is_freeze, 'can_plan_freeze': can_plan_freeze, 'have_pending_plan': have_pending_plan})


class changePassword(View):
    def post(self,request):
        user_object = request.user
        data = json.loads(request.body)
        password = data.get('password')
        newpassword = data.get('newPassword')
        user = authenticate(request, username=user_object.email, password=password)
        if user is not None:
            user_object.set_password(newpassword)
            user_object.save()
            update_session_auth_hash(request, user_object)
            return JsonResponse({'success': True})
        else:
            return JsonResponse({'success': False})


class ReactivePlan(View):
    def post(self,request):
        now = arrow.now(settings.PROJECT_TIME_ZONE).replace(tzinfo='utc').datetime
        user_plan = request.user.current_plan.filter(
            expires_at__gte=now,
        ).first()
        if user_plan:
            create_user_pending_plan(request.user, user_plan, 0, arrow.get(user_plan.expires_at).datetime + datetime.timedelta(microseconds=1))
            return JsonResponse({'success': True})
        else:
            return JsonResponse({'success': False, 'message': 'No cuentas con un plan actual'})