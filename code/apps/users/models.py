from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE
from django.contrib.postgres.fields import JSONField
import arrow
import math
from .utils import generate_user_code
# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(
        User, 
        on_delete=models.CASCADE,
        related_name='profile'
    )

    document_type = models.CharField(
        verbose_name="Tipo de documento",
        max_length=200,
        null=True
    )

    document_number = models.CharField(
        verbose_name="Número de documento",
        max_length=200,
        null=True
    )

    mobile_phone = models.CharField(
        verbose_name="Teléfono celular",
        max_length=200,
        null=True
    )

    phone = models.CharField(
        verbose_name="Teléfono",
        max_length=200,
        null=True
    )
    birthday = models.CharField(
        verbose_name="Fecha de nacimiento",
        max_length=200,
        null=True
    )

    culqi_client_id = models.CharField(
        verbose_name="Culqi client id",
        max_length=200,
        null=True
    )


class UserCode(SafeDeleteModel):

    code = models.CharField(
        max_length=100,
        verbose_name="Código"
    )

    user = models.ForeignKey(
        User, 
        on_delete=models.CASCADE,
        related_name='codes'
    )

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance, phone=0)
        # UserCode.objects.create(user=instance, code=generate_user_code())


class UserPlan(SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE

    user = models.ForeignKey(
        User, 
        on_delete=models.CASCADE,
        related_name='current_plan'
    )

    name = models.CharField(
        max_length=100,
        verbose_name="Nombre del plan"
    )

    last_price = models.FloatField(
        verbose_name="Último precio"   
    )

    current_price = models.FloatField(
        verbose_name="Precio actual"   
    )

    is_premium = models.BooleanField(
        default=False, 
        verbose_name="Es premium"
    )

    last_price = models.FloatField(
        verbose_name="Precio anterior"
    )

    days = models.IntegerField(
        verbose_name="Días transcurridos después de la compra",
        null=True
    )

    months = models.IntegerField(
        verbose_name="Días transcurridos después de la compra",
        null=True
    )

    pass_quantity = models.IntegerField(
        verbose_name="Cantidad de pases"
    )

    repetitions_quantity = models.IntegerField(
        verbose_name="Cantidad de repeticiones"
    )

    friendpass_quantity = models.IntegerField(
        verbose_name="Cantidad de friendpass"
    )

    expires_at = models.DateTimeField(
        verbose_name="Fecha de expiración"
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name="Fecha de creación"
    )

    freeze_day = models.DateTimeField(
        null=True,
        verbose_name="Fecha de congelación",
    )

    discount = models.PositiveIntegerField(
        default=0,
        verbose_name="Descuento"
    )

    transaction = models.ForeignKey(
        'UserPayment', 
        on_delete=models.CASCADE,
        related_name='payment_info'
    )

    @property
    def days_to_renew(self):
        now = arrow.now(settings.PROJECT_TIME_ZONE) \
              .replace(hour=0, minute=0, second=0, microsecond=0).datetime
        expires_at = arrow.get(self.expires_at, settings.PROJECT_TIME_ZONE) \
                   .replace(hour=0, minute=0, second=0, microsecond=0).datetime
        diff = expires_at - now
        seconds = diff.total_seconds()
        minutes = (seconds / 60) / 1440
        return round(minutes)

    @property
    def is_freeze(self):
        if self.freeze_day:
            now = arrow.now(settings.PROJECT_TIME_ZONE) \
                  .replace(hour=0, minute=0, second=0, microsecond=0)
            start_freeze = arrow.get(self.freeze_day, \
                           settings.PROJECT_TIME_ZONE) \
                           .replace(hour=0, minute=0, second=0, microsecond=0)
            end_freeze = start_freeze.shift(days=10)
            if now >= start_freeze and now <= end_freeze:
                return True
            return False
        return False 

    @property
    def days_to_active(self):
        if self.freeze_day:
            now = arrow.now(settings.PROJECT_TIME_ZONE) \
                  .replace(hour=0, minute=0, second=0, microsecond=0)
            freeze_day = arrow.get(self.freeze_day, \
                         settings.PROJECT_TIME_ZONE) \
                         .replace(hour=0, minute=0, second=0, microsecond=0)
            end_freeze = freeze_day.shift(days=10)
            diff = end_freeze - now
            seconds = diff.total_seconds()
            minutes = (seconds / 60) / 1440
            return round(minutes)
        return 0


class UserPayment(models.Model):

    _safedelete_policy = SOFT_DELETE

    amount = models.FloatField(
        verbose_name="Monto"
    )

    transaction_id = models.CharField(
        max_length=100,
        verbose_name="Código de transacción"
    )
    transaction_date = models.DateTimeField(
        verbose_name="Fecha de transacción"
    )
    card_number = models.CharField(
        max_length=100,
        verbose_name="Número de tarjeta"
    )
    card_brand = models.CharField(
        max_length=100,
        verbose_name="Marca de tarjeta"
    )
    card_bank = models.CharField(
        max_length=100,
        verbose_name="Banco de tarjeta",
        null=True
    )
    card_bank_country = models.CharField(
        max_length=100,
        verbose_name="País del banco de tarjeta",
        null=True
    )
    card_bank_country_code = models.CharField(
        max_length=100,
        verbose_name="Código del país del banco de tarjeta",
        null=True
    )

    all_charge_data = JSONField(
        verbose_name="Charge data"
    )

    def get_price(self):
        return '$' + '{:.2f}'.format(self.amount) if self.amount else ''
    get_price.short_description = 'Precio'

    def category_name(self):
        if self.card_category:
            return self.card_category
        else:
            return '-'
    category_name.short_description = 'Categoría de tarjeta'

    def __str__(self):
        return ""

    @property
    def real_amount(self):
        return self.amount / 100

    class Meta():
        verbose_name = "Registro de pago"
        verbose_name_plural = "Registros de pago"


class UserCard(SafeDeleteModel):
    card_id = models.CharField(
        verbose_name="Número de tarjeta",
        max_length=200
    )

    card_brand = models.CharField(
        verbose_name="Tipo de tarjeta",
        max_length=200
    )

    card_number_masked = models.CharField(
        verbose_name="Número de tarjeta enmascarado",
        max_length=200
    )

    card_expire_month = models.CharField(
        verbose_name="Mes de expiración",
        max_length=2
    )

    card_expire_year = models.CharField(
        verbose_name="Año de expiración",
        max_length=4
    )

    all_card_data = JSONField(
        verbose_name="Card data"
    )

    user = models.ForeignKey(
        User, 
        on_delete=models.CASCADE,
        related_name='cards'
    )


class UserPass(SafeDeleteModel):

    user = models.ForeignKey(
        User, 
        on_delete=models.CASCADE,
        related_name='passes'
    )

    sport_center = models.ForeignKey(
        'sport_centers.SportCenter', 
        on_delete=models.CASCADE,
        null=True,
        related_name='user_passes'
    )

    plan = models.ForeignKey(
        UserPlan, 
        on_delete=models.CASCADE,
        null=True,
        related_name="plan_passes"
    )

    transaction = models.ForeignKey(
        UserPayment, 
        on_delete=models.CASCADE,
        related_name='passes',
        null=True
    )

    buy_reference = models.CharField(
        verbose_name="Referencia de compra",
        max_length=200,
        null=True
    )

    expires_at = models.DateTimeField(
        verbose_name="Fecha de expiración"
    )

    used = models.BooleanField(
        default=False,
        verbose_name="Usado",
    )

    assisted_at = models.DateTimeField(
        verbose_name="Fecha de asistencia",
        null=True,
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name="Fecha de creación"
    )

    @property
    def days_to_expire(self):
        diff = self.expires_at - self.created_at
        return diff.days


class UserFriendPass(SafeDeleteModel):


    user = models.ForeignKey(
        User, 
        on_delete=models.CASCADE,
        related_name='friend_passes'
    )

    sport_center = models.ForeignKey(
        'sport_centers.SportCenter', 
        on_delete=models.CASCADE,
        null=True,
    )

    plan = models.ForeignKey(
        UserPlan, 
        on_delete=models.CASCADE,
        null=True,
        related_name="plan_friend_passes"
    )

    expires_at = models.DateTimeField(
        verbose_name="Fecha de expiración"
    )

    assisted_at = models.DateTimeField(
        verbose_name="Fecha de asistencia",
        null=True,
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name="Fecha de creación"
    )


class UserPendingPlan(SafeDeleteModel):

    name = models.CharField(
        max_length=100,
        verbose_name="Nombre del plan"
    )

    last_price = models.FloatField(
        verbose_name="Último precio"   
    )

    current_price = models.FloatField(
        verbose_name="Precio actual"   
    )

    is_premium = models.BooleanField(
        default=False, 
        verbose_name="Es premium"
    )

    last_price = models.FloatField(
        verbose_name="Precio anterior"
    )

    days = models.IntegerField(
        verbose_name="Días transcurridos después de la compra",
        null=True
    )

    months = models.IntegerField(
        verbose_name="Días transcurridos después de la compra",
        null=True
    )

    pass_quantity = models.IntegerField(
        verbose_name="Cantidad de pases"
    )

    repetitions_quantity = models.IntegerField(
        verbose_name="Cantidad de repeticiones"
    )

    friendpass_quantity = models.IntegerField(
        verbose_name="Cantidad de friendpass"
    )

    user = models.ForeignKey(
        User, 
        on_delete=models.CASCADE,
        related_name='pending_plan'
    )

    discount = models.PositiveIntegerField(
        default=0,
        verbose_name="Descuento"
    )

    renew_at = models.DateTimeField(
        verbose_name="Fecha de renovación",
        null=True,
    )

    renewed = models.BooleanField(
        default=False, 
        verbose_name="Renovado"
    )

    notified_count = models.PositiveIntegerField(
        default=0,
        verbose_name="Cantidad de notificaciones"
    )

    @property
    def days_to_renew(self):
        now = arrow.now(settings.PROJECT_TIME_ZONE) \
              .replace(hour=0, minute=0, second=0, microsecond=0)
        renew_at = arrow.get(self.renew_at, settings.PROJECT_TIME_ZONE) \
                   .replace(hour=0, minute=0, second=0, microsecond=0)
        diff = renew_at - now
        seconds = diff.total_seconds()
        minutes = (seconds / 60) / 1440
        return round(minutes)

class UserActivityReservation(SafeDeleteModel):

    activity_schedule = models.ForeignKey(
        'sport_centers.SportCenterActivitySchedule', 
        on_delete=models.CASCADE,
    )

    reservation_day = models.DateField(
        verbose_name="Fecha",
    )

    cancelled = models.BooleanField(
        verbose_name="Cancelado",
        default=False
    )

    related_pass = models.ForeignKey(
        UserPass, 
        on_delete=models.CASCADE,
        related_name='reservation_passes',
        null=True
    )

    notified_count = models.PositiveIntegerField(
        default=0,
        verbose_name="Cantidad de notificaciones"
    )

    notified_not_assisted = models.BooleanField(
        default=False,
        verbose_name="Notificado cuando no asistió"
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name="Fecha de creación"
    )

    weight = models.FloatField(
        null=True
    )

class UserActivityReservationFriendpass(SafeDeleteModel):

    user_activity_reservation = models.ForeignKey(
        UserActivityReservation, 
        on_delete=models.CASCADE,
        related_name="reservation_friendpass"
    )

    friendpass = models.ForeignKey(
        UserFriendPass,
        on_delete=models.CASCADE,
        related_name="reservation_friendpass"
    )

    first_name = models.CharField(
        max_length=100,
        verbose_name="Nombres",
        null=True
    )

    last_name = models.CharField(
        max_length=100,
        verbose_name="Apellidos",
        null=True
    )

    document_number = models.CharField(
        max_length=100,
        verbose_name="Número de documento",
        null=True
    )

    email = models.CharField(
        max_length=100,
        verbose_name="Email",
        null=True
    )

    def get_sport_center(self):
        return self.user_activity_reservation.activity_schedule.activity.sport_center


class UserUsedCoupon(SafeDeleteModel):


    user = models.ForeignKey(
        User, 
        on_delete=models.CASCADE,
        related_name='used_coupons'
    )

    related_user = models.ForeignKey(
        User, 
        on_delete=models.CASCADE,
    )

    coupon = models.CharField(
        verbose_name="Código de cupón",
        max_length=200,
        null=True
    )

    plan = models.ForeignKey(
        'plans.Plan', 
        on_delete=models.CASCADE
    )


class AdminSportCenter(SafeDeleteModel):
    user = models.OneToOneField(
        User, 
        on_delete=models.CASCADE,
        related_name='admin_sport_center'
    )

    sport_center = models.OneToOneField(
        'sport_centers.SportCenter',
        on_delete=models.CASCADE
    )

