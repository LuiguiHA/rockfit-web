from django.urls import path, re_path
from django.contrib import admin
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    re_path('^register-user$', views.Register.as_view(), name="register-user"),
    re_path('^cerrar-sesion$', views.Logout.as_view(), name="logout-user"),
    re_path('^iniciar-sesion$', views.Login.as_view(), name="login-user"),
    re_path('^mi-cuenta$', views.MyAccount.as_view(), name="my-account"),
    re_path('^update-my-information$', views.UpdateMyInformation.as_view(), name="update-my-information"),
    re_path('^update-my-code$', views.UpdateMyCode.as_view(), name="update-my-code"),
    re_path('^update-card$', views.UpdateCard.as_view(), name="update-card"),
    re_path('^cancel-pending-plan$', views.CancelPendingPlan.as_view(), name="cancel-pending-plan"),
    re_path('^reactive-plan$', views.ReactivePlan.as_view(), name="reactive-plan"),
    re_path('^cancel-reservation$', views.CancelReservation.as_view(), name="cancel-reservation"),
    re_path('^have-plan$', views.HavePlan.as_view(), name="have-plan"),
    re_path('^change-password$', views.changePassword.as_view(), name="change-password"),
    re_path('^reset-password/$', auth_views.password_reset, name='password_reset'),
    re_path('^reset-password/hecho/$', auth_views.password_reset_done, name='password_reset_done'),
    re_path('^reset-password/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    re_path('^reset-password/completo/$', auth_views.password_reset_complete, name='password_reset_complete'),
    path('validate-assistance', views.ValidateAssistanceAdmin.as_view(), name='validate-assistance'),
    path('confirm-assistance', views.ConfirmAssistanceAdmin.as_view(), name='confirm-assistance'),
]