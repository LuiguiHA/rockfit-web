ELEMENT.locale(ELEMENT.lang.es)
var BeatLoader = VueSpinner.BeatLoader
var auth = new Vue({
  el: '#auth',
  delimiters: ['${', '}'],
  data: {
    code: window.code,
    havePlan: null,
    havePlanUrl: window.havePlanUrl,
    email: "",
    password: "",
    name: "",
    lastname: "",
    phone: "",
    terms: false,
    birthday: "",
    next: null,
    username: window.username,
    email: window.email,
    disabledDialog: true,
    showForgotModal: false,
    loadingLogin: false,
    loadingRegister: false,
    canPlanFreeze: false,
    planIsFreeze: false,
    havePendingPlan: null,
    errorLogin: false
  },
  validations: {
    email: {
      email: emailValidator,
      required: requiredValidator,
    },
    password: {
      required: requiredValidator,
      minLength: minLengthValidator(5)
    },
    name: {
      required: requiredValidator,
      minLength: minLengthValidator(3),
      alphaRegex: alphaValidator
    },
    lastname: {
      required: requiredValidator,
      minLength: minLengthValidator(3),
      alpha: alphaValidator
    },
    phone: {
      integer: integerValidator,
      minLength: minLengthValidator(7)
    },
    birthday: {
    },
    terms: {
      required: requiredValidator
    }
  },
  components: {
    BeatLoader: BeatLoader
  },
  created: function(){
    // this.$on('change-name', username => {
    //   console.log(username)
    //   this.username = username
    // });
    this.loadHavePlan();
  },
  mounted: function(){
    window.fbAsyncInit = function() {
      FB.init({
        appId            : '593533767786436',
        autoLogAppEvents : true,
        xfbml            : true,
        version          : 'v3.2'
      });
    };
  
    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));


    if(this.username) this.username = this.username.split(' ')[0]
    
    document.getElementById('auth').style.display = 'block';
    this.next = $.urlParam("next")
    if(this.next){
      // this.disabledDialog = false
      $(document).keyup(function(e) {
        if (e.keyCode == 27) e.stopPropagation();
      });
    }
    var login = $.urlParam("login")
    if(login == 1){
      this.showLoginModal(false)      
    }
    var _this = this
    $('.my-plan').on("click", function(){
      _this.hideMenu();
      app.selectedTab = 'my-plan'
      $('.tab-pane.active').removeClass('active')
      $('#my-plan').addClass('active')
      $('.tab-menu.active').removeClass('active')
      $('.plan').addClass('active')
    })
    $('.my-info').on("click", function(){
      _this.hideMenu();
      app.selectedTab = 'my-info'
      $('.tab-pane.active').removeClass('active')
      $('#my-info').addClass('active')
      $('.tab-menu.active').removeClass('active')
      $('.plan').addClass('active')
    })
  },
  methods:{
    registerWithFacebook: function(){
      FB.login(function(response) {
        if (response.authResponse) {
          var userId = response.authResponse.userID
          var url = "/" + userId + "/"
          FB.api(url,
                {fields: 'email,first_name,last_name'},
                function (response) {
                  console.log(response)
                  if (response && !response.error) {
                    this.email = response.email
                    this.name = response.first_name
                    this.lastname = response.last_name
                  }
                }.bind(this)
            );
        } 
        // else {
        //  console.log('User cancelled login or did not fully authorize.');
        // }
      }.bind(this), {scope: 'email'});
    },
    showModalChangePassword: function(){
      this.hideMenu();
      app.changePasswordModal = true
    },
    hideMenu: function(){
      document.getElementById('menu-account').style.height = '0';
      setTimeout(function(){ 
        document.getElementById('menu-account').style.opacity = '0';
      }, 200);
      document.getElementById('menu-account').style.pointerEvents = 'none';
    },
    openSportCenterModal: function(){
      window.home.openSportCenterModal()
    },
    resetData: function(){
      this.email = ''
      this.name = ''
      this.lastname = ''
      this.phone = ''
      this.password = ''
    },
    hideAllModals: function(){
      this.resetData()
      this.$v.$reset()
      this.$modal.hide("signin")
      this.$modal.hide("signup")
      this.$modal.hide("forgot-password")
    },
    showLoginModal: function(value){
      this.showForgotModal = false
      this.hideAllModals()
      this.$nextTick(function(){
        this.$modal.show("signin")
        // $(".nav-box").hide()
        // $(".bars").removeClass("open")
      }.bind(this))
      if(value){
        $(".nav-box").hide()
        $(".bars").removeClass("open")
      }
    },
    hideModalSignup: function(){
      this.$modal.hide("signup")
    },
    hideModalLogin: function(){
      if(this.disabledDialog == false){
        return
      }
      this.$modal.hide("signin")
      this.showForgotModal = false
    },
    showRegisterModal: function(value){
      this.hideAllModals()
      this.$nextTick(function(){
        this.$modal.show("signup")
        // $(".nav-box").hide()
        // $(".bars").removeClass("open")
      }.bind(this))
      if(value){
        $(".nav-box").hide()
        $(".bars").removeClass("open")
      }
    },
    // showForgotModal: function(e){
    //   e.preventDefault()
    //   this.hideAllModals()
    //   this.$nextTick(function(){
    //     this.$modal.show("forgot-password")
    //     // this.$v.email.$touch()
    //   }.bind(this))
    // },
    registerUser: function(){
      this.loadingRegister = true
      if (this.$v.$invalid == false) {
         var instance = axios.create({
          timeout: 500000,
          headers: {'X-CSRFToken': csrf_token}
        })
        instance.post(registerUserUrl, {
          email: this.email,
          name: this.name,
          lastname: this.lastname,
          password: this.password,
          phone: this.phone,
          birthday: this.birthday != null && this.birthday != '' ? moment(this.birthday).format('YYYY-MM-DD') : null
        })
        .then(function (response) {
          this.loadingRegister = false
          // console.log(response);
          if(response.data.success){
            if(this.next){
              window.location = this.next
            } else {
              window.location.reload()    
            }
            
          } else {
            new Noty({
              type: 'error',
              layout: 'top',
              timeout: 2000,
              text: 'Este correo ya está siendo usado',
              killer: true,
              force: true
            }).show();
          }
          
        }.bind(this))
        .catch(function (error) {
          console.log(error);
        });
      } else {
        if(this.terms == false){
          new Noty({
            type: 'error',
            layout: 'topCenter',
            timeout: 2000,
            text: 'Debes aceptar los términos y condiciones',
            killer: true,
            force: true
          }).show();
        }
      }

     
    },
    loginUser: function(){
      this.loadingLogin = true
      var instance = axios.create({
        timeout: 5000,
        headers: {'X-CSRFToken': csrf_token}
      })

      instance.post(loginUserUrl, {
        email: this.email,
        password: this.password
      })
      .then(function (response) {
        this.loadingLogin = false
        if(response.data.success){
          this.errorLogin = false
          if(this.next){
            window.location = this.next
          } else {
            window.location.reload()    
          }
        } else {
          this.errorLogin = true
          // new Noty({
          //   type: 'error',
          //   layout: 'topCenter',
          //   timeout: 2000,
          //   text: 'Credenciales incorrectas',
          //   killer: true,
          //   force: true
          // }).show();
        }
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    sendPasswordReset: function(){
      var instance = axios.create({
        timeout: 500000,
        headers: {'X-CSRFToken': csrf_token}
      })

      var form = new FormData()
      form.append("email", this.email)

      instance.post(resetPasswordUrl, form)
      .then(function (response) {
      })
      .catch(function (error) {
        console.log(error);
      });
      this.showForgotModal = false
      this.hideAllModals()

      new Noty({
        type: 'success',
        layout: 'top',
        theme: 'mint',
        timeout: 7000,
        text: 'Te acabo de enviar un correo electrónico indicando dónde puedes cambiar tu contraseña. De nada (:',
        killer: true,
        force: true
      }).show();
    },
    loadHavePlan: function(){
      axios.get(this.havePlanUrl)
      .then(function(response){
        this.havePlan = response.data.success
        this.havePendingPlan = response.data.have_pending_plan
        this.planIsFreeze = response.data.plan_is_freeze
        this.canPlanFreeze = response.data.can_plan_freeze
      }.bind(this))
    }
  }
})