ELEMENT.locale(ELEMENT.lang.es)
var BeatLoader = VueSpinner.BeatLoader
var app = new Vue({
  el: '#my-account',
  delimiters: ['${', '}'],
  data: {
    code: window.code,
    planFreezeUrl: window.planFreezeUrl,
    changePasswordUrl: window.changePasswordUrl,
    havePlan: window.havePlan,
    creditCardNumber:"",
    creditCardDate:"",
    creditCardCVV:"",
    loading: false,
    loadingCard: false,
    loadingPassword: false,
    showCard: false,
    haveMoreSalesExpired: false,
    haveMorePastVisits: false,
    pageSalesExpired: 1,
    pagePastVisits: 1,
    pastVisits: [],
    passesList: [],
    selectedTab: null,
    errorPassword: false,
    actualPassword: '',
    newPassword: '',
    confirmPassword: '',
    friendpassUsed: window.friendpassUsed,
    friendpassModal: false,
    closeSesionModal: false,
    changePasswordModal: false,
    referFriendpassModal: false,
    cancelPlanModal: false,
    gridData: [],
    loadingCancel: false,
    centersVisit: window.centersVisit,
    totalVisitsCenters: 0,
    canceledReserveDialog: false,
    loadingCancelReserve: false,
    reservationId: 0,
    scheduleId: 0,
    reserveDay: '',
    reservations: window.reservations,
    classInfo: null,
    showDialogClass: false,
    age: null,
    canPlanFreeze: window.canPlanFreeze,
    planIsFreeze: window.planIsFreeze,
    freezePlanModal: false,
    reactiveModal: false,
    loadingFreeze: false,
    existePendingPlan: window.existePendingPlan,
    today: window.today,
    dateFreeze: window.dateFreeze,
    dataDiscount: [{'prop_1': 'Descuento', 'prop_2': '30%', 'prop_3': '40%', 'prop_4': '50%'}],
    card: {
      cardBrand: window.cardBrand,
      cardNumberMasked: window.cardNumberMasked,
      cardExpireMonth: window.cardExpireMonth,
      cardExpireYear: window.cardExpireYear,
    },
    user: {
      firstName: window.firstName,
      lastName: window.lastName,
      phone: window.phone,
      email: window.email,
      birthday: window.birthday
    },
    userEdit: {
      firstName: window.firstName,
      lastName: window.lastName,
      phone: window.phone,
      email: window.email,
      birthday: window.birthday
    },
    gaugeOptions: null,
    usedClass: window.usedClass,
    totalPasses: window.totalPasses,
    editProfile: false,
  },
  validations: {
    userEdit: {
      firstName:{
        required: requiredValidator,
        minLength: minLengthValidator(3),
        alphaRegex: alphaValidator
      },
      lastName:{
        required: requiredValidator,
        minLength: minLengthValidator(3),
        alphaRegex: alphaValidator
      },
      email:{
        required: requiredValidator,
        email: emailValidator
      },
      phone:{
        integer: integerValidator,
        minLength: minLengthValidator(7)
      },
      birthday: {
      }
    },
    creditCardNumber: {
      required: requiredValidator,
      minLength: minLengthValidator(17)
    },
    creditCardDate: {
      required: requiredValidator,
      minLength: minLengthValidator(9)
    },
    creditCardCVV: {
      required: requiredValidator,
      minLength: minLengthValidator(3)
    },
    actualPassword: {
      required: requiredValidator,
      minLength: minLengthValidator(4)
    },
    newPassword: {
      required: requiredValidator,
      minLength: minLengthValidator(4)
    },
    confirmPassword: {
      required: requiredValidator,
      minLength: minLengthValidator(4)
    },
  },
  components: {
    BeatLoader: BeatLoader
  },
  computed:{
    cardNumber: function(){
      return this.creditCardNumber.replace(" ", "")
    },
    cardMonth: function () {
      var dateSplitted = this.creditCardDate.split("/")

      if(dateSplitted.length == 2){
        return dateSplitted[0].replace(/\s/g, '');
      }
      return '';
    },
    cardYear: function () {
      var dateSplitted = this.creditCardDate.split("/")
      if(dateSplitted.length == 2){
        var year = dateSplitted[1].replace(/\s/g, '');
        return year;
      }
      return '';
    }
  }, 
  watch: {
    confirmPassword: function(){
      this.equalPassword();
    },
    newPassword: function(){
      this.equalPassword();
    }
  },
  mounted: function(){
    moment.locale('es')
    moment.updateLocale('es',{
      months:['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio',
              'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'
             ],
      weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']
    })
    Culqi.publicKey = window.CULQI_PUBLIC_KEY;
    Culqi.init();
    if(window.birthday){
      this.age = moment(new Date()).diff(moment(window.birthday), 'year')
      this.user.birthday = moment(this.user.birthday).format('DD/MM/YYYY')
      this.userEdit.birthday = moment(this.userEdit.birthday).toDate()
    }

    document.getElementById('my-account').style.display = 'block';
    var _this = this
    $(document).ready(function(){
      $("select[name='document_type']").on('change', function(e) {
          _this.user.documentType = $(this).val()
      });  
    })
    this.gaugeOptions = {
      chart: {
          type: 'solidgauge',
          height: '170',
          plotBorderWidth: 0,
          margin:  [0,0,0,0]
      },
      title: null,
      pane: {
          center: ['50%', '70%'],
          size: '140%',
          startAngle: -90,
          endAngle: 90,
          background: {
            backgroundColor: 'white',
              innerRadius: '85%',
              outerRadius: '100%',
              shape: 'arc',
              borderWidth: 0
          }
      },
      tooltip: {
          enabled: false
      },
      
      // the value axis
      yAxis: {
          stops: [
              [1, '#4918a2']
          ],
          lineWidth: 0,
          minorTickInterval: null,
          tickAmount: 1,
          tickPositioner: function() {
            return [this.min, this.max];
          },
          tickWidth: 0,
          labels: {
              y: 16,
              distance: -7,
              style: {
                color: 'white',
                fontSize: '13px'
              }
          }
      },
      plotOptions: {
          solidgauge: {
              innerRadius: '85%',
              dataLabels: {
                  y: 5,
                  borderWidth: 1,
                  borderColor: 'red',
                  useHTML: true,
                  color: 'white',
                  formatter: function(){
                    return "<span style='color:white'>" + this.y + "</span>"
                  }
              },
              borderWidth: 2,
              borderColor: '#282828'
          },
          gauge: {
            dial: {
              borderWidth: 0
            }
          }
      }
    };
    this.selectedTab = 'my-activity'
    if(this.havePlan){
      this.loadClassGraphic();
      this.loadSportCenterGraphic();
    }
    this.loadSalesExpired();
    this.loadPastVisits();
    this.formatFriendPassUsed();
    document.getElementById("user-code").style.display = "block"
    
  },
  methods:{
    freezePlan: function(){
      this.loadingFreeze = true
      var instance = axios.create({
        timeout: 30000,
        headers: {'X-CSRFToken': csrf_token}
      })
      instance.post(this.planFreezeUrl)
      .then(function(response){
        this.loadingFreeze = false
        this.freezePlanModal = false
        if(response.data.success){
          this.planIsFreeze = true
          new Noty({
            type: 'success',
            layout: 'topCenter',
            timeout: 2000,
            text: 'Plan congelado correctamente',
            killer: true,
            force: true
          }).show();
          location.reload()
        }
        else{
          new Noty({
            type: 'error',
            layout: 'top',
            timeout: 4000,
            text: response.data.message,
            killer: true,
            force: true
          }).show();
        }
      }.bind(this))
    },
    reactivePlanDate: function(day){
      return moment(day).add(10, 'day').format('DD/MM/YYYY')
    },
    showClass: function(clas){
      this.classInfo = clas
      this.showDialogClass = true
    },
    formatDateReserve: function(day){
      var newDay = day.split('/')[2] + '-' + day.split('/')[1] + '-' + day.split('/')[0]
      return moment(newDay).format('dddd[,] DD MMMM')
    },
    equalPassword: function(){
      if(this.newPassword == this.confirmPassword) this.errorPassword = false
      else this.errorPassword = true
    },
    changePassword: function(){
      this.loadingPassword =  true
      var instance = axios.create({
        timeout: 30000,
        headers: {'X-CSRFToken': csrf_token}
      })
      instance.post(this.changePasswordUrl, 
      {
        password: this.actualPassword,
        newPassword: this.newPassword
      })
      .then(function (response) {
        this.loadingPassword = false
        if(response.data.success){
          this.changePasswordModal = false
          return
        }
        new Noty({
          type: 'error',
          layout: 'topCenter',
          timeout: 4000,
          text: 'Contraseña actual incorrecta.',
          killer: true,
          force: true
        }).show();
      }.bind(this))
    },
    showCanceledReserveDialog: function(reservation_id, activity_schedule_id, day){
      this.reservationId = reservation_id
      this.reserveDay = day
      this.scheduleId = activity_schedule_id
      this.canceledReserveDialog = true
    },
    cancelReservation: function(){
      this.loadingCancelReserve = true
      var instance = axios.create({
        timeout: 30000,
        headers: {'X-CSRFToken': csrf_token}
      })
      instance.post(cancelReservation, 
        {activity_schedule_id: this.scheduleId,
         date: this.reserveDay
      })
      .then(function (response) {
        console.log(response)
        this.loadingCancelReserve = false
        this.canceledReserveDialog = false
        if(response.data.success){
          document.getElementById("reservation-" + this.reservationId).remove()
          new Noty({
            type: 'success',
            layout: 'topCenter',
            timeout: 2000,
            text: 'Reserva cancelada correctamente',
            killer: true,
            force: true
          }).show();
        }
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    cancelPendingPlan: function(){
      this.loadingCancel = true
      var instance = axios.create({
        timeout: 30000,
        headers: {'X-CSRFToken': csrf_token}
      })
      instance.post(cancelPendingPlan)
      .then(function (response) {
        this.loadingCancel = false
        this.cancelPlanModal = false
        new Noty({
          type: 'success',
          layout: 'topCenter',
          timeout: 2000,
          text: 'Plan cancelado correctamente',
          killer: true,
          force: true
        }).show();
        location.reload();
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    reactivePlan: function(){
      this.loadingCancel = true
      var instance = axios.create({
        timeout: 30000,
        headers: {'X-CSRFToken': csrf_token}
      })
      instance.post(reactivePlan)
      .then(function (response) {
        this.loadingCancel = false
        this.reactiveModal = false
        new Noty({
          type: 'success',
          layout: 'top',
          timeout: 2000,
          text: 'Se reactivó la membresía correctamente',
          killer: true,
          force: true
        }).show();
        location.reload();
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    renewCode: function(){
      var instance = axios.create({
        timeout: 30000,
        headers: {'X-CSRFToken': csrf_token}
      })
      instance.post(updateMyCode)
      .then(function (response) {
        console.log(response)
        this.code = response.data.code
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    addCard: function(cardToken){
      var instance = axios.create({
        timeout: 30000,
        headers: {'X-CSRFToken': csrf_token}
      })
      instance.post(updateCardUrl, {
        card_token: cardToken.id,
        card_expire_month: this.cardMonth,
        card_expire_year: this.cardYear,
      })
      .then(function (response) {
        this.loadingCard = false
        if(response.data.success){
          this.card.cardBrand = response.data.card_brand
          this.card.cardNumberMasked = response.data.card_number_masked
          this.card.cardExpireMonth = response.data.card_expire_month
          this.card.cardExpireYear = response.data.card_expire_year
          this.creditCardNumber = ''
          this.creditCardDate = ''
          this.creditCardCVV = ''
          this.showCard = false

          new Noty({
            type: 'success',
            layout: 'top',
            timeout: 2000,
            text: '¡Tarjeta actualizada exitosamente!',
            killer: true,
            force: true
          }).show();
          
        } else {
          new Noty({
            type: 'error',
            layout: 'top',
            timeout: 2000,
            text: 'Hubo un error al actualizar la tarjeta, por favor inténtelo nuevamente',
            killer: true,
            force: true
          }).show();
        }
        
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    updateCard: function(){
      this.loadingCard = true;
      this.$v.creditCardNumber.$touch();
      this.$v.creditCardDate.$touch();
      this.$v.creditCardCVV.$touch();
      if (this.$v.creditCardNumber.$invalid || this.$v.creditCardDate.$invalid || this.$v.creditCardCVV.$invalid) {
        this.loadingCard = false
        return;
      }
      Culqi.createToken();
      window.culqi = function() {
        if(Culqi.token) {
          this.addCard(Culqi.token)
        } else {
          this.loadingCard = false;
          var message = "Hubo un error al registrar la tarjeta"
          switch(Culqi.error.code){
            case "invalid_email":
            message = "El email es inválido"
            break;

            case "invalid_number":
            message = "El número de tarjeta es inválido"
            break;

            case "invalid_expiration_year":
            message = "El año de expiración es inválido"
            break;

            case "invalid_expiration_month":
            message = "El mes de expiración es inválido"
            break;
          }
          new Noty({
            type:"error",
            text: message,
            layout: "top",
            timeout: 5000,
            force: true,
            killer: true
          }).show();
        }
      }.bind(this)
    },
    updateMyInformation: function(){
      this.loading = true;
      this.$v.userEdit.$touch();
      if (this.$v.userEdit.$invalid) {
        this.loading = false
        return;
      }
      var instance = axios.create({
        timeout: 30000,
        headers: {'X-CSRFToken': csrf_token}
      })
      instance.post(updateMyInformationUrl, this.userEdit)
      .then(function (response) {
        this.loading = false
        if(response.data.success){
          new Noty({
            type: 'success',
            layout: 'topCenter',
            timeout: 2000,
            text: '¡Información actualizada exitosamente!',
            killer: true,
            force: true
          }).show();
          this.user = _.clone(this.userEdit)
          if(this.user.birthday){
            this.age = moment(new Date()).diff(moment(this.user.birthday), 'year')
            this.user.birthday = moment(this.user.birthday).format('DD/MM/YYYY')
          }
          var firstName = this.user.firstName.split(' ')[0]
          firstName = firstName.toUpperCase()
          auth.username = firstName
          auth.email = this.user.email
          // this.$emit('change-name', full_name);
          this.editProfile = false
        } else {
          new Noty({
            type: 'error',
            layout: 'topCenter',
            timeout: 2000,
            text: 'Este correo ya está siendo usado',
            killer: true,
            force: true
          }).show();
        }
        
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
    },
    loadClassGraphic: function(){
      if($("#container-speed").length == 0){
        return
      }
      var chartSpeed = Highcharts.chart('container-speed', Highcharts.merge(this.gaugeOptions, {
        yAxis: {
            min: 0,
            max: this.totalPasses,
            title: {
                text: ''
            }
        },
        plotOptions: {
          solidgauge: {
              dataLabels: {
                  y: 5,
                  borderWidth: 0,
                  useHTML: true,
                  color: 'white',
              }
          }
        },
        credits: {
            enabled: false
        },
    
        series: [{
            name: 'Speed',
            data: [this.usedClass],
            dataLabels: {
                formatter: function(){
                  if(this.y == 1){
                    return '<div style="text-align:center"><span style="font-size:18px;color:white;font-weight:500">1 clase<br>utilizada</span></div>'
                  }
                  return '<div style="text-align:center"><span style="font-size:18px;color:white;font-weight:500">' + this.y + ' clases<br>utilizadas</span></div>'
                }
            },
        }]
    
    }));
    },
    loadSportCenterGraphic: function(){
      this.centersVisit = _.map(function(center){
        return {name: center.sport_name, y: center.y}
      })(this.centersVisit)
      if(this.centersVisit.length > 5){
        var totalOthers = 0
        var fiveCenters = this.centersVisit.slice(0, 5)
        var others = this.centersVisit.slice(5)
        _.forEach(function(center){
          totalOthers = totalOthers + center.y
        })(others)
        others = {name: 'Otros', y: totalOthers}
        fiveCenters.push(others)
        this.centersVisit = fiveCenters
      }

      _.forEach(function(center){
        this.totalVisitsCenters =  this.totalVisitsCenters + center.y
      }.bind(this))(this.centersVisit)

      Highcharts.chart('container-sport-centers', {
        chart: {
          height: 280,
          plotBorderWidth: 0,
          plotShadow: false
        },
        title: {
            text: this.totalVisitsCenters,
            align: 'center',
            style: {
              color: 'white'
            },
            verticalAlign: 'middle',
            y: -20
        },
        tooltip: {
            // pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            enabled: false
        },
        credits: {
          enabled: false
        },
        legend: {
          y: 0,
          width: 380,
          x: 20,
          itemWidth: 190,
          floating: true,
          align: 'center',
          itemMarginBottom: 10,
          itemStyle: {
            color: '#a1a5a0',
            fontWeight: '100',
            fontSize: '14'
          },
          itemHoverStyle: {
            fontSize: '15',
            color: '#a1a5a0'
          },
          navigation: {
            enabled: false
          }
        },
        plotOptions: {
            pie: {
              dataLabels: {
                enabled: true,
                distance: -30,
                style: {
                    fontWeight: 'bold',
                    color: 'white'
                },
                format: '{y}'
              },
              startAngle: -90,
              endAngle: 90,
              // center: ['50%', '75%'],
              // size: '50%',
              borderColor: null,
              colors: ['#00aef6','#0060cd', '#3407b3', '#5511ad', '#8c4cde', '#676565'],
              showInLegend: true
            }
        },
        series: [{
            type: 'pie',
            // name: 'Browser share',
            innerSize: '50%',
            data: this.centersVisit
        }],
        responsive: {
          rules: [{
              condition: {
                maxWidth: 991
              },
              chartOptions: {
                legend: {
                  align: 'center',
                  itemStyle: {
                    color: '#a1a5a0',
                    fontWeight: '100',
                    fontSize: '12'
                  },
                }
              }
            },{
              condition: {
                maxWidth: '480px'
              },
              chartOptions: {
                legend: {
                  layout: 'vertical'
                }
              }
            }
          ]
        }
      });
      Highcharts.chart('container-sport-centers-mobile', {
        chart: {
          height: 310,
          plotBorderWidth: 0,
          plotShadow: false
        },
        title: {
            text: this.totalVisitsCenters,
            align: 'center',
            style: {
              color: 'white'
            },
            verticalAlign: 'middle',
            y: -20
        },
        tooltip: {
            // pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            enabled: false
        },
        credits: {
          enabled: false
        },
        legend: {
          // width: 170,
          // itemWidth: 170,
          floating: true,
          y: 20,
          align: 'center',
          layout: 'vertical',
          itemMarginBottom: 10,
          itemStyle: {
            color: '#a1a5a0',
            fontWeight: '300',
            fontSize: '12'
          },
          itemHoverStyle: {
            fontSize: '13',
            color: '#a1a5a0'
          },
          navigation: {
            enabled: false
          }
        },
        plotOptions: {
            pie: {
              dataLabels: {
                enabled: true,
                distance: -30,
                style: {
                    fontWeight: 'bold',
                    color: 'white'
                },
                format: '{y}'
              },
              startAngle: -90,
              endAngle: 90,
              // size: '85%',
              borderColor: null,
              colors: ['#00aef6','#0060cd', '#3407b3', '#5511ad', '#8c4cde', '#676565'],
              showInLegend: true
            }
        },
        series: [{
            type: 'pie',
            innerSize: '50%',
            data: this.centersVisit
        }]
    });
    },
    loadSalesExpired: function(){
      var instance = axios.create({
        timeout: 30000,
        headers: {'X-CSRFToken': csrf_token}
      })
      instance.post('', {
        page : this.pageSalesExpired,
        mode: 1
      })
      .then(function(response){
        var newPasses = response.data.data_list
        this.haveMoreSalesExpired = response.data.have_more
        _.forEach(function(p){
          p.created_at = moment(p.created_at).format('DD [de] MMMM, YYYY')
          p.expires_at = moment(p.expires_at).format('DD [de] MMMM, YYYY')
          p.transaction__amount = p.transaction__amount.toFixed(2)
        })(newPasses)
        this.passesList = this.passesList.concat(newPasses)
        this.pageSalesExpired = this.pageSalesExpired + 1
      }.bind(this))
    },
    loadPastVisits: function(){
      var instance = axios.create({
        timeout: 30000,
        headers: {'X-CSRFToken': csrf_token}
      })
      instance.post('', {
        page : this.pagePastVisits,
        mode: 2
      })
      .then(function(response){
        var newVisits = response.data.data_list
        this.haveMorePastVisits = response.data.have_more
        _.forEach(function(p){
          p.day = moment(p.day).format('DD/MM/YYYY')
        })(newVisits)
        this.pastVisits = this.pastVisits.concat(newVisits)
        this.pagePastVisits = this.pagePastVisits + 1
      }.bind(this))
    },
    setTab: function(){
      $('.tab-pane.active').removeClass('active')
      var selector = "#" + this.selectedTab
      $(selector).addClass('active')
    },
    formatFriendPassUsed: function(){
      this.friendpassUsed = _.groupBy(function(f) { return f.reservation_id })(this.friendpassUsed)
      
      this.friendpassUsed = _.transform(function(result, value) {
        var dict = {
          quantity: value.length,
          sportCenterName: value[0].sport_center_name,
          sportCenterSlug: value[0].sport_center_slug,
          day: moment(value[0].date).format('DD [de] MMMM, YYYY')
        }
        result.push(dict)
      }, [])(this.friendpassUsed)
    }
  }
})