from django.contrib import admin
from django.contrib.auth.models import *
from django.contrib.admin import AdminSite

# Register your models here.

admin.site.unregister(User)
admin.site.unregister(Group)

admin.site.site_header = 'Administración Rockfit'