# Generated by Django 2.0.5 on 2018-08-29 16:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0010_useractivityreservation_notified_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='userpendingplan',
            name='notified_count',
            field=models.PositiveIntegerField(default=0, verbose_name='Cantidad de notificaciones'),
        ),
    ]
