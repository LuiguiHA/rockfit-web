FROM python:3

WORKDIR /usr/src/app

COPY code .
RUN pip install --no-cache-dir -r requirements.txt

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -

RUN apt update && apt install nodejs -y

RUN npm install -g bower

RUN echo '{ "allow_root": true }' > /root/.bowerrc

ENV DJANGO_SETTINGS_MODULE=project.production_settings

EXPOSE 80

RUN apt-get update && apt-get install -y --no-install-recommends nginx vim gettext libcairo2 libffi-dev libpango1.0-0 \
  libgdk-pixbuf2.0-0 libxml2-dev libxslt1-dev shared-mime-info cron gdal-bin
  
RUN pip install -r requirements.txt
RUN python manage.py collectstatic --noinput

ADD nginx/nginx.conf /etc/nginx/
ADD nginx/certs/ /etc/ssl/

ENV GUNICORN_CMD_ARGS="--bind=unix:/tmp/gunicorn.sock --workers=3 -D"

CMD ["bash", "start.sh"]